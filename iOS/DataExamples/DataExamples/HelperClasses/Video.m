//
//  Video.m
//  how2dance
//
//  Created by jbott on 10/24/12.
//  Copyright (c) 2012 jbott. All rights reserved.
//

#import "Video.h"

@implementation Video

- (id) initWithEnglishName: (NSString*) englishName fileName: (NSString*) fileName videoId: (NSInteger) videoId lessonNumber: (NSInteger) lessonNumber
{
    if (self = [super init])
    {
        self.englishName = englishName;
        self.fileName = fileName;
        self.videoId = videoId;
        self.lessonNumber = lessonNumber;
    }
    return self;
}

@end
