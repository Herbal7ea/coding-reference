//
//  VideoGroup.h
//  how2dance
//
//  Created by jbott on 10/24/12.
//  Copyright (c) 2012 jbott. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoGroup : NSObject

@property NSInteger groupNumber;
@property (strong, nonatomic) NSString* title;
@property (strong, nonatomic) NSMutableArray* videos;

-(id)initWithTitle: (NSString*) title andGroupNumber: (NSInteger) groupNumber;

@end
