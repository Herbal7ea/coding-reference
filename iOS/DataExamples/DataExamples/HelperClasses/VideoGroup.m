//
//  VideoGroup.m
//  how2dance
//
//  Created by jbott on 10/24/12.
//  Copyright (c) 2012 jbott. All rights reserved.
//

#import "VideoGroup.h"

@implementation VideoGroup

-(id)initWithTitle: (NSString*) title andGroupNumber: (NSInteger) groupNumber
{
    if (self = [super init])
    {
        self.groupNumber = groupNumber;
        self.title = title;
        self.videos = [[NSMutableArray alloc] init];
    }
    return self;
}

@end


