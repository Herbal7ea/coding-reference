//
//  Video.h
//  how2dance
//
//  Created by jbott on 10/24/12.
//  Copyright (c) 2012 jbott. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Video : NSObject

@property (strong, nonatomic) NSString* englishName;
@property (strong, nonatomic) NSString* fileName;
@property NSInteger videoId;
@property NSInteger lessonNumber;

- (id) initWithEnglishName: (NSString*) englishName fileName: (NSString*) fileName videoId: (NSInteger) videoId lessonNumber: (NSInteger) lessonNumber;

@end
