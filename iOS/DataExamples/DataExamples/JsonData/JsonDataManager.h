//
//  JsonDataManager.h
//  DataExamples
//
//  Created by jbott on 4/3/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Video;
@class VideoGroup;

@interface JsonDataManager : NSObject

@property (strong, nonatomic) NSMutableArray*lessonGroups;
@property (strong, nonatomic) NSMutableArray* demoGroups;

+(JsonDataManager *)sharedInstance;

- (void)parseConfig;

//- (Video *)getVideoByGroupsType:(NSString *)groupsType AndGroupNumber:(NSInteger)groupNumber AndVideoNumber:(NSInteger)videoNumber;
//
//- (NSURL *)getMovieUrlByGroupsType:(NSString *)groupsType AndGroupNumber:(NSInteger)groupNumber AndVideoNumber:(NSInteger)videoNumber;
//
//- (VideoGroup *)getVideoGroupByGroupsType:(NSString *)groupsType AndGroupNumber:(NSInteger)groupNumber;


@end
