//
//  JsonDataManager.m
//  DataExamples
//
//  Created by jbott on 4/3/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "JsonDataManager.h"
#import "Video.h"
#import "VideoGroup.h"

@implementation JsonDataManager

+ (JsonDataManager *)sharedInstance
{
	static JsonDataManager *sharedInstance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^
	{
		sharedInstance = [JsonDataManager new];
		//Do any other initialisation stuff here
	});

	return sharedInstance;
}

- (void)parseConfig
{
	NSError *error;

	NSString *path = [NSBundle.mainBundle pathForResource:@"videos" ofType:@"json"];
	NSData *jsonData = [[NSMutableData alloc] initWithContentsOfFile:path];

	id json = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
	if( !json )
	{
		//handle error
	}

	//	NSLog(@"json: %@", json );
	NSLog( @"%@", json[ @"videoConfig" ][ @"groups" ][ 0 ][ @"group" ][ 0 ][ @"title" ] );
}
@end
