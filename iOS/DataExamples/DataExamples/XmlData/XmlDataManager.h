//
//  GoogleXml.h
//  XMLParse1
//
//  Created by jbott on 9/27/12.
//  Copyright (c) 2012 jbott. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VideoGroup;
@class Video;

static NSString *const VIDEO_GROUP_TYPE__DEMO = @"demoGroups";
static NSString *const VIDEO_GROUP_TYPE__LESSON = @"lessonGroups";

@interface XmlDataManager : NSObject

@property (strong, nonatomic) NSMutableArray*lessonGroups;
@property (strong, nonatomic) NSMutableArray* demoGroups;

+(XmlDataManager *)sharedInstance;

- (void)parseConfig;

- (NSURL *)getMovieUrlByGroupsType:(NSString *)groupsType AndGroupNumber:(NSInteger)groupNumber AndVideoNumber:(NSInteger)videoNumber;

- (VideoGroup *)getVideoGroupByGroupsType:(NSString *)groupsType AndGroupNumber:(NSInteger)groupNumber;

- (Video *)getVideoByGroupsType:(NSString *)groupsType AndGroupNumber:(NSInteger)groupNumber AndVideoNumber:(NSInteger)videoNumber;


@end
