//
//  GoogleXml.m
//  XMLParse1
//
//  Created by jbott on 9/27/12.
//  Copyright (c) 2012 jbott. All rights reserved.
//

#import "XmlDataManager.h"
#import "GDataXMLNode.h"
#import "VideoGroup.h"
#import "Video.h"

@implementation XmlDataManager


+ (XmlDataManager *)sharedInstance
{
	static XmlDataManager *sharedInstance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^
	{
		sharedInstance = [XmlDataManager new];
		//Do any other initialisation stuff here
	});
	return sharedInstance;
}

- (void)parseConfig
{
	NSError *error;

	NSString *path = [NSBundle.mainBundle pathForResource:@"videos" ofType:@"xml"];
	NSData *xmlData = [[NSMutableData alloc] initWithContentsOfFile:path];
	GDataXMLDocument *doc = [[GDataXMLDocument alloc] initWithData:xmlData options:0 error:&error];

	if(doc == nil)
	{
		NSLog(@"No data found");
		return;
	}

	self.lessonGroups = [self parseXmlForGroupsWithXPath:@"(//videoConfig)/groups[1]/group" ForDocument:doc];
	self.demoGroups = [self parseXmlForGroupsWithXPath:@"(//videoConfig)/groups[2]/group" ForDocument:doc];

	[self logData];
}

- (Video *)getVideoByGroupsType:(NSString *)groupsType AndGroupNumber:(NSInteger)groupNumber AndVideoNumber:(NSInteger)videoNumber
{
	NSMutableArray *groups = [self valueForKey:groupsType];
	Video *video = (Video *) ((VideoGroup *) groups[(NSUInteger) groupNumber]).videos[(NSUInteger) videoNumber];

	return video;
}

- (NSURL *)getMovieUrlByGroupsType:(NSString *)groupsType AndGroupNumber:(NSInteger)groupNumber AndVideoNumber:(NSInteger)videoNumber
{
	Video *video = [self getVideoByGroupsType:groupsType AndGroupNumber:groupNumber AndVideoNumber:videoNumber];

	if([video.fileName isEqualToString:@""])
		return nil;
	else
		return [NSURL fileURLWithPath:[NSBundle.mainBundle pathForResource:video.fileName ofType:@"m4v"]];
}

- (VideoGroup *)getVideoGroupByGroupsType:(NSString *)groupsType AndGroupNumber:(NSInteger)groupNumber;
{
	NSMutableArray *groups = [self valueForKey:groupsType];
	return (VideoGroup *) groups[(NSUInteger) groupNumber];
}

#pragma mark - private methods

- (NSMutableArray *)parseXmlForGroupsWithXPath:(NSString *)xPath ForDocument:(GDataXMLDocument *)doc
{
	NSMutableArray *videoGroups = @[].mutableCopy;
	NSArray *videoGroupElements = [doc nodesForXPath:xPath error:nil];

	[videoGroupElements enumerateObjectsUsingBlock:^(GDataXMLElement *videoGroupElement, NSUInteger idx, BOOL *stop)
	{
		NSString *title = [videoGroupElement attributeForName:@"title"].stringValue;
		int groupNumber = videoGroupElement[@"number"].stringValue.intValue;

		VideoGroup *videoGroup = [[VideoGroup alloc] initWithTitle:title andGroupNumber:groupNumber];

		[videoGroupElement.children enumerateObjectsUsingBlock:^(GDataXMLElement *videoElement, NSUInteger idx2, BOOL *stop2)
		{
			Video *video = [[Video alloc] initWithEnglishName:[videoElement attributeForName:@"englishName"].stringValue
													 fileName:[videoElement attributeForName:@"fileName"].stringValue
													  videoId:[videoElement attributeForName:@"videoId"].stringValue.intValue
												 lessonNumber:[videoElement attributeForName:@"lessonNumber"].stringValue.intValue];
			[videoGroup.videos addObject:video];
		}];

		[videoGroups addObject:videoGroup];
	}];

	return videoGroups;
}

#pragma mark - helper methods

- (void)logData
{
	[self LogGroups:self.lessonGroups];
	[self LogGroups:self.demoGroups];
}

- (void)LogGroups:(NSMutableArray *)videoGroups
{
	[videoGroups enumerateObjectsUsingBlock:^(VideoGroup *group, NSUInteger idx, BOOL *stop)
	{
		NSLog(@"~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
		NSLog(@"videoGroup title: %@ number: %d", group.title, group.groupNumber);
		NSLog(@"count: %d", group.videos.count);
		[group.videos enumerateObjectsUsingBlock:^(Video *video, NSUInteger idx2, BOOL *stop2)
		{
			NSLog(@"<video englishName=\"%@\" fileName=\"%@\" videoId=\"%d\" lessonNumber=\"%d\" />", video.englishName, video.fileName, video.videoId, video.lessonNumber);
		}];
	}];
}

@end
