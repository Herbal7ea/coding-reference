#import "CallbackExample.h"

typedef NSString *(^simpleCallback)(NSString *someVar);
typedef void (^handleNumber)(NSNumber *number);
typedef NSString *(^handleNumberAndReturnString)(NSNumber *number);


@interface CallbackExample ()
{
	NSArray *myArray;
}

@end

@implementation CallbackExample
{

}

- (instancetype)init
{
	self = [super init];
	if(self)
	{
		myArray = @[@1, @2, @3];
	}

	return self;
}

+ (void)run
{
	CallbackExample *cb = [CallbackExample new];



	[cb methodUsingCallback:^NSString *(NSString *someVar) {
		NSLog(@"~some complex code");
		return @"final value";
	}];

	[cb loopThroughNumbersWithBlock:^(NSNumber *number) {
		NSLog(@"\tDo something with the Number: %@", number);
	}];

	[cb loopThroughNumbersAndReturnValuesWithBlock:^NSString *(NSNumber *number) {
		NSLog(@"\tinside block: %@", number);
		return number.stringValue;
	}];
}

- (void)methodUsingCallback:(simpleCallback)callback
{
	NSString *returnValue = callback(@"someString");
	NSLog(@"called from Callback: %@", returnValue);
}

- (void)loopThroughNumbersWithBlock:(handleNumber)handleNumberBlock
{
	NSLog(@"loopThroughNumbersWithBlock");
	if (handleNumberBlock != nil) {
		for (int i = 0; i < myArray.count; i++)
		{
			handleNumberBlock(myArray[i]);
		}
	}
}

- (void)loopThroughNumbersAndReturnValuesWithBlock:(handleNumberAndReturnString)handler
{
	if(handler != nil)
	{
		for (int i = 0; i < myArray.count; i++)
		{
			NSString *retVal = handler(myArray[i]);
			NSLog(@"return value: %@", retVal);
		}
	}
}



@end