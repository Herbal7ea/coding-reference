//
//  AppDelegate.m
//  DataExamples
//
//  Created by jbott on 4/3/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "AppDelegate.h"
#import "XmlDataManager.h"
#import "JsonDataManager.h"
#import "CallbackExample.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	[self presentExamples];

	return YES;
}

- (void)presentExamples
{
//	[XmlDataManager.sharedInstance parseConfig];
	[JsonDataManager.sharedInstance parseConfig];
//	[CallbackExample run];
    [self presentDateExample];
    
}

- (void)presentDateExample
{
    NSDate *today = [NSDate date];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay |
                                    NSCalendarUnitMonth |
                                    NSCalendarUnitYear
                                                                   fromDate:today];
    
    NSInteger day = [components day];
    NSInteger month = [components month];
    NSInteger year = [components year];
    
    NSLog(@"month %d", month);
    
    
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    
    [df setDateFormat:@"dd"];
    NSString *myDayString = [df stringFromDate:[NSDate date]];
    
    [df setDateFormat:@"MMM"];
    NSString *myMonthString = [df stringFromDate:[NSDate date]];
    
    [df setDateFormat:@"yy"];
    NSString *myYearString = [df stringFromDate:[NSDate date]];
    
    [df setDateFormat:@"MM"];
    NSString *monthNumberString = [df stringFromDate:[NSDate date]];
    
    NSLog(myDayString);
    NSLog(myMonthString);
    NSLog(myYearString);
    NSLog(monthNumberString);
    
    
    //    [myDayString intValue];
}


@end
