#import "PopoverContentViewController.h"

@interface PopoverContentViewController ()

@property (nonatomic, strong) UIButton *buttonPhoto;
@property (nonatomic, strong) UIButton *buttonAudio;
@property (nonatomic, strong) UILabel *labelPopOver;

@end

@implementation PopoverContentViewController

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self createSimpleView];
}


#pragma mark - private methods

- (void) gotoAppleWebsite:(id)paramSender
{

    if (self.isInPopover)
	{
        /* Go to website and then dismiss popover */
        [self.myPopoverController dismissPopoverAnimated:YES];
    } else {
        /* Handle case for iPhone */
    }

}

- (void) gotoAppleStoreWebsite:(id)paramSender{

    if (self.isInPopover)
	{
        /* Go to website and then dismiss popover */
        [self.myPopoverController dismissPopoverAnimated:YES];
    } else {
        /* Handle case for iPhone */
    }
}

#pragma mark - helper methods

- (void)createSimpleView
{
	self.preferredContentSize = CGSizeMake(200, 200);

	CGRect buttonRect = CGRectMake(20, 20, 160, 37);

	self.buttonPhoto = [UIButton buttonWithType:UIButtonTypeSystem];
	self.buttonPhoto.frame = buttonRect;
	[self.buttonPhoto setTitle:@"Photo" forState:UIControlStateNormal];
	[self.buttonPhoto addTarget:self action:@selector(gotoAppleWebsite:) forControlEvents:UIControlEventTouchUpInside];


	buttonRect.origin.y += 50.0f;
	self.buttonAudio = [UIButton buttonWithType:UIButtonTypeSystem];
	self.buttonAudio.frame = buttonRect;
	[self.buttonAudio setTitle:@"Audio" forState:UIControlStateNormal];
	[self.buttonAudio addTarget:self action:@selector(gotoAppleStoreWebsite:) forControlEvents:UIControlEventTouchUpInside];

	buttonRect.origin.y += 50;
	buttonRect.origin.x += 50;
	self.labelPopOver = [[UILabel alloc] initWithFrame: buttonRect];
	self.labelPopOver.text = @"PopOver";

	[self.view addSubview:self.buttonPhoto];
	[self.view addSubview:self.buttonAudio];
	[self.view addSubview:self.labelPopOver];
}

- (BOOL) isInPopover
{

	Class popoverClass = NSClassFromString(@"UIPopoverController");

	return (popoverClass != nil  &&
			UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad &&
			self.myPopoverController != nil);
}

@end
