//
//  Popover_Controller.m
//  StoryBoards
//
//  Created by jbott on 3/21/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "Popover_Controller.h"
#import "PopoverContentViewController.h"


static NSString *const BUTTON_TITLE__PHOTO = @"Photo";
static NSString *const BUTTON_TITLE_AUDIO = @"Audio";

@interface Popover_Controller  () <UIAlertViewDelegate>

@property (nonatomic, strong) UIPopoverController *myPopoverController;
@property (nonatomic, strong) UIBarButtonItem *barButtonAdd;

@end

@implementation Popover_Controller

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self preparePopover];
}

- (void)preparePopover
{
	SEL action;

	Class popoverClass = NSClassFromString(@"UIPopoverController");

	if (popoverClass != nil && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
		PopoverContentViewController *content = [[PopoverContentViewController alloc] initWithNibName:nil bundle:nil];

		self.myPopoverController = [[UIPopoverController alloc] initWithContentViewController:content];
		content.myPopoverController = self.myPopoverController;

		action = @selector(performAddWithPopover:);
	} else {

		action = @selector(performAddWithAlertView:);
	}

	self.barButtonAdd = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:action];

//	[self.navigationItem setRightBarButtonItem:self.barButtonAdd animated:NO];
	self.navigationItem.rightBarButtonItem = self.barButtonAdd;
}

#pragma mark - events

- (void) alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];

	if ([buttonTitle isEqualToString:BUTTON_TITLE__PHOTO])
	{

	}
	else if ([buttonTitle isEqualToString:BUTTON_TITLE_AUDIO])
	{

	}
}

#pragma mark - private methods

- (void) performAddWithAlertView:(id)paramSender
{
	[[[UIAlertView alloc] initWithTitle:nil
								message:@"Add..."
							   delegate:self
					  cancelButtonTitle:@"Cancel"
					  otherButtonTitles:BUTTON_TITLE__PHOTO, BUTTON_TITLE_AUDIO, nil] show];

}

- (void) performAddWithPopover:(id)paramSender
{
	[self.myPopoverController presentPopoverFromBarButtonItem:self.barButtonAdd permittedArrowDirections:UIPopoverArrowDirectionAny animated:YES];
}


@end
