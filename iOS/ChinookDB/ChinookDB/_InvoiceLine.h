// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to InvoiceLine.h instead.

#import <CoreData/CoreData.h>


extern const struct InvoiceLineAttributes {
	__unsafe_unretained NSString *invoiceId;
	__unsafe_unretained NSString *invoiceLineId;
	__unsafe_unretained NSString *quantity;
	__unsafe_unretained NSString *trackId;
	__unsafe_unretained NSString *unitPrice;
} InvoiceLineAttributes;

extern const struct InvoiceLineRelationships {
} InvoiceLineRelationships;

extern const struct InvoiceLineFetchedProperties {
} InvoiceLineFetchedProperties;








@interface InvoiceLineID : NSManagedObjectID {}
@end

@interface _InvoiceLine : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (InvoiceLineID*)objectID;





@property (nonatomic, strong) NSNumber* invoiceId;



@property int32_t invoiceIdValue;
- (int32_t)invoiceIdValue;
- (void)setInvoiceIdValue:(int32_t)value_;

//- (BOOL)validateInvoiceId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* invoiceLineId;



@property int32_t invoiceLineIdValue;
- (int32_t)invoiceLineIdValue;
- (void)setInvoiceLineIdValue:(int32_t)value_;

//- (BOOL)validateInvoiceLineId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* quantity;



@property int32_t quantityValue;
- (int32_t)quantityValue;
- (void)setQuantityValue:(int32_t)value_;

//- (BOOL)validateQuantity:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* trackId;



@property int32_t trackIdValue;
- (int32_t)trackIdValue;
- (void)setTrackIdValue:(int32_t)value_;

//- (BOOL)validateTrackId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDecimalNumber* unitPrice;



//- (BOOL)validateUnitPrice:(id*)value_ error:(NSError**)error_;






@end

@interface _InvoiceLine (CoreDataGeneratedAccessors)

@end

@interface _InvoiceLine (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveInvoiceId;
- (void)setPrimitiveInvoiceId:(NSNumber*)value;

- (int32_t)primitiveInvoiceIdValue;
- (void)setPrimitiveInvoiceIdValue:(int32_t)value_;




- (NSNumber*)primitiveInvoiceLineId;
- (void)setPrimitiveInvoiceLineId:(NSNumber*)value;

- (int32_t)primitiveInvoiceLineIdValue;
- (void)setPrimitiveInvoiceLineIdValue:(int32_t)value_;




- (NSNumber*)primitiveQuantity;
- (void)setPrimitiveQuantity:(NSNumber*)value;

- (int32_t)primitiveQuantityValue;
- (void)setPrimitiveQuantityValue:(int32_t)value_;




- (NSNumber*)primitiveTrackId;
- (void)setPrimitiveTrackId:(NSNumber*)value;

- (int32_t)primitiveTrackIdValue;
- (void)setPrimitiveTrackIdValue:(int32_t)value_;




- (NSDecimalNumber*)primitiveUnitPrice;
- (void)setPrimitiveUnitPrice:(NSDecimalNumber*)value;




@end
