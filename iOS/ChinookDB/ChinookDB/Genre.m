#import "Genre.h"


@interface Genre ()

// Private interface goes here.

@end


@implementation Genre

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nGenre\ngenreId: %@\n name: %@", self.genreId, self.name];
}

@end
