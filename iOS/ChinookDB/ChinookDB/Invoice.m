#import "Invoice.h"


@interface Invoice ()

// Private interface goes here.

@end


@implementation Invoice

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nInvoice\ninvoiceId: %@\n customerId: %@\n invoiceDate: %@\n total: %@\n billingAddress: %@\n billingCity: %@\n billingPostalCode: %@\n billingState: %@\n billingCountry: %@", self.invoiceId, self.customerId, self.invoiceDate, self.total, self.billingAddress, self.billingCity, self.billingPostalCode, self.billingState, self.billingCountry];
}



@end
