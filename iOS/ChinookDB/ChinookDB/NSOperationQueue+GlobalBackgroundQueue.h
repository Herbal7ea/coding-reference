//
//  NSOperationQueue+NSOperationQueue_GlobalBackgroundQueue.h
//  ChinookDB
//
//  Created by jbott on 9/18/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSOperationQueue (GlobalBackgroundQueue)

+ (instancetype)backgroundQueue;

@end
