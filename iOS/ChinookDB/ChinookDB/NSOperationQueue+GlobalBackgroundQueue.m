//
//  NSOperationQueue+NSOperationQueue_GlobalBackgroundQueue.m
//  ChinookDB
//
//  Created by jbott on 9/18/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import "NSOperationQueue+GlobalBackgroundQueue.h"

//NSOperationQueue+GlobalBackgroundQueue.m

static NSOperationQueue *backgroundQueue;

@implementation NSOperationQueue (GlobalBackgroundQueue)

+ (instancetype)backgroundQueue
{
	static dispatch_once_t dispatchToken;
	dispatch_once( &dispatchToken, ^{
		backgroundQueue = [self new];
	} );
	return backgroundQueue;
}

@end
