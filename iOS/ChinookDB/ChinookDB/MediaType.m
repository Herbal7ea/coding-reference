#import "MediaType.h"


@interface MediaType ()

// Private interface goes here.

@end


@implementation MediaType

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nMediaType\nmediaTypeId: %@\n name: %@", self.mediaTypeId, self.name];
}

@end
