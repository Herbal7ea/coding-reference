// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Customer.m instead.

#import "_Customer.h"

const struct CustomerAttributes CustomerAttributes = {
	.address = @"address",
	.city = @"city",
	.company = @"company",
	.country = @"country",
	.customerId = @"customerId",
	.email = @"email",
	.fax = @"fax",
	.firstName = @"firstName",
	.lastName = @"lastName",
	.phone = @"phone",
	.postalCode = @"postalCode",
	.state = @"state",
	.supportRepId = @"supportRepId",
};

const struct CustomerRelationships CustomerRelationships = {
};

const struct CustomerFetchedProperties CustomerFetchedProperties = {
};

@implementation CustomerID
@end

@implementation _Customer

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Customer" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Customer";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Customer" inManagedObjectContext:moc_];
}

- (CustomerID*)objectID {
	return (CustomerID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"customerIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"customerId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"supportRepIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"supportRepId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic address;






@dynamic city;






@dynamic company;






@dynamic country;






@dynamic customerId;



- (int32_t)customerIdValue {
	NSNumber *result = [self customerId];
	return [result intValue];
}

- (void)setCustomerIdValue:(int32_t)value_ {
	[self setCustomerId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCustomerIdValue {
	NSNumber *result = [self primitiveCustomerId];
	return [result intValue];
}

- (void)setPrimitiveCustomerIdValue:(int32_t)value_ {
	[self setPrimitiveCustomerId:[NSNumber numberWithInt:value_]];
}





@dynamic email;






@dynamic fax;






@dynamic firstName;






@dynamic lastName;






@dynamic phone;






@dynamic postalCode;






@dynamic state;






@dynamic supportRepId;



- (int32_t)supportRepIdValue {
	NSNumber *result = [self supportRepId];
	return [result intValue];
}

- (void)setSupportRepIdValue:(int32_t)value_ {
	[self setSupportRepId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveSupportRepIdValue {
	NSNumber *result = [self primitiveSupportRepId];
	return [result intValue];
}

- (void)setPrimitiveSupportRepIdValue:(int32_t)value_ {
	[self setPrimitiveSupportRepId:[NSNumber numberWithInt:value_]];
}










@end
