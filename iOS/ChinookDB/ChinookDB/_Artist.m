// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Artist.m instead.

#import "_Artist.h"

const struct ArtistAttributes ArtistAttributes = {
	.artistId = @"artistId",
	.name = @"name",
};

const struct ArtistRelationships ArtistRelationships = {
};

const struct ArtistFetchedProperties ArtistFetchedProperties = {
};

@implementation ArtistID
@end

@implementation _Artist

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Artist" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Artist";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Artist" inManagedObjectContext:moc_];
}

- (ArtistID*)objectID {
	return (ArtistID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"artistIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"artistId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic artistId;



- (int32_t)artistIdValue {
	NSNumber *result = [self artistId];
	return [result intValue];
}

- (void)setArtistIdValue:(int32_t)value_ {
	[self setArtistId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveArtistIdValue {
	NSNumber *result = [self primitiveArtistId];
	return [result intValue];
}

- (void)setPrimitiveArtistIdValue:(int32_t)value_ {
	[self setPrimitiveArtistId:[NSNumber numberWithInt:value_]];
}





@dynamic name;











@end
