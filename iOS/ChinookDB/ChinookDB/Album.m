#import "Album.h"


@interface Album ()

// Private interface goes here.

@end


@implementation Album

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nAlbum\nalbumId:%@ \ntitle: %@ \nartistId: %@", self.albumId, self.title, self.artistId];
}


@end
