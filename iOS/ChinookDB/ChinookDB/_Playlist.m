// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Playlist.m instead.

#import "_Playlist.h"

const struct PlaylistAttributes PlaylistAttributes = {
	.name = @"name",
	.playlistId = @"playlistId",
};

const struct PlaylistRelationships PlaylistRelationships = {
};

const struct PlaylistFetchedProperties PlaylistFetchedProperties = {
};

@implementation PlaylistID
@end

@implementation _Playlist

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Playlist" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Playlist";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Playlist" inManagedObjectContext:moc_];
}

- (PlaylistID*)objectID {
	return (PlaylistID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"playlistIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"playlistId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic name;






@dynamic playlistId;



- (int32_t)playlistIdValue {
	NSNumber *result = [self playlistId];
	return [result intValue];
}

- (void)setPlaylistIdValue:(int32_t)value_ {
	[self setPlaylistId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePlaylistIdValue {
	NSNumber *result = [self primitivePlaylistId];
	return [result intValue];
}

- (void)setPrimitivePlaylistIdValue:(int32_t)value_ {
	[self setPrimitivePlaylistId:[NSNumber numberWithInt:value_]];
}










@end
