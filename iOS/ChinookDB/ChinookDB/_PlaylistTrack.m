// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PlaylistTrack.m instead.

#import "_PlaylistTrack.h"

const struct PlaylistTrackAttributes PlaylistTrackAttributes = {
	.playlistId = @"playlistId",
	.trackId = @"trackId",
};

const struct PlaylistTrackRelationships PlaylistTrackRelationships = {
};

const struct PlaylistTrackFetchedProperties PlaylistTrackFetchedProperties = {
};

@implementation PlaylistTrackID
@end

@implementation _PlaylistTrack

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"PlaylistTrack" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"PlaylistTrack";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"PlaylistTrack" inManagedObjectContext:moc_];
}

- (PlaylistTrackID*)objectID {
	return (PlaylistTrackID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"playlistIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"playlistId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"trackIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"trackId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic playlistId;



- (int32_t)playlistIdValue {
	NSNumber *result = [self playlistId];
	return [result intValue];
}

- (void)setPlaylistIdValue:(int32_t)value_ {
	[self setPlaylistId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitivePlaylistIdValue {
	NSNumber *result = [self primitivePlaylistId];
	return [result intValue];
}

- (void)setPrimitivePlaylistIdValue:(int32_t)value_ {
	[self setPrimitivePlaylistId:[NSNumber numberWithInt:value_]];
}





@dynamic trackId;



- (int32_t)trackIdValue {
	NSNumber *result = [self trackId];
	return [result intValue];
}

- (void)setTrackIdValue:(int32_t)value_ {
	[self setTrackId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveTrackIdValue {
	NSNumber *result = [self primitiveTrackId];
	return [result intValue];
}

- (void)setPrimitiveTrackIdValue:(int32_t)value_ {
	[self setPrimitiveTrackId:[NSNumber numberWithInt:value_]];
}










@end
