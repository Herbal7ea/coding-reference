//
//  LocalHTTPServer.m
//  Playground
//
//  Created by Brian Mullen on 8/21/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "LocalHTTPServer.h"
#import "LocalHTTPConnection.h"


@implementation LocalHTTPServer

+ (instancetype)sharedInstance
{
    static LocalHTTPServer *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once( &oncePredicate, ^
    {
        _sharedInstance = [[self alloc] init];
        _sharedInstance.interface = @"localhost";
        _sharedInstance.port = 12866;
        _sharedInstance.connectionClass = LocalHTTPConnection.class;
        _sharedInstance.documentRoot = [NSBundle.mainBundle.bundlePath stringByAppendingPathComponent:@"htdocs"];
    } );
    
    return _sharedInstance;
}

@end
