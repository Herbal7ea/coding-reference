//
//  APIClient.h
//  Playground
//
//  Created by Brian Mullen on 8/21/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "AFHTTPSessionManager.h"


@interface APIClient : AFHTTPSessionManager

@property(nonatomic, strong) NSArray *classNames;

@property(nonatomic, readonly) int classIndex;

+ (instancetype)sharedInstance;

- (void)loadJsonAll;

- (void)loadJsonForClass:(NSString *)className;
@end
