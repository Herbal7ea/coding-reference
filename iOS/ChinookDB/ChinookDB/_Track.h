// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Track.h instead.

#import <CoreData/CoreData.h>


extern const struct TrackAttributes {
	__unsafe_unretained NSString *albumId;
	__unsafe_unretained NSString *bytes;
	__unsafe_unretained NSString *composer;
	__unsafe_unretained NSString *genreId;
	__unsafe_unretained NSString *mediaTypeId;
	__unsafe_unretained NSString *milliseconds;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *trackId;
	__unsafe_unretained NSString *unitPrice;
} TrackAttributes;

extern const struct TrackRelationships {
} TrackRelationships;

extern const struct TrackFetchedProperties {
} TrackFetchedProperties;












@interface TrackID : NSManagedObjectID {}
@end

@interface _Track : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (TrackID*)objectID;





@property (nonatomic, strong) NSNumber* albumId;



@property int32_t albumIdValue;
- (int32_t)albumIdValue;
- (void)setAlbumIdValue:(int32_t)value_;

//- (BOOL)validateAlbumId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* bytes;



@property int32_t bytesValue;
- (int32_t)bytesValue;
- (void)setBytesValue:(int32_t)value_;

//- (BOOL)validateBytes:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* composer;



//- (BOOL)validateComposer:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* genreId;



@property int32_t genreIdValue;
- (int32_t)genreIdValue;
- (void)setGenreIdValue:(int32_t)value_;

//- (BOOL)validateGenreId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* mediaTypeId;



@property int32_t mediaTypeIdValue;
- (int32_t)mediaTypeIdValue;
- (void)setMediaTypeIdValue:(int32_t)value_;

//- (BOOL)validateMediaTypeId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* milliseconds;



@property int32_t millisecondsValue;
- (int32_t)millisecondsValue;
- (void)setMillisecondsValue:(int32_t)value_;

//- (BOOL)validateMilliseconds:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* trackId;



@property int32_t trackIdValue;
- (int32_t)trackIdValue;
- (void)setTrackIdValue:(int32_t)value_;

//- (BOOL)validateTrackId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDecimalNumber* unitPrice;



//- (BOOL)validateUnitPrice:(id*)value_ error:(NSError**)error_;






@end

@interface _Track (CoreDataGeneratedAccessors)

@end

@interface _Track (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveAlbumId;
- (void)setPrimitiveAlbumId:(NSNumber*)value;

- (int32_t)primitiveAlbumIdValue;
- (void)setPrimitiveAlbumIdValue:(int32_t)value_;




- (NSNumber*)primitiveBytes;
- (void)setPrimitiveBytes:(NSNumber*)value;

- (int32_t)primitiveBytesValue;
- (void)setPrimitiveBytesValue:(int32_t)value_;




- (NSString*)primitiveComposer;
- (void)setPrimitiveComposer:(NSString*)value;




- (NSNumber*)primitiveGenreId;
- (void)setPrimitiveGenreId:(NSNumber*)value;

- (int32_t)primitiveGenreIdValue;
- (void)setPrimitiveGenreIdValue:(int32_t)value_;




- (NSNumber*)primitiveMediaTypeId;
- (void)setPrimitiveMediaTypeId:(NSNumber*)value;

- (int32_t)primitiveMediaTypeIdValue;
- (void)setPrimitiveMediaTypeIdValue:(int32_t)value_;




- (NSNumber*)primitiveMilliseconds;
- (void)setPrimitiveMilliseconds:(NSNumber*)value;

- (int32_t)primitiveMillisecondsValue;
- (void)setPrimitiveMillisecondsValue:(int32_t)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitiveTrackId;
- (void)setPrimitiveTrackId:(NSNumber*)value;

- (int32_t)primitiveTrackIdValue;
- (void)setPrimitiveTrackIdValue:(int32_t)value_;




- (NSDecimalNumber*)primitiveUnitPrice;
- (void)setPrimitiveUnitPrice:(NSDecimalNumber*)value;




@end
