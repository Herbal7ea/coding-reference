// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Album.h instead.

#import <CoreData/CoreData.h>


extern const struct AlbumAttributes {
	__unsafe_unretained NSString *albumId;
	__unsafe_unretained NSString *artistId;
	__unsafe_unretained NSString *title;
} AlbumAttributes;

extern const struct AlbumRelationships {
} AlbumRelationships;

extern const struct AlbumFetchedProperties {
} AlbumFetchedProperties;






@interface AlbumID : NSManagedObjectID {}
@end

@interface _Album : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (AlbumID*)objectID;





@property (nonatomic, strong) NSNumber* albumId;



@property int32_t albumIdValue;
- (int32_t)albumIdValue;
- (void)setAlbumIdValue:(int32_t)value_;

//- (BOOL)validateAlbumId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* artistId;



@property int32_t artistIdValue;
- (int32_t)artistIdValue;
- (void)setArtistIdValue:(int32_t)value_;

//- (BOOL)validateArtistId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;






@end

@interface _Album (CoreDataGeneratedAccessors)

@end

@interface _Album (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveAlbumId;
- (void)setPrimitiveAlbumId:(NSNumber*)value;

- (int32_t)primitiveAlbumIdValue;
- (void)setPrimitiveAlbumIdValue:(int32_t)value_;




- (NSNumber*)primitiveArtistId;
- (void)setPrimitiveArtistId:(NSNumber*)value;

- (int32_t)primitiveArtistIdValue;
- (void)setPrimitiveArtistIdValue:(int32_t)value_;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;




@end
