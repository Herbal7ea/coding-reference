//#define MR_SHORTHAND

#import "DataManager.h"
#import "MagicalRecord.h"
#import "MagicalRecord+Setup.h"
#import "NSOperationQueue+GlobalBackgroundQueue.h"
#import "MagicalRecord+Options.h"
#import "MagicalRecord+Actions.h"
#import "Album.h"
#import "NSManagedObjectContext+MagicalRecord.h"
#import "NSManagedObject+MagicalAggregation.h"
#import "NSPersistentStoreCoordinator+MagicalRecord.h"
#import "PlaylistTrack.h"
#import "Employee.h"
#import "NSManagedObject+MagicalFinders.h"
#import "NSManagedObject+MagicalRecord.h"
#import "NSArray+LinqExtensions.h"#import "NSManagedObjectContext+MagicalSaves.h"#import "MagicalRecord+Actions.h"

@interface DataManager()
{
}

@property(readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property(readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property(readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

@end

@implementation DataManager

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

+ (DataManager *)sharedInstance
{
	static DataManager *sharedInstance = nil;
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^
	{
		sharedInstance = [DataManager new];
		//Do any other initialisation stuff here
	});

	return sharedInstance;
}


- (void)cleanUp
{
	[MagicalRecord cleanUp];
}

- (void)loadDatabase
{
		[[NSOperationQueue backgroundQueue] addOperationWithBlock:^{

			[MagicalRecord setShouldDeleteStoreOnModelMismatch:YES];
			[MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"ChinookDB.sqlite"];

			NSManagedObjectContext *ctx = [NSManagedObjectContext MR_context];

			int count = [Album MR_countOfEntitiesWithContext:ctx];
			NSLog( @"count = %d", count );

			NSPersistentStoreCoordinator *psc = [NSPersistentStoreCoordinator MR_defaultStoreCoordinator];
			NSPersistentStore *ps = psc.persistentStores.lastObject;
			NSURL *url = ps.URL;

			{// Exclude Database from icloud
				NSString *path = [url path];
				path = [path stringByDeletingLastPathComponent];
				[DataManager addSkipBackupAttributeToItemAtURL:[NSURL fileURLWithPath:path]];
			}

			if( count <= 0 )
			{
				[MagicalRecord cleanUp];
				[self loadBundledData:url];
				[MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"ChinookDB.sqlite"];
			}
            
			NSPersistentStore *persistentStore = [NSPersistentStore MR_defaultPersistentStore];

            NSLog(@"%@", [NSPersistentStore MR_urlForStoreName:@"ChinookDB.sqlite"]);
            NSLog(@"%@", persistentStore.URL);

		}];
}


- (void)loadBundledData:(NSURL *)url
{
    NSFileManager *fm = NSFileManager.defaultManager;
	NSString *databasePath = [url.path stringByDeletingLastPathComponent];

	for( NSString *itemPath in [fm contentsOfDirectoryAtPath:databasePath error:nil] )
    {
        NSString *toPath = [databasePath stringByAppendingPathComponent:itemPath];
        [fm removeItemAtPath:toPath error:nil];
    }
    
    NSString *defaultPath = [NSBundle.mainBundle.bundlePath stringByAppendingPathComponent:@"BundledData"];
    
    NSArray *comps = [fm contentsOfDirectoryAtPath:defaultPath error:nil];
    
    for( NSString *itemPath in comps )
    {
        NSString *fromPath = [defaultPath     stringByAppendingPathComponent:itemPath];
        NSString *toPath   = [databasePath stringByAppendingPathComponent:itemPath];
        
        [fm removeItemAtPath:toPath error:nil];
        
        if( fromPath != nil && toPath != nil )
        {
            [fm copyItemAtPath:fromPath toPath:toPath error:nil];
        }
    }
}

+ (BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL;
{

	assert( [NSFileManager.defaultManager fileExistsAtPath:URL.path] );

	NSError *error = nil;
	BOOL success = [URL setResourceValue:@YES forKey:NSURLIsExcludedFromBackupKey error:&error];

	if( !success )
	{
		NSLog( @"Error excluding %@ from backup %@", URL.lastPathComponent, error );
	}

	return success;
}

- (NSInteger)countAlbums
{
	NSManagedObjectContext *context = [NSManagedObjectContext MR_context];

	return [Album MR_countOfEntitiesWithContext:context];
}

- (NSInteger)countPlaylistTracks
{
	return [PlaylistTrack MR_countOfEntities];
}

- (NSInteger)countITEmployees
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[c] %@", @"IT"];

	return [Employee MR_countOfEntitiesWithPredicate:predicate];
}

- (NSInteger)countITEmployees2
{
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[c] $filterText"];
	NSPredicate *finalPredicate = [predicate predicateWithSubstitutionVariables:@{@"filterText":@"IT"}];

	return [Employee MR_countOfEntitiesWithPredicate:finalPredicate];
}

- (NSInteger)countPeopleWithTheNameOfZebraChalk_PersistData
{
	[MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {

		Employee *employee = [Employee MR_createInContext:localContext];
		employee.employeeId = @100000;
		employee.firstName = @"ZebraChalk";
		employee.lastName = @"MUD";

	}];
    
	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstName ==[c] 'ZebraChalk'"];
	NSArray *results = [Employee MR_findAllWithPredicate:predicate];

	return results.count;
}

- (NSInteger)countPeopleWithTheNameOfZebraChalk_DoesNotPersistData
{
    Employee *employee = [Employee MR_createEntity];
    employee.employeeId = @100000;
    employee.firstName = @"ZebraChalk";
    employee.lastName = @"MUD";
    
//    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait]; //this will persist the usage
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstName ==[c] 'ZebraChalk'"];
    NSArray *results = [Employee MR_findAllWithPredicate:predicate];
    
    return results.count;
}

- (NSInteger)countPeopleWithTheNameOfZebraChalk_oldSchool
{
	NSString *firstName = @"ZebraChalk";
	NSManagedObjectContext *context = self.managedObjectContext;

	Employee *employee = [NSEntityDescription insertNewObjectForEntityForName:@"Employee" inManagedObjectContext:context];
	employee.employeeId = @100000;
	employee.firstName = firstName;
	employee.lastName = @"MUD";

	NSError *error;

	if(![context save:&error])
	{
		NSLog(@"Whoops, couldn't save: %@", error.localizedDescription);
	}

	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"firstName ==[c] %@", firstName];

	NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Employee"];
	fetchRequest.predicate = predicate;

	NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

	return results.count;
}



- (NSInteger)countEntityCountOldSchool
{
	NSError *error;
	NSString *entityName = @"PlaylistTrack";

	NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entityName];
	NSUInteger count = [self.managedObjectContext countForFetchRequest:fetchRequest error:&error];

//	if(count == NSNotFound) {
//		return 0;
//	}

	return count;
}


- (NSInteger)countITEmployeesOldSchool
{
	NSError *error;
	NSString *entityName = @"Employee";

	NSPredicate *predicate = [NSPredicate predicateWithFormat:@"title CONTAINS[c] %@", @"IT"];

	NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entityName];
	fetchRequest.predicate = predicate;

	NSArray *results = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];

	return results.count;
}

- (NSInteger)countEmployeesBornInThe70s
{
	NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];

	dateFormatter.dateFormat = @"MM/dd/yyyy";
	NSDate *startDate = [dateFormatter dateFromString:@"01/01/1970"];

	dateFormatter.dateFormat = @"MM/dd/yyyy";
	NSDate *endDate = [dateFormatter dateFromString:@"01/01/1980"];

	NSPredicate *predicateAfter1970  = [NSPredicate predicateWithFormat:@"birthDate >= %@", startDate];
	NSPredicate *predicateBefore1980 = [NSPredicate predicateWithFormat:@"birthDate <  %@" , endDate];

    NSCompoundPredicate *predicateInThe70s = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicateAfter1970, predicateBefore1980]];

	NSArray *result = [Employee MR_findAllWithPredicate:predicateInThe70s];

	return result.count;
}

- (NSArray *)firstNamesOfEmployeesBornInThe70s
{
	NSDateFormatter *dateFormatter = [NSDateFormatter new];

	dateFormatter.dateFormat = @"MM/dd/yyyy";
	NSDate *startDate = [dateFormatter dateFromString:@"01/01/1970"];

	dateFormatter.dateFormat = @"MM/dd/yyyy";
	NSDate *endDate = [dateFormatter dateFromString:@"01/01/1980"];

//	NSPredicate *predicateAfter1970  = [NSPredicate predicateWithFormat:@"birthDate >= %@", startDate];
//	NSPredicate *predicateBefore1980 = [NSPredicate predicateWithFormat:@"birthDate <  %@" , endDate];
//	NSCompoundPredicate *predicateInThe70s = [NSCompoundPredicate andPredicateWithSubpredicates:@[predicateAfter1970, predicateBefore1980]];

	NSPredicate *predicateInThe70s = [NSPredicate predicateWithFormat:@"(birthDate >= %@) AND (birthDate < %@)", startDate, endDate];


	NSSortDescriptor *descriptor = [NSSortDescriptor sortDescriptorWithKey:@"firstName" ascending:YES];

	NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Employee"];
	fetchRequest.resultType = NSDictionaryResultType;
//	fetchRequest.returnsDistinctResults = YES;Ô
	fetchRequest.propertiesToFetch = @[@"firstName"];
	fetchRequest.sortDescriptors   = @[descriptor];
	fetchRequest.predicate         = predicateInThe70s;

	return [[Employee MR_executeFetchRequest:fetchRequest] linq_select:^NSString *(NSDictionary *employee) {
				return employee[@"firstName"];
			}];
}

- (NSArray *)firstNamesOfEmployeesBornInThe70sBundledFetchRequest
{
	NSFetchRequest *fetchRequest = [self.managedObjectModel fetchRequestTemplateForName:@"employeesBornInThe70s"];

//    NSFetchRequest *fetchRequest = [self.managedObjectModel fetchRequestFromTemplateWithName:@"employeesBornInThe70s" substitutionVariables:nil];

	NSArray *array = [Employee MR_executeFetchRequest:fetchRequest];

	return [array linq_select:^NSString *(Employee *employee) {
		return employee.firstName;
	}];
}

#pragma mark - Core Data stack

- (void)saveContext {
	NSError *error = nil;
	NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
	if(managedObjectContext != nil) {
		if([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
			// Replace this implementation with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			abort();
		}
	}
}

- (NSManagedObjectContext *)managedObjectContext {
	if(_managedObjectContext != nil) {
		return _managedObjectContext;
	}

	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if(coordinator != nil) {
		_managedObjectContext = [[NSManagedObjectContext alloc] init];
		[_managedObjectContext setPersistentStoreCoordinator:coordinator];
	}
	return _managedObjectContext;
}

- (NSManagedObjectModel *)managedObjectModel {
	if(_managedObjectModel != nil) {
		return _managedObjectModel;
	}
	NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"ChinookDB" withExtension:@"momd"];
	_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
	return _managedObjectModel;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
	if(_persistentStoreCoordinator != nil) {
		return _persistentStoreCoordinator;
	}

//	NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"ChinookDB.sqlite"];
	NSURL *storeURL = [self.applicationSupportDirectory URLByAppendingPathComponent:@"ChinookDB/ChinookDB.sqlite"];
	
    NSLog(@"%@", storeURL.path);
    
	NSError *error = nil;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
	if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
		/*
		 Replace this implementation with code to handle the error appropriately.

		 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

		 Typical reasons for an error here include:
		 * The persistent store is not accessible;
		 * The schema for the persistent store is incompatible with current managed object model.
		 Check the error message to determine what the actual problem was.


		 If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.

		 If you encounter schema incompatibility errors during development, you can reduce their frequency by:
		 * Simply deleting the existing store:
		 [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]

		 * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
		 @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}

		 Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.

		 */
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		abort();
	}

	return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)documentsDirectory
{
	return [NSFileManager.defaultManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].lastObject;
}

- (NSURL *)applicationSupportDirectory
{
    return [NSFileManager.defaultManager URLsForDirectory:NSApplicationSupportDirectory inDomains:NSUserDomainMask].lastObject;
}



@end