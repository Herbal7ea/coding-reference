//
//  ChinookDBTests.m
//  ChinookDBTests
//
//  Created by jbott on 8/27/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DataManager.h"

@interface ChinookDBTests : XCTestCase

@end

@implementation ChinookDBTests

- (void)setUp
{
    [super setUp];

	[[DataManager sharedInstance] loadDatabase];

}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testNumberOfAlbums
{
	NSInteger numberOfAlbums = [DataManager.sharedInstance countAlbums];

	XCTAssertEqual( 347, numberOfAlbums);
}

- (void)testNumberofPlaylistTracks
{
	NSInteger numberofPlaylistTracks = [DataManager.sharedInstance countPlaylistTracks];

	XCTAssertEqual( 8715, numberofPlaylistTracks );
}

- (void)test_NumberOfITEmployees
{
	NSInteger numberOfITEmployees = [DataManager.sharedInstance countITEmployees];

	XCTAssertEqual( 3, numberOfITEmployees );
}

- (void)test_NumberOfITEmployees2
{
	NSInteger numberOfITEmployees = [DataManager.sharedInstance countITEmployees2];

	XCTAssertEqual( 3, numberOfITEmployees );
}

- (void)test_EntityCountOldSchool
{
	NSInteger numberOfITEmployees = [DataManager.sharedInstance countEntityCountOldSchool];

	XCTAssertEqual( 8715, numberOfITEmployees );
}

- (void)test_countITEmployeesOldSchool
{
	NSInteger numberOfITEmployees = [DataManager.sharedInstance countITEmployeesOldSchool];

	XCTAssertEqual( 3, numberOfITEmployees );
}

- (void)test_countEmployeesBornInThe70s
{
    NSInteger numberOfITEmployees = [DataManager.sharedInstance countEmployeesBornInThe70s];
    
    XCTAssertEqual( 3, numberOfITEmployees );
}

- (void)test_firstNamesOfEmployeesBornInThe70s
{
    NSArray *names = [DataManager.sharedInstance firstNamesOfEmployeesBornInThe70s];

    XCTAssertEqualObjects( names[0], @"Jane" );
    XCTAssertEqualObjects( names[1], @"Michael" );
    XCTAssertEqualObjects( names[2], @"Robert" );
}

- (void)test_firstNamesOfEmployeesBornInThe70sBundledFetchRequest
{
	NSArray *names = [DataManager.sharedInstance firstNamesOfEmployeesBornInThe70sBundledFetchRequest];

	XCTAssertEqualObjects( names[0], @"Jane" );
	XCTAssertEqualObjects( names[1], @"Michael" );
	XCTAssertEqualObjects( names[2], @"Robert" );
}

- (void)test_countPeopleWithTheNameOfZebraChalk
{
    NSInteger numberOfPeople = [DataManager.sharedInstance countPeopleWithTheNameOfZebraChalk_DoesNotPersistData];

    XCTAssertEqual( 1, numberOfPeople );
}

- (void)test_countPeopleWithTheNameOfZebraChalk_oldSchool
{
    NSInteger numberOfPeople = [DataManager.sharedInstance countPeopleWithTheNameOfZebraChalk_oldSchool];

    XCTAssertEqual( 1, numberOfPeople );
}

@end
