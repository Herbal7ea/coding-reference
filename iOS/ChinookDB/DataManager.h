#import <Foundation/Foundation.h>


@interface DataManager : NSObject

+ (DataManager *)sharedInstance;

- (void)cleanUp;

- (void)loadDatabase;

- (NSInteger)countAlbums;

- (NSInteger)countPlaylistTracks;

- (NSInteger)countITEmployees;

- (NSInteger)countITEmployees2;

- (NSInteger)countPeopleWithTheNameOfZebraChalk_PersistData;

- (NSInteger)countEntityCountOldSchool;

- (NSInteger)countITEmployeesOldSchool;

- (NSInteger)countEmployeesBornInThe70s;

- (NSArray *)firstNamesOfEmployeesBornInThe70s;

- (NSArray *)firstNamesOfEmployeesBornInThe70sBundledFetchRequest;

- (NSInteger)countPeopleWithTheNameOfZebraChalk_DoesNotPersistData;

- (NSInteger)countPeopleWithTheNameOfZebraChalk_oldSchool;
@end