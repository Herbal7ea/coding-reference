// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to GenreModel.m instead.

#import "_GenreModel.h"

const struct GenreModelAttributes GenreModelAttributes = {
	.name = @"name",
};

const struct GenreModelRelationships GenreModelRelationships = {
	.movies = @"movies",
};

const struct GenreModelFetchedProperties GenreModelFetchedProperties = {
};

@implementation GenreModelID
@end

@implementation _GenreModel

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"GenreModel" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"GenreModel";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"GenreModel" inManagedObjectContext:moc_];
}

- (GenreModelID*)objectID {
	return (GenreModelID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic name;






@dynamic movies;

	






@end
