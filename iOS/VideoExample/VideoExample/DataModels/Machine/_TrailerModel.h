// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrailerModel.h instead.

#import <CoreData/CoreData.h>


extern const struct TrailerModelAttributes {
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *url;
} TrailerModelAttributes;

extern const struct TrailerModelRelationships {
	__unsafe_unretained NSString *movies;
} TrailerModelRelationships;

extern const struct TrailerModelFetchedProperties {
} TrailerModelFetchedProperties;

@class MovieModel;




@interface TrailerModelID : NSManagedObjectID {}
@end

@interface _TrailerModel : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (TrailerModelID*)objectID;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* url;



//- (BOOL)validateUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) MovieModel *movies;

//- (BOOL)validateMovies:(id*)value_ error:(NSError**)error_;





@end

@interface _TrailerModel (CoreDataGeneratedAccessors)

@end

@interface _TrailerModel (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;




- (NSString*)primitiveUrl;
- (void)setPrimitiveUrl:(NSString*)value;





- (MovieModel*)primitiveMovies;
- (void)setPrimitiveMovies:(MovieModel*)value;


@end
