// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MovieModel.m instead.

#import "_MovieModel.h"

const struct MovieModelAttributes MovieModelAttributes = {
	.poster = @"poster",
	.rating = @"rating",
	.released = @"released",
	.summary = @"summary",
	.title = @"title",
	.url = @"url",
};

const struct MovieModelRelationships MovieModelRelationships = {
	.genres = @"genres",
	.trailers = @"trailers",
};

const struct MovieModelFetchedProperties MovieModelFetchedProperties = {
};

@implementation MovieModelID
@end

@implementation _MovieModel

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MovieModel" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MovieModel";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MovieModel" inManagedObjectContext:moc_];
}

- (MovieModelID*)objectID {
	return (MovieModelID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"releasedValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"released"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic poster;






@dynamic rating;






@dynamic released;



- (int16_t)releasedValue {
	NSNumber *result = [self released];
	return [result shortValue];
}

- (void)setReleasedValue:(int16_t)value_ {
	[self setReleased:[NSNumber numberWithShort:value_]];
}

- (int16_t)primitiveReleasedValue {
	NSNumber *result = [self primitiveReleased];
	return [result shortValue];
}

- (void)setPrimitiveReleasedValue:(int16_t)value_ {
	[self setPrimitiveReleased:[NSNumber numberWithShort:value_]];
}





@dynamic summary;






@dynamic title;






@dynamic url;






@dynamic genres;

	
- (NSMutableOrderedSet*)genresSet {
	[self willAccessValueForKey:@"genres"];
  
	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"genres"];
  
	[self didAccessValueForKey:@"genres"];
	return result;
}
	

@dynamic trailers;

	
- (NSMutableOrderedSet*)trailersSet {
	[self willAccessValueForKey:@"trailers"];
  
	NSMutableOrderedSet *result = (NSMutableOrderedSet*)[self mutableOrderedSetValueForKey:@"trailers"];
  
	[self didAccessValueForKey:@"trailers"];
	return result;
}
	






@end
