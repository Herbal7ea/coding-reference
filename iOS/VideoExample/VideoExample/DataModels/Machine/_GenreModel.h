// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to GenreModel.h instead.

#import <CoreData/CoreData.h>


extern const struct GenreModelAttributes {
	__unsafe_unretained NSString *name;
} GenreModelAttributes;

extern const struct GenreModelRelationships {
	__unsafe_unretained NSString *movies;
} GenreModelRelationships;

extern const struct GenreModelFetchedProperties {
} GenreModelFetchedProperties;

@class MovieModel;



@interface GenreModelID : NSManagedObjectID {}
@end

@interface _GenreModel : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (GenreModelID*)objectID;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) MovieModel *movies;

//- (BOOL)validateMovies:(id*)value_ error:(NSError**)error_;





@end

@interface _GenreModel (CoreDataGeneratedAccessors)

@end

@interface _GenreModel (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;





- (MovieModel*)primitiveMovies;
- (void)setPrimitiveMovies:(MovieModel*)value;


@end
