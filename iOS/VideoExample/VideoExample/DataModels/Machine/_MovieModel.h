// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MovieModel.h instead.

#import <CoreData/CoreData.h>


extern const struct MovieModelAttributes {
	__unsafe_unretained NSString *poster;
	__unsafe_unretained NSString *rating;
	__unsafe_unretained NSString *released;
	__unsafe_unretained NSString *summary;
	__unsafe_unretained NSString *title;
	__unsafe_unretained NSString *url;
} MovieModelAttributes;

extern const struct MovieModelRelationships {
	__unsafe_unretained NSString *genres;
	__unsafe_unretained NSString *trailers;
} MovieModelRelationships;

extern const struct MovieModelFetchedProperties {
} MovieModelFetchedProperties;

@class GenreModel;
@class TrailerModel;








@interface MovieModelID : NSManagedObjectID {}
@end

@interface _MovieModel : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MovieModelID*)objectID;





@property (nonatomic, strong) NSString* poster;



//- (BOOL)validatePoster:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* rating;



//- (BOOL)validateRating:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* released;



@property int16_t releasedValue;
- (int16_t)releasedValue;
- (void)setReleasedValue:(int16_t)value_;

//- (BOOL)validateReleased:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* summary;



//- (BOOL)validateSummary:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* url;



//- (BOOL)validateUrl:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSOrderedSet *genres;

- (NSMutableOrderedSet*)genresSet;




@property (nonatomic, strong) NSOrderedSet *trailers;

- (NSMutableOrderedSet*)trailersSet;





@end

@interface _MovieModel (CoreDataGeneratedAccessors)

- (void)addGenres:(NSOrderedSet*)value_;
- (void)removeGenres:(NSOrderedSet*)value_;
- (void)addGenresObject:(GenreModel*)value_;
- (void)removeGenresObject:(GenreModel*)value_;

- (void)addTrailers:(NSOrderedSet*)value_;
- (void)removeTrailers:(NSOrderedSet*)value_;
- (void)addTrailersObject:(TrailerModel*)value_;
- (void)removeTrailersObject:(TrailerModel*)value_;

@end

@interface _MovieModel (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitivePoster;
- (void)setPrimitivePoster:(NSString*)value;




- (NSString*)primitiveRating;
- (void)setPrimitiveRating:(NSString*)value;




- (NSNumber*)primitiveReleased;
- (void)setPrimitiveReleased:(NSNumber*)value;

- (int16_t)primitiveReleasedValue;
- (void)setPrimitiveReleasedValue:(int16_t)value_;




- (NSString*)primitiveSummary;
- (void)setPrimitiveSummary:(NSString*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;




- (NSString*)primitiveUrl;
- (void)setPrimitiveUrl:(NSString*)value;





- (NSMutableOrderedSet*)primitiveGenres;
- (void)setPrimitiveGenres:(NSMutableOrderedSet*)value;



- (NSMutableOrderedSet*)primitiveTrailers;
- (void)setPrimitiveTrailers:(NSMutableOrderedSet*)value;


@end
