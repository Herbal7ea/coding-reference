//
//  AppDelegate.h
//  PageView
//
//  Created by jbott on 3/26/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
