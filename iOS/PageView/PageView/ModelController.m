//
//  ModelController.m
//  Enabling Paging with UIPageViewController
//
//  Created by Vandad NP on 23/06/2013.
//  Copyright (c) 2013 Pixolity Ltd. All rights reserved.
//

#import "ModelController.h"

#import "DataViewController.h"

/*
 A controller object that manages a simple model -- a collection of month names.
 
 The controller serves as the data source for the page view controller; it therefore implements pageViewController:viewControllerBeforeViewController: and pageViewController:viewControllerAfterViewController:.
 It also implements a custom method, viewControllerAtIndex: which is useful in the implementation of the data source methods, and in the initial configuration of the application.
 
 There is no need to actually create view controllers for each page in advance -- indeed doing so incurs unnecessary overhead. Given the data model, these methods create, configure, and return a new view controller on demand.
 */

@interface ModelController ()

@property(readonly, strong, nonatomic) NSArray *pageData;

@end

@implementation ModelController

- (id)init
{
	self = [super init];
	if(self)
	{
		NSDateFormatter *dateFormatter = [NSDateFormatter new];
		_pageData = [dateFormatter.monthSymbols copy];
	}
	return self;
}

- (DataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard
{
	if(self.pageData.count == 0 || index >= self.pageData.count)
		return nil;

	DataViewController *dataViewController = [storyboard instantiateViewControllerWithIdentifier:@"DataViewController"];
	dataViewController.dataObject = self.pageData[index];
	return dataViewController;
}

- (NSUInteger)indexOfViewController:(DataViewController *)dataViewController
{
	return [self.pageData indexOfObject:dataViewController.dataObject];
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(DataViewController *)dataViewController
{
	NSUInteger index = [self indexOfViewController:dataViewController];
	if(index == 0 || index == NSNotFound)
		return nil;

	index--;
	return [self viewControllerAtIndex:index storyboard:dataViewController.storyboard];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(DataViewController *)dataViewController
{
	NSUInteger index = [self indexOfViewController:dataViewController];

	if(index == NSNotFound)
		return nil;

	index++;
	if(index == self.pageData.count)
		return nil;

	return [self viewControllerAtIndex:index storyboard:dataViewController.storyboard];
}

@end
