//
//  RootViewController.m
//  Enabling Paging with UIPageViewController
//
//  Created by Vandad NP on 23/06/2013.
//  Copyright (c) 2013 Pixolity Ltd. All rights reserved.
//

#import "RootViewController.h"

#import "ModelController.h"

#import "DataViewController.h"

@interface RootViewController ()

@property (readonly, strong, nonatomic) ModelController *modelController;

@end

@implementation RootViewController

@synthesize modelController = _modelController;

- (void)viewDidLoad
{
    [super viewDidLoad];

	[self presentExample];
}

- (void)presentExample
{
	CGRect pageViewRect = self.view.bounds;

	if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
		pageViewRect = CGRectInset(pageViewRect, 40.0, 40.0);


	DataViewController *startingViewController = [self.modelController viewControllerAtIndex:0 storyboard:self.storyboard];

	self.pageViewController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStylePageCurl navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
	self.pageViewController.delegate = self;
	self.pageViewController.dataSource = self.modelController;
	self.pageViewController.view.frame = pageViewRect;
	[self.pageViewController setViewControllers:@[startingViewController] direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:NULL];

	[self.pageViewController didMoveToParentViewController:self];
	[self addChildViewController:self.pageViewController];

	[self.view addSubview:self.pageViewController.view];

	//so that the gestures are started more easily.
//	self.view.gestureRecognizers = self.pageViewController.gestureRecognizers;
}

#pragma mark - UIPageViewController delegate methods

/*
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray *)previousViewControllers transitionCompleted:(BOOL)completed
{
    
}
 */

- (UIPageViewControllerSpineLocation)pageViewController:(UIPageViewController *)pageViewController spineLocationForInterfaceOrientation:(UIInterfaceOrientation)orientation
{

	//	In portrait orientation or on iPhone: Set the spine position to "min" and the page view controller's view controllers array to contain just one view controller.
	// Setting the spine position to 'UIPageViewControllerSpineLocationMid' in landscape orientation sets the doubleSided property to YES, so set it to NO here.
	// 	In landscape orientation: Set set the spine location to "mid" and the page view controller's view controllers array to contain two view controllers.
	// If the current page is even, set it to contain the current and next view controllers; if it is odd, set the array to contain the previous and current view controllers.

	NSArray *viewControllers = nil;
	UIViewController *currentViewController = self.pageViewController.viewControllers[0];
	UIPageViewControllerSpineLocation spineLocation;

	if (UIInterfaceOrientationIsPortrait(orientation) || UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
	{

		viewControllers = @[currentViewController];
        self.pageViewController.doubleSided = NO;
		spineLocation = UIPageViewControllerSpineLocationMin;
    }
	else
	{

		if ( [self isZeroOrEven:currentViewController] )
		{
			UIViewController *nextViewController = [self.modelController pageViewController:self.pageViewController viewControllerAfterViewController:currentViewController];
			viewControllers = @[currentViewController, nextViewController];
		}
		else
		{
			UIViewController *previousViewController = [self.modelController pageViewController:self.pageViewController viewControllerBeforeViewController:currentViewController];
			viewControllers = @[previousViewController, currentViewController];
		}

		spineLocation = UIPageViewControllerSpineLocationMid;
	}


	[self.pageViewController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:NULL];
	return spineLocation;
}

#pragma mark - helper methods

- (ModelController *)modelController
{
	if (!_modelController)
		_modelController = [ModelController new];

	return _modelController;
}

- (BOOL)isZeroOrEven:(UIViewController *)currentViewController
{
	NSUInteger indexOfCurrentViewController = [self.modelController indexOfViewController:currentViewController];

	return indexOfCurrentViewController == 0 || indexOfCurrentViewController % 2 == 0;
}

#pragma mark - system methods

- (BOOL)prefersStatusBarHidden
{
	return YES;
}

@end
