//
//  ViewController.m
//  DTRichTextEditorExample
//
//  Created by Jon Bott on 8/3/15.
//  Copyright (c) 2015 jonbott.com. All rights reserved.
//

#import "ViewController.h"
#import "DTRichTextEditorView.h"
#import <DTCoreText/DTCoreText.h>
#import <DTRichTextEditor/DTRichTextEditor.h>

@interface ViewController ()
{
    NSCache *_imageViewCache;
}

@property (weak, nonatomic) IBOutlet DTRichTextEditorView *richEditor;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.richEditor.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.richEditor.textDelegate = self;

    NSString *cssStyle = @"p {margin-bottom:0.5em} ol {margin-bottom:0.5em; -webkit-padding-start:40px;} ul {margin-bottom:0.5em;-webkit-padding-start:40px;}";

    DTCSSStylesheet *styleSheet = [[DTCSSStylesheet alloc] initWithStyleBlock:cssStyle];
    DTColor *dtColor = DTColorCreateWithHTMLName(@"purple");

    NSDictionary *defaults = @{
                               DTDefaultLinkDecoration: @YES,
                               DTProcessCustomHTMLAttributes: @YES,
                               DTDefaultStyleSheet: styleSheet,
                               DTDefaultLinkColor: dtColor
                               };


    // load initial string from file
    NSString *path = [[NSBundle mainBundle] pathForResource:@"test" ofType:@"html"];
    NSString *html = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:NULL];

    // defaults
    self.richEditor.baseURL = [NSURL URLWithString:@"http://www.drobnik.com"];
    self.richEditor.editable = NO;
    self.richEditor.textDelegate = self;
    self.richEditor.textDefaults = defaults;
    self.richEditor.defaultFontSize = 20;
    self.richEditor.defaultFontFamily = @"Helvetica";
    self.richEditor.autocorrectionType = UITextAutocorrectionTypeYes;
    self.richEditor.textSizeMultiplier = 1.0;
    self.richEditor.maxImageDisplaySize = CGSizeMake(300, 300);

    [self.richEditor setHTMLString:html];

    // image as drawn by your custom views which you return in the delegate method
    self.richEditor.attributedTextContentView.shouldDrawImages = NO;
}

#pragma mark - DTAttributedTextContentViewDelegate

- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView viewForAttachment:(DTTextAttachment *)attachment frame:(CGRect)frame
{
    NSNumber *cacheKey = [NSNumber numberWithUnsignedInteger:[attachment hash]];

    UIImageView *imageView = [self.imageViewCache objectForKey:cacheKey];

    if (imageView)
    {
        imageView.frame = frame;
        return imageView;
    }

    if ([attachment isKindOfClass:[DTImageTextAttachment class]])
    {
        DTImageTextAttachment *imageAttachment = (DTImageTextAttachment *)attachment;

        imageView = [[UIImageView alloc] initWithFrame:frame];
        imageView.image = imageAttachment.image;

        [self.imageViewCache setObject:imageView forKey:cacheKey];

        return imageView;
    }


    return nil;
}

- (UIView *)attributedTextContentView:(DTAttributedTextContentView *)attributedTextContentView viewForLink:(NSURL *)url identifier:(NSString *)identifier frame:(CGRect)frame
{
    DTLinkButton *button = [[DTLinkButton alloc] initWithFrame:frame];
                  button.URL = url;
                  button.GUID = identifier;
                  button.minimumHitSize = CGSizeMake(25, 25); // adjusts it's bounds so that button is always large enough

    [button addTarget:self action:@selector(linkPushed:) forControlEvents:UIControlEventTouchUpInside];

    // demonstrate combination with long press
    //UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(linkLongPressed:)];
    //[button addGestureRecognizer:longPress];

    return button;
}

- (void)linkPushed:(DTLinkButton *)sender
{
    NSLog(@"link pushed: %@", sender.URL);
    // do something when a link was pushed
}



#pragma mark Properties

- (NSCache *)imageViewCache
{
    if (!_imageViewCache)
    {
        _imageViewCache = [[NSCache alloc] init];
    }

    return _imageViewCache;
}

@end
