//
//  ViewController.h
//  DTRichTextEditorExample
//
//  Created by Jon Bott on 8/3/15.
//  Copyright (c) 2015 jonbott.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "DTRichTextEditorView.h"


@interface ViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, DTAttributedTextContentViewDelegate>


@end

