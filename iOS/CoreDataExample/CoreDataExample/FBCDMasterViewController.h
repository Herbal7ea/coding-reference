//
//  FBCDMasterViewController.h
//  FailedBankCD2
//
//  Created by jbott on 4/17/13.
//  Copyright (c) 2013 Chameleon Software. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FBCDMasterViewController : UITableViewController

@property(nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property(nonatomic, strong) NSArray *failedBankInfos;

@end
