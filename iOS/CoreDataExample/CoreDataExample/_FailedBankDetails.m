// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FailedBankDetails.m instead.

#import "_FailedBankDetails.h"

const struct FailedBankDetailsAttributes FailedBankDetailsAttributes = {
	.closeDate = @"closeDate",
	.updateDate = @"updateDate",
	.zip = @"zip",
};

const struct FailedBankDetailsRelationships FailedBankDetailsRelationships = {
	.info = @"info",
};

const struct FailedBankDetailsFetchedProperties FailedBankDetailsFetchedProperties = {
};

@implementation FailedBankDetailsID
@end

@implementation _FailedBankDetails

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"FailedBankDetails" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"FailedBankDetails";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"FailedBankDetails" inManagedObjectContext:moc_];
}

- (FailedBankDetailsID*)objectID {
	return (FailedBankDetailsID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"zipValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"zip"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic closeDate;






@dynamic updateDate;






@dynamic zip;



- (int32_t)zipValue {
	NSNumber *result = [self zip];
	return [result intValue];
}

- (void)setZipValue:(int32_t)value_ {
	[self setZip:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveZipValue {
	NSNumber *result = [self primitiveZip];
	return [result intValue];
}

- (void)setPrimitiveZipValue:(int32_t)value_ {
	[self setPrimitiveZip:[NSNumber numberWithInt:value_]];
}





@dynamic info;

	






@end
