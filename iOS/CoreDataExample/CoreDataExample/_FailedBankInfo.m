// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FailedBankInfo.m instead.

#import "_FailedBankInfo.h"

const struct FailedBankInfoAttributes FailedBankInfoAttributes = {
	.city = @"city",
	.name = @"name",
	.state = @"state",
};

const struct FailedBankInfoRelationships FailedBankInfoRelationships = {
	.details = @"details",
};

const struct FailedBankInfoFetchedProperties FailedBankInfoFetchedProperties = {
};

@implementation FailedBankInfoID
@end

@implementation _FailedBankInfo

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"FailedBankInfo" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"FailedBankInfo";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"FailedBankInfo" inManagedObjectContext:moc_];
}

- (FailedBankInfoID*)objectID {
	return (FailedBankInfoID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic city;






@dynamic name;






@dynamic state;






@dynamic details;

	






@end
