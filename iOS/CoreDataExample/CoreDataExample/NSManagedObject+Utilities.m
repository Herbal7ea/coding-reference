#import "NSManagedObject+Utilities.h"


@implementation NSManagedObject (Utilities)

//- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key {
//	[self setValue:obj forKeyPath:key];
//}

- (id)objectForKeyedSubscript:(id)key {
	return [self valueForKey:key];
}

- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key {
	[self setObject:obj forKeyedSubscript:key];
}

@end