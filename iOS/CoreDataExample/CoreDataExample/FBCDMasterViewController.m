//
//  FBCDMasterViewController.m
//  FailedBankCD2
//
//  Created by jbott on 4/17/13.
//  Copyright (c) 2013 Chameleon Software. All rights reserved.
//

#import "FBCDMasterViewController.h"
#import "FailedBankInfo.h"

@interface FBCDMasterViewController ()

@end

@implementation FBCDMasterViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.title = @"Failed Banks";

	//	NSFetchRequest *fetchRequest = [NSFetchRequest new];
	//	fetchRequest.entity = [NSEntityDescription entityForName:@"FailedBankInfo" inManagedObjectContext:self.managedObjectContext];

	//		NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"FailedBankInfo"];


	NSError *error;
	NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FailedBankInfo"];

	self.failedBankInfos = [self.managedObjectContext executeFetchRequest:fetchRequest error:&error];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return self.failedBankInfos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	FailedBankInfo *info = self.failedBankInfos[indexPath.row];

	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
	cell.textLabel.text = info.name;
	cell.detailTextLabel.text = [NSString stringWithFormat:@"%@, %@", info.city, info.state];

	return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	// Navigation logic may go here. Create and push another view controller.
	/*
	 <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
	 // ...
	 // Pass the selected object to the new view controller.
	 [self.navigationController pushViewController:detailViewController animated:YES];
	 */
}

@end
