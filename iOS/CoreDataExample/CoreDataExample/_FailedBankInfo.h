// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FailedBankInfo.h instead.

#import <CoreData/CoreData.h>


extern const struct FailedBankInfoAttributes {
	__unsafe_unretained NSString *city;
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *state;
} FailedBankInfoAttributes;

extern const struct FailedBankInfoRelationships {
	__unsafe_unretained NSString *details;
} FailedBankInfoRelationships;

extern const struct FailedBankInfoFetchedProperties {
} FailedBankInfoFetchedProperties;

@class FailedBankDetails;





@interface FailedBankInfoID : NSManagedObjectID {}
@end

@interface _FailedBankInfo : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FailedBankInfoID*)objectID;





@property (nonatomic, strong) NSString* city;



//- (BOOL)validateCity:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* state;



//- (BOOL)validateState:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) FailedBankDetails *details;

//- (BOOL)validateDetails:(id*)value_ error:(NSError**)error_;





@end

@interface _FailedBankInfo (CoreDataGeneratedAccessors)

@end

@interface _FailedBankInfo (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveCity;
- (void)setPrimitiveCity:(NSString*)value;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSString*)primitiveState;
- (void)setPrimitiveState:(NSString*)value;





- (FailedBankDetails*)primitiveDetails;
- (void)setPrimitiveDetails:(FailedBankDetails*)value;


@end
