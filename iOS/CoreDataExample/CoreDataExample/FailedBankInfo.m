//
//  FailedBankInfo.m
//  CoreDataExample
//
//  Created by jbott on 3/28/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "FailedBankInfo.h"


@implementation FailedBankInfo

@dynamic city;
@dynamic name;
@dynamic state;
@dynamic details;

@end
