// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to FailedBankDetails.h instead.

#import <CoreData/CoreData.h>


extern const struct FailedBankDetailsAttributes {
	__unsafe_unretained NSString *closeDate;
	__unsafe_unretained NSString *updateDate;
	__unsafe_unretained NSString *zip;
} FailedBankDetailsAttributes;

extern const struct FailedBankDetailsRelationships {
	__unsafe_unretained NSString *info;
} FailedBankDetailsRelationships;

extern const struct FailedBankDetailsFetchedProperties {
} FailedBankDetailsFetchedProperties;

@class FailedBankInfo;





@interface FailedBankDetailsID : NSManagedObjectID {}
@end

@interface _FailedBankDetails : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (FailedBankDetailsID*)objectID;





@property (nonatomic, strong) NSDate* closeDate;



//- (BOOL)validateCloseDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* updateDate;



//- (BOOL)validateUpdateDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* zip;



@property int32_t zipValue;
- (int32_t)zipValue;
- (void)setZipValue:(int32_t)value_;

//- (BOOL)validateZip:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) FailedBankInfo *info;

//- (BOOL)validateInfo:(id*)value_ error:(NSError**)error_;





@end

@interface _FailedBankDetails (CoreDataGeneratedAccessors)

@end

@interface _FailedBankDetails (CoreDataGeneratedPrimitiveAccessors)


- (NSDate*)primitiveCloseDate;
- (void)setPrimitiveCloseDate:(NSDate*)value;




- (NSDate*)primitiveUpdateDate;
- (void)setPrimitiveUpdateDate:(NSDate*)value;




- (NSNumber*)primitiveZip;
- (void)setPrimitiveZip:(NSNumber*)value;

- (int32_t)primitiveZipValue;
- (void)setPrimitiveZipValue:(int32_t)value_;





- (FailedBankInfo*)primitiveInfo;
- (void)setPrimitiveInfo:(FailedBankInfo*)value;


@end
