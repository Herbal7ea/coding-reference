//
//  FailedBankDetails.m
//  CoreDataExample
//
//  Created by jbott on 3/28/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "FailedBankDetails.h"
#import "FailedBankInfo.h"


@implementation FailedBankDetails

@dynamic closeDate;
@dynamic updateDate;
@dynamic zip;
@dynamic info;

@end
