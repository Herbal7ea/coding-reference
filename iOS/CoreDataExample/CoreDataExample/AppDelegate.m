//
//  AppDelegate.m
//  CoreDataExample
//
//  Created by jbott on 3/28/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "AppDelegate.h"

#import "FBCDMasterViewController.h"
#import "FailedBankInfo.h"
#import "FailedBankDetails.h"
#import "NSManagedObject+Utilities.h"

@implementation AppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	[self setUpNav];

	return YES;
}

- (void)setUpNav
{
	// Override point for customization after application launch.
	UINavigationController *navigationController = (UINavigationController *) self.window.rootViewController;
	FBCDMasterViewController *controller = (FBCDMasterViewController *) navigationController.topViewController;
	controller.managedObjectContext = self.managedObjectContext;

	//	[self addCoreDataStuff_weaklyTyped];
	//	[self addCoreDataStuff_stronglyTyped];
	[self addCoreDataStuff_stronglyTyped2];
}

- (void)addCoreDataStuff_stronglyTyped2
{
	NSManagedObjectContext *context = self.managedObjectContext;
	FailedBankInfo *failedBankInfo 	     = [NSEntityDescription insertNewObjectForEntityForName:@"FailedBankInfo"    inManagedObjectContext:context];
	FailedBankDetails *failedBankDetails = [NSEntityDescription insertNewObjectForEntityForName:@"FailedBankDetails" inManagedObjectContext:context];

	failedBankInfo.name = @"Test Bank";
	failedBankInfo.city = @"Testville";
	failedBankInfo.state = @"Testland";
	failedBankInfo.details = failedBankDetails;

	failedBankDetails.info = failedBankInfo;
	failedBankDetails.closeDate = NSDate.date;
	failedBankDetails.updateDate = NSDate.date;
	failedBankDetails.zip = @(arc4random() % 12345);//@12345;

	NSError *error;

	if(![context save:&error])
	{
		NSLog(@"Whoops, couldn't save: %@", error.localizedDescription);
	}


	//	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	//	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FailedBankInfo" inManagedObjectContext:context];
	//	fetchRequest.entity = entity;

	[self fetchAndLogData:context];

}

- (void)fetchAndLogData:(NSManagedObjectContext *)context
{
	NSError *error;

	NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"FailedBankInfo"];
    //fetchRequest.predicate = [NSPredicate predicateWithFormat:@"details.zip = 5470 || details.zip = 10955"];;

	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
	for(FailedBankInfo *info in fetchedObjects)
	{
		FailedBankDetails *details = info.details;
		NSLog(@"Name: %@", info.name);
		NSLog(@"Zip: %@", details.zip);
	}
}

- (void)addCoreDataStuff_stronglyTyped
{
	NSManagedObjectContext *context = [self managedObjectContext];
	FailedBankInfo *failedBankInfo       = [NSEntityDescription insertNewObjectForEntityForName:@"FailedBankInfo"    inManagedObjectContext:context];
	FailedBankDetails *failedBankDetails = [NSEntityDescription insertNewObjectForEntityForName:@"FailedBankDetails" inManagedObjectContext:context];

	failedBankInfo.name = @"Test Bank";
	failedBankInfo.city = @"Testville";
	failedBankInfo.state = @"Testland";
	failedBankInfo.details = failedBankDetails;

	failedBankDetails.info = failedBankInfo;
	failedBankDetails.closeDate  = NSDate.date;
	failedBankDetails.updateDate = NSDate.date;
	failedBankDetails.zip = @(arc4random_uniform(12345));//@(arc4random() % 12345);//[NSNumber numberWithInt:12345];

	NSError *error;
	if(![context save:&error])
	{
		NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
	}

	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FailedBankInfo" inManagedObjectContext:context];
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	fetchRequest.entity = entity;

	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];

	for(FailedBankInfo *info in fetchedObjects)
	{

		FailedBankDetails *details = info.details;
		NSLog(@"Name: %@", info.name);
		NSLog(@"Zip: %@", details.zip);
	}
}

- (void)addCoreDataStuff_weaklyTyped
{
	NSManagedObjectContext *context = self.managedObjectContext;
	NSManagedObject *failedBankInfo    = [NSEntityDescription insertNewObjectForEntityForName:@"FailedBankInfo"    inManagedObjectContext:context];
	NSManagedObject *failedBankDetails = [NSEntityDescription insertNewObjectForEntityForName:@"FailedBankDetails" inManagedObjectContext:context];

	failedBankDetails[@"closeDate"] = [NSDate date];
	failedBankDetails[@"updateDate"] = [NSDate date];
	failedBankDetails[@"zip"] = [NSNumber numberWithInt:12345];
	failedBankDetails[@"info"] = failedBankInfo;

	failedBankInfo[@"details"] = failedBankDetails;
	failedBankInfo[@"name"] = @"Test Bank";
	failedBankInfo[@"city"] = @"TestVille";
	failedBankInfo[@"name"] = @"Test Bank";
	failedBankInfo[@"state"] = @"Testland";


	NSError *error;
	if(![context save:&error])
	{
		NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
	}

	NSEntityDescription *entity = [NSEntityDescription entityForName:@"FailedBankInfo" inManagedObjectContext:context];

	NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
	fetchRequest.entity = entity;

	NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
	for(NSManagedObject *info in fetchedObjects)
	{

		NSManagedObject *details = info[@"details"];
		NSLog(@"Name: %@", info[@"name"]);
		NSLog(@"Zip: %@", details[@"zip"]);
	}
}

- (void)saveContext
{
	NSError *error = nil;
	NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
	if(managedObjectContext != nil)
	{
		if([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
		{
			// Replace this implementation with code to handle the error appropriately.
			// abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
			NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
			abort();
		}
	}
}

#pragma mark - Core Data stack

- (NSManagedObjectContext *)managedObjectContext
{
	if(_managedObjectContext != nil)
		return _managedObjectContext;

	NSPersistentStoreCoordinator *coordinator = self.persistentStoreCoordinator;

	if(coordinator != nil)
	{
		_managedObjectContext = [NSManagedObjectContext new];
		_managedObjectContext.persistentStoreCoordinator = coordinator;
	}

	return _managedObjectContext;
}

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
	if(_persistentStoreCoordinator != nil)
		return _persistentStoreCoordinator;

	NSError *error = nil;
	NSURL *storeURL = [self.applicationDocumentsDirectory URLByAppendingPathComponent:@"FailedBankCD01.sqlite"];
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.managedObjectModel];

	if(![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
	{
		/*
		 Replace this implementation with code to handle the error appropriately.

		 abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.

		 Typical reasons for an error here include:
		 * The persistent store is not accessible;
		 * The schema for the persistent store is incompatible with current managed object model.
		 Check the error message to determine what the actual problem was.


		 If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.

		 If you encounter schema incompatibility errors during development, you can reduce their frequency by:
		 * Simply deleting the existing store:
		 [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]

		 * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
		 @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}

		 Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.

		 */
		NSLog(@"Unresolved error %@, %@", error, error.userInfo);
		abort();
	}

	return _persistentStoreCoordinator;
}

- (NSManagedObjectModel *)managedObjectModel
{
	if(_managedObjectModel != nil)
		return _managedObjectModel;

	NSURL *modelURL = [NSBundle.mainBundle URLForResource:@"FailedBank" withExtension:@"momd"];
	_managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];

	return _managedObjectModel;
}

#pragma mark - Application's Documents directory

- (NSURL *)applicationDocumentsDirectory
{
	return [NSFileManager.defaultManager URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask].lastObject;
}


#pragma mark - System Events
- (void)applicationWillResignActive:(UIApplication *)application
{
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
