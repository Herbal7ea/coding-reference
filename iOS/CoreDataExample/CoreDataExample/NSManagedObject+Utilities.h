#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface NSManagedObject (Utilities)

- (void)setObject:(id)obj forKeyedSubscript:(id <NSCopying>)key;

- (id)objectForKeyedSubscript:(id)key;

@end