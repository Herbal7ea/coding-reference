//
//  UnitTestsTests.m
//  UnitTestsTests
//
//  Created by jbott on 9/4/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import <XCTest/XCTest.h>

bool someMethod();

@interface UnitTestsTests : XCTestCase

@end

@implementation UnitTestsTests

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}




- (void)testExample
{
	//    XCTFail(@"No implementation for \"%s\"", __PRETTY_FUNCTION__);

	XCTAssert( someMethod( ), @"should be true" );
	XCTAssertTrue( true, "is false" );
	XCTAssertFalse( false, "is true" );
	XCTAssertEqual( 1 + 1, 2, @"one plus one should equal two" );
	XCTAssertNotEqual( 1 + 2, 2, @"one plus two should equal three" );


	float f = 1.3210;
	double d = 1.3212;
	XCTAssertNotEqualWithAccuracy( f, d, .0002 ); //within .0003 difference
	XCTAssertEqualWithAccuracy( f, d, .0003 ); //within .0003 difference


	NSString *s1 = nil;
	NSString *s2;
	NSString *s3 = @"";

	XCTAssertNil( s1 );
	XCTAssertNil( s2 );
	XCTAssertNotNil( s3 );


	[self equalVsEqualObjects];
}

- (void)equalVsEqualObjects
{
	NSString *s1 = @"aaa";
	NSString *s1b = s1;
	NSString *s2 = @"aaa";
	NSString *s3 = @"bbb";

	XCTAssertEqual(s1, s1b);
	XCTAssertEqual(s1, s2);

	XCTAssertEqualObjects( s1, s1b, @"The string should be the same" );
	XCTAssertEqualObjects( s1, s2, @"The string should be the same" );
	XCTAssertNotEqualObjects( s1, s3, @"The string should be the same" );
}

@end


bool someMethod()
{
	return true;
}