//
//  SPAppDelegate.h
//  UnitTests
//
//  Created by jbott on 9/4/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SPAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
