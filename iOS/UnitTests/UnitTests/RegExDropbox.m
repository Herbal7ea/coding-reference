//
//  RegExDropbox.m
//  UnitTests
//
//  Created by Jon Bott on 10/13/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "RegExCategories.h"

@interface RegExDropbox : XCTestCase

@end

@implementation RegExDropbox

- (void)setUp {
    [super setUp];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)test_RegexForDropboxUrl
{

    NSString *startingUrl = @"https://www.dropbox.com/s/qxkd1957qf7iq9x/04%20-%20Test%20Document.doc?dl=0";

    NSString *expectedUrl = @"https://www.dropbox.com/s/qxkd1957qf7iq9x/04%20-%20Test%20Document.doc?dl=1";

    NSURL *url = [NSURL URLWithString:startingUrl];
    NSURL *finalUrl = url;

    NSString *regexForDropbox = @".*dropbox\\.com.*\\?.*dl=0.*";

    NSLog(@"z url: %@", url );

    BOOL isMatch = [url.absoluteString isMatch:RX( regexForDropbox )];
    XCTAssert( isMatch );

    if( isMatch )
    {
        NSString *modifiedPath = [NSString stringWithFormat:@"%@?%@", url.path, @"dl=1"];
        finalUrl = [[NSURL alloc] initWithScheme:url.scheme host:url.host path:modifiedPath];
    }

    NSLog(@"z url: %@", url );

    XCTAssertEqualObjects(expectedUrl, finalUrl.absoluteString);
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
        // Put the code you want to measure the time of here.
    }];
}

@end
