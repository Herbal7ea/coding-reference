//
//  BAAnnotation.h
//  CoreLocationAdvisor
//
//  Created by jbott on 8/26/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface BAAnnotation : NSObject <MKAnnotation>

@property(nonatomic) CLLocationCoordinate2D coordinate;

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end
