//
//  main.m
//  CoreLocationAdvisor
//
//  Created by jbott on 8/26/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SPAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SPAppDelegate class]));
    }
}
