//
//  SPViewController.m
//  CoreLocationAdvisor
//
//  Created by jbott on 8/26/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

#import "SPViewController.h"
#import "BAAnnotation.h"


@interface SPViewController () <CLLocationManagerDelegate>
{
	IBOutlet MKMapView *map;
	IBOutlet UILabel *locationLabel;
	IBOutlet UILabel *addressLabel;
	CLLocationManager *manager;
}

@end


@implementation SPViewController
- (void)viewDidLoad
{
	[super viewDidLoad];

	manager = [[CLLocationManager alloc] init];
	manager.delegate = self;

	[manager startUpdatingLocation];
}

- (void)viewDidUnload
{
	[manager stopUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	if( newLocation.coordinate.latitude != oldLocation.coordinate.latitude )
	{
		[self revGeocode2];
	}
}

- (void)revGeocode:(CLLocation *)location
{
	addressLabel.text = @"reverse geocoding coordinate ...";

	CLGeocoder *geocoder = [[CLGeocoder alloc] init];

	[geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
		CLPlacemark *revMark = placemarks[ 0 ];
		NSArray *addressLines = revMark.addressDictionary[ @"FormattedAddressLines" ];
		NSString *revAddress = [addressLines componentsJoinedByString:@"\n"];

		addressLabel.text = [NSString stringWithFormat:@"Reverse geocoded address: \n%@", revAddress];

		[self geocode:revAddress];
	}];
}

- (void)revGeocode2
{
	addressLabel.text = @"reverse geocoding coordinate ...";

//	CLLocation *glassesDotComLocation = [[CLLocation alloc] initWithLatitude:40.50113892842447 longitude:-111.88949704170227];
	CLLocation *glassesDotComLocation = [[CLLocation alloc] initWithLatitude:53.4167 longitude:-3];

	CLGeocoder *geocoder = [[CLGeocoder alloc] init];

	[geocoder reverseGeocodeLocation:glassesDotComLocation completionHandler:^(NSArray *placemarks, NSError *error) {

		CLPlacemark *revMark = placemarks[ 0 ];
		NSArray *addressLines = revMark.addressDictionary[ @"FormattedAddressLines" ];
		NSString *revAddress = [addressLines componentsJoinedByString:@"\n"];

		addressLabel.text = [NSString stringWithFormat:@"Reverse geocoded address: \n%@", revAddress];

		[self geocode:revAddress];
	}];
}

- (void)geocode:(NSString *)address
{
	locationLabel.text = @"geocoding address...";
	CLGeocoder *gc = [[CLGeocoder alloc] init];

	[gc geocodeAddressString:address completionHandler:^(NSArray *placemarks, NSError *error) {
		if( placemarks.count > 0 )
		{
			CLPlacemark *mark = (CLPlacemark *) placemarks[ 0 ];
			double lat = mark.location.coordinate.latitude;
			double lng = mark.location.coordinate.longitude;

			locationLabel.text = [NSString stringWithFormat:@"Cordinate\nlat: %@, long: %@", @(lat), @(lng)];

			CLLocationCoordinate2D coordinate;
			coordinate.latitude = lat;
			coordinate.longitude = lng;

			MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(coordinate, 1000, 1000);
			MKCoordinateRegion adjustedRegion = [map regionThatFits:viewRegion];

			[map addAnnotation:[[BAAnnotation alloc] initWithCoordinate:coordinate] ];
			[map setRegion:adjustedRegion animated:YES];
		}
	}];
}

@end












