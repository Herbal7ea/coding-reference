//
//  BAAnnotation.m
//  CoreLocationAdvisor
//
//  Created by jbott on 8/26/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import "BAAnnotation.h"

@implementation BAAnnotation

- (id)initWithCoordinate:(CLLocationCoordinate2D)coordinate
{
	if( self = [super init] )
	{
		self.coordinate = coordinate;
	}

	return self;
}

@end