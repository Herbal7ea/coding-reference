//
//  NavController.h
//  DynamicUI
//
//  Created by jbott on 3/13/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavController : UINavigationController

@end
