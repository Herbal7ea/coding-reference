//
//  UIPickerView_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/19/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UIPickerView_Controller.h"

@interface UIPickerView_Controller ()<UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, strong) UIPickerView *myPicker;

@end

@implementation UIPickerView_Controller



- (void)viewDidLoad{
	[super viewDidLoad];
	self.view.backgroundColor = UIColor.whiteColor;

	[self presentExamples];
}

- (void)presentExamples
{
	[self simplePicker];
}

- (void)simplePicker
{
	self.myPicker = [UIPickerView new];
	self.myPicker.showsSelectionIndicator = NO;
	self.myPicker.dataSource = self;
	self.myPicker.delegate = self;
	self.myPicker.center = self.view.center;

	[self.view addSubview:self.myPicker];
}


#pragma mark - datasource

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
	if( pickerView == self.myPicker )
	{
		/* Row is zero-based and we want the first row (with index 0)
		 to be rendered as Row 1 so we have to +1 every row index */
		return [NSString stringWithFormat:@"Row %ld", (long)row + 1];
	}

	return nil;
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
	return pickerView == self.myPicker;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{

	if( pickerView == self.myPicker )
	{
		return 10;
	}

	return 0;
}

#pragma mark - events

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
	if( pickerView == self.myPicker )
	{
		NSLog(@"selected row#: [%d, %d]", row, component);
	}
}


@end
