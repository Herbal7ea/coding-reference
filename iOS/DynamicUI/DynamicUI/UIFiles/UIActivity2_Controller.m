//
//  UIActivity2_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/28/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UIActivity2_Controller.h"

@interface UIActivity2_Controller () <UITextFieldDelegate>

@property(nonatomic, strong) UITextField *textField;
@property(nonatomic, strong) UIButton *buttonShare;
@property(nonatomic, strong) UIActivityViewController *activityViewController;

@end

@implementation UIActivity2_Controller

- (void)viewDidLoad
{

	[super viewDidLoad];
	[self createTextField];
	[self createButton];

}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}

#pragma mark - private methods

- (void)createTextField
{
	self.textField = [[UITextField alloc] initWithFrame:CGRectMake(20, 35, 280, 30)];
	self.textField.translatesAutoresizingMaskIntoConstraints = NO;
	self.textField.borderStyle = UITextBorderStyleRoundedRect;
	self.textField.placeholder = @"Enter text to share...";
	self.textField.delegate = self;

	[self.view addSubview:self.textField];
}

- (void)createButton
{
	self.buttonShare = [UIButton buttonWithType:UIButtonTypeRoundedRect];
	self.buttonShare.translatesAutoresizingMaskIntoConstraints = NO;
	self.buttonShare.frame = CGRectMake(20, 80, 280, 44);

	[self.buttonShare setTitle:@"Share" forState:UIControlStateNormal];
	[self.buttonShare addTarget:self action:@selector(handleShare:) forControlEvents:UIControlEventTouchUpInside];

	[self.view addSubview:self.buttonShare];
}

- (void)handleShare:(id)paramSender
{
	if(self.textField.text.length == 0)
	{
		[[[UIAlertView alloc] initWithTitle:nil
									message:@"Please enter a text and then press Share"
								   delegate:nil
						  cancelButtonTitle:@"OK"
						  otherButtonTitles:nil] show];
	}
	else
	{
		self.activityViewController = [[UIActivityViewController alloc] initWithActivityItems:@[self.textField.text] applicationActivities:nil];

		[self presentViewController:self.activityViewController animated:YES completion:nil];
	}
}

@end
