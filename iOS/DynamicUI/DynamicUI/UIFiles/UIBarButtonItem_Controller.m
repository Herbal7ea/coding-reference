//
//  UIBarButtonItem_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/14/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//


#warning This class must be created in the AppDelegate


#import "UIBarButtonItem_Controller.h"

@interface UIBarButtonItem_Controller ()

@end

@implementation UIBarButtonItem_Controller

- (void)viewDidLoad{
	[super viewDidLoad];

	self.view.backgroundColor = UIColor.whiteColor;
	self.title = @"First Controller";

	[self presentExamples];
}

- (void)presentExamples
{
//	[self createBarButtonWithText];
//	[self createBarButtonWithSystemItems];
	[self createBarButtonWithSwitch];
//	[self createBarButtonWithSegmentedControl];
//	[self createBarButtonWithSegmentedControlAndAnimation];
}



#pragma mark - example 1 (includes UISegmentedControl)

- (void)createBarButtonWithSegmentedControl{
	NSArray *items = @[
			@"Up",
			@"Down"
	];

	UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:items];
	segmentedControl.momentary = YES;
	[segmentedControl addTarget:self action:@selector(segmentedControlTapped:) forControlEvents:UIControlEventValueChanged];

	UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:segmentedControl];

	self.navigationItem.rightBarButtonItem = rightBarButton;
}


- (void)createBarButtonWithSegmentedControlAndAnimation
{
	NSArray *items = @[
						@"Up",
						@"Down"
					  ];

	UISegmentedControl *segmentedControl = [[UISegmentedControl alloc] initWithItems:items];
	segmentedControl.momentary = NO;//YES;
	[segmentedControl addTarget:self action:@selector(segmentedControlTapped:) forControlEvents:UIControlEventValueChanged];


	UIBarButtonItem *rightBarButton = [[UIBarButtonItem alloc] initWithCustomView:segmentedControl];

	[self.navigationItem setRightBarButtonItem:rightBarButton animated:YES];
}



- (void) segmentedControlTapped:(UISegmentedControl *)paramSender
{
	switch ( paramSender.selectedSegmentIndex )
	{
		case 0:
				NSLog(@"Up");
			break;
		case 1:
				NSLog(@"Down");
			break;
	}
}


#pragma mark - example 2 (simple buttons)

- (void)createBarButtonWithText
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Add" style:UIBarButtonItemStylePlain target:self action:@selector(performAdd:)];
}

- (void)createBarButtonWithSystemItems
{
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(performAdd:)];
}


- (void) performAdd:(id)paramSender
{
	NSLog(@"Action method got called.");
}

#pragma mark - example 3 (with UISwitch)

- (void)createBarButtonWithSwitch
{
	UISwitch *simpleSwitch = [[UISwitch alloc] init];
	simpleSwitch.on = YES;
	[simpleSwitch addTarget:self action:@selector(switchIsChanged:) forControlEvents:UIControlEventValueChanged];

	self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:simpleSwitch];
}


- (void) switchIsChanged:(UISwitch *)paramSender
{
	if ( paramSender.isOn )
		NSLog(@"Switch is on.");
	else
		NSLog(@"Switch is off.");
}




@end
