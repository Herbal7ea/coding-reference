//
//  UIActivity_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/27/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UIActivity_Controller.h"
#import "StringReverserActivity.h"

@interface UIActivity_Controller ()

@end

@implementation UIActivity_Controller

- (void) viewDidAppear:(BOOL)animated{
	
	[super viewDidAppear:animated];

	NSArray *itemsToShare = @[ @"Item 1", @"Item 2", @"Item 3", ];
	NSArray *activities = @[[StringReverserActivity new]];

	UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:activities];

	[self presentViewController:activity animated:YES completion:nil];
}

@end
