//
//  EnumsAndBitFlags.m
//  DynamicUI
//
//  Created by jbott on 3/27/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "EnumsAndBitFlags.h"

// If you replace with this and delete the default case statement, there is no warning! This is because the compiler has no way to know the VideoGameType typedef and the enum are related.

//enum tag_VideoGameType {
//    VideoGameTypeRPG,
//    VideoGameTypeStrategy,
//    VideoGameTypeAction,
//    VideoGameTypeFPS
//};
//typedef int VideoGameType;

typedef NS_ENUM(int, VideoGameType) {
    VideoGameTypeRPG,
    VideoGameTypeStrategy,
    VideoGameTypeAction,
    VideoGameTypeFPS
};

typedef NS_OPTIONS(int, EntityCategory) {
    EntityCategoryPlayer = 1 << 0,
    EntityCategoryEnemy = 1 << 1,
    EntityCategoryAlien = 1 << 2,
    EntityCategoryStrange = 1 << 16,
};

@implementation EnumsAndBitFlags

+ (void) showExample
{
    // Demo 1: Enum
    VideoGameType type = VideoGameTypeFPS;
    NSLog(@"Sizeof type: %lu", sizeof(type));
    
    switch (type) {
        case VideoGameTypeRPG:
            NSLog(@"RPG");
            break;
        case VideoGameTypeStrategy:
            NSLog(@"Strategy");
            break;
        default:
            NSLog(@"Other!");
            break;
    }
    
    // Demo 2: Bitmasks
    char category = EntityCategoryEnemy | EntityCategoryAlien;
    
    NSLog(@"%x", EntityCategoryPlayer);
    NSLog(@"%x", EntityCategoryEnemy);
    NSLog(@"%x", EntityCategoryAlien);
    NSLog(@"%x", category);
    
    if (category & EntityCategoryPlayer) {
        NSLog(@"Player!");
    }
    if (category & EntityCategoryEnemy) {
        NSLog(@"Enemy!");
    }
    if (category & EntityCategoryAlien) {
        NSLog(@"Alien!");
    }
}

@end
