//
//  Constants.h
//  DynamicUI
//
//  Created by jbott on 3/27/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import <Foundation/Foundation.h>

#define TIMEOUT_INTERVAL 300

#define UIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define UIColorFromRGBAndAlpha(rgbValue, alp) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:alp]

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_PHONE !IS_IPAD

typedef NS_ENUM(NSInteger, SomeType){
    SomeType_One = 0,
    SomeType_Two,
    SomeType_Three,
    SomeType_Four
};

static const int someIntConst2 = 30;

extern const int someIntConst;
extern NSString * const someStringConst;

@interface Constants : NSObject

@end
