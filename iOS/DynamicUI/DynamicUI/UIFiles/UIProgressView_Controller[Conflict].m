//
//  UIProgressView_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/25/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UIProgressView_Controller.h"

@interface UIProgressView_Controller ()

@property (nonatomic, strong) UIProgressView *progressView;

@end

@implementation UIProgressView_Controller

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = UIColor.whiteColor;

	[self presentExamples];
}

- (void)presentExamples
{
	self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleBar];
	self.progressView.center = self.view.center;
	self.progressView.progress = 20.0f / 30.0f;

	[self.view addSubview:self.progressView];
}


@end
