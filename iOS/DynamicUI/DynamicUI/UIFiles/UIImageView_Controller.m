//
//  UIImageView_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/19/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UIImageView_Controller.h"

@interface UIImageView_Controller ()

@property (nonatomic, strong) UIImageView *myImageView;

@end

@implementation UIImageView_Controller

- (void)viewDidLoad{
	[super viewDidLoad];
	self.view.backgroundColor = UIColor.whiteColor;

	[self presentExamples];
}

- (void)presentExamples
{
//	[self showImageView_WithScale];
//	[self showImageView_Streched];
	[self showImageView_Concise];
}


- (void)showImageView_WithScale
{
	UIImage *macBookAir = [UIImage imageNamed:@"MacBookAir"];

	self.myImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
	self.myImageView.contentMode = UIViewContentModeScaleAspectFit;
	self.myImageView.image = macBookAir;
	self.myImageView.center = self.view.center;

	[self.view addSubview:self.myImageView];
}

- (void)showImageView_Streched
{

	UIImage *macBookAir = [UIImage imageNamed:@"MacBookAir"];

	self.myImageView = [[UIImageView alloc] initWithFrame:self.view.bounds];
	self.myImageView.image = macBookAir;
	self.myImageView.center = self.view.center;

	[self.view addSubview:self.myImageView];

}

- (void)showImageView_Concise
{

	UIImage *macBookAir = [UIImage imageNamed:@"MacBookAir"];
	self.myImageView = [[UIImageView alloc] initWithImage:macBookAir];
	self.myImageView.center = self.view.center;
	[self.view addSubview:self.myImageView];

}

@end
