//
//  UILabel_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/14/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UILabel_Controller.h"

@interface UILabel_Controller ()

@property (nonatomic, strong) UILabel *myLabel;

@end

@implementation UILabel_Controller

- (void)viewDidLoad{

	[super viewDidLoad];
	self.view.backgroundColor = UIColor.whiteColor;

	[self presentExamples];
}

- (void)presentExamples
{
	[self createSimpleLabel];
	[self createSimpleLabel2];
	[self createLabelWithAdjustableFont];
	[self createLabelWithNotWordWrapping];
}

- (void)createSimpleLabel
{
	self.myLabel = [[UILabel alloc] initWithFrame: CGRectMake(0.0f, 0.0f, 100.0f, 23.0f)];
	self.myLabel.text = @"Hello World";
	self.myLabel.font = [UIFont boldSystemFontOfSize:14.0f];
	self.myLabel.center = self.view.center;

	[self.view addSubview:self.myLabel];
}


- (void)createSimpleLabel2
{
	self.myLabel = [UILabel new];
	self.myLabel.backgroundColor = UIColor.clearColor;
	self.myLabel.text = @"iOS SDK";
	self.myLabel.font = [UIFont boldSystemFontOfSize:70.0f];
	self.myLabel.textColor = UIColor.blackColor;
	self.myLabel.shadowColor = UIColor.lightGrayColor;
	self.myLabel.shadowOffset = CGSizeMake(2.0f, 2.0f);
	[self.myLabel sizeToFit];
	self.myLabel.center = self.view.center;

	[self.view addSubview:self.myLabel];
}


- (void)createLabelWithAdjustableFont
{
	self.myLabel = [[UILabel alloc] initWithFrame: CGRectMake(0.0f, 0.0f, 100.0f, 23.0f)];
	self.myLabel.text = @"iOS 7 Programming Cookbook";
	self.myLabel.font = [UIFont boldSystemFontOfSize:14.0f];
	self.myLabel.center = self.view.center;

	[self.view addSubview:self.myLabel];

	self.myLabel.adjustsFontSizeToFitWidth = YES;
}

- (void)createLabelWithNotWordWrapping
{
	self.myLabel = [[UILabel alloc] initWithFrame: CGRectMake(0.0f, 0.0f, 100.0f, 23.0f)];
	self.myLabel.text = @"iOS 7 Programming Cookbook";
	self.myLabel.font = [UIFont boldSystemFontOfSize:14.0f];
	self.myLabel.center = self.view.center;

	[self.view addSubview:self.myLabel];

	CGRect rect = self.myLabel.frame;
	rect.size.height = 70.0f;
	self.myLabel.frame = rect;
	self.myLabel.numberOfLines = 3;
	self.myLabel.lineBreakMode = NSLineBreakByWordWrapping;

	//	self.myLabel.layer.borderColor = UIColor.blackColor.CGColor;
	//	self.myLabel.layer.borderWidth = 1.0f;

}

@end
