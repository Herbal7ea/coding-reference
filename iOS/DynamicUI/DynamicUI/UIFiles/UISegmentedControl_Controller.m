//
//  UISegmentedControl_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/26/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UISegmentedControl_Controller.h"

@interface UISegmentedControl_Controller ()

@property (nonatomic, strong) UISegmentedControl *mySegmentedControl;

@end

@implementation UISegmentedControl_Controller

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = UIColor.whiteColor;

	[self presentSegmentedControl_WithImage];
}


- (void)presentSegmentedControl_WithImage
{
	CGRect segmentedFrame = self.mySegmentedControl.frame;
	segmentedFrame.size.height = 64.0f;
	segmentedFrame.size.width = 300.0f;

	self.mySegmentedControl = [[UISegmentedControl alloc] initWithItems: @[@"iPhone", [UIImage imageNamed:@"iPad"], @"iPod", @"iMac"] ];
	self.mySegmentedControl.frame = segmentedFrame;
	self.mySegmentedControl.center = self.view.center;
	self.mySegmentedControl.momentary = YES; //clicks and unclicks
	//	self.mySegmentedControl.segmentedControlStyle = UISegmentedControlStyleBezeled;

	[self.mySegmentedControl addTarget:self action:@selector(segmentChanged:) forControlEvents:UIControlEventValueChanged];

	[self.view addSubview:self.mySegmentedControl];
}

#pragma mark - event

- (void) segmentChanged:(UISegmentedControl *)paramSender
{

	if ([paramSender isEqual:self.mySegmentedControl])
	{
		NSInteger selectedSegmentIndex = [paramSender selectedSegmentIndex];
		NSString  *selectedSegmentText = [paramSender titleForSegmentAtIndex:selectedSegmentIndex];

		NSLog(@"Segment %ld with %@ text is selected", (long)selectedSegmentIndex, selectedSegmentText);
	}
}


@end
