//
//  UIDatePicker_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/19/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UIDatePicker_Controller.h"

@interface UIDatePicker_Controller ()

@property (nonatomic, strong) UIDatePicker *myDatePicker;

@end

@implementation UIDatePicker_Controller

- (void)viewDidLoad{
	[super viewDidLoad];
	self.view.backgroundColor = UIColor.whiteColor;

	[self presentExamples];
}

- (void)presentExamples
{
//	[self presentPickerWithEvents];
//	[self presentDatePicker_WithCountDownTimer];
	[self presentDatePicker_WithLimitedRange];
}

- (void)presentPickerWithEvents{
    self.myDatePicker = [[UIDatePicker alloc] init];
    self.myDatePicker.center = self.view.center;
    [self.view addSubview:self.myDatePicker];

    [self.myDatePicker addTarget:self action:@selector(datePickerDateChanged:) forControlEvents:UIControlEventValueChanged];
}


- (void)presentDatePicker_WithCountDownTimer
{
	NSTimeInterval twoMinutes = 2 * 60;

	self.myDatePicker = [UIDatePicker new];
	self.myDatePicker.center = self.view.center;
	self.myDatePicker.datePickerMode = UIDatePickerModeCountDownTimer;
	self.myDatePicker.countDownDuration = twoMinutes;

	[self.view addSubview:self.myDatePicker];
}

- (void)presentDatePicker_WithLimitedRange;
{
	NSTimeInterval oneYearTime = 365 * 24 * 60 * 60;
	NSDate *todayDate = [NSDate date];
	NSDate *oneYearFromToday = [todayDate dateByAddingTimeInterval:oneYearTime];
	NSDate *twoYearsFromToday = [todayDate dateByAddingTimeInterval:2 * oneYearTime];

	self.myDatePicker = [[UIDatePicker alloc] init];
	self.myDatePicker.center = self.view.center;
	self.myDatePicker.datePickerMode = UIDatePickerModeDate;
	self.myDatePicker.minimumDate = oneYearFromToday;
	self.myDatePicker.maximumDate = twoYearsFromToday;

	[self.view addSubview:self.myDatePicker];
}


#pragma mark - events
-
(void) datePickerDateChanged:(UIDatePicker *)paramDatePicker
{
	if (paramDatePicker == self.myDatePicker)
		NSLog(@"Selected date = %@", paramDatePicker.date);
}


@end
