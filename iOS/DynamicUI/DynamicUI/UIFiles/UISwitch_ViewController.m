//
//  UISwitch_ViewController.m
//  DynamicUI
//
//  Created by jbott on 3/13/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UISwitch_ViewController.h"

@interface UISwitch_ViewController ()

@property (nonatomic, strong) UISwitch *mySwitch;

@end

@implementation UISwitch_ViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	self.view.backgroundColor = UIColor.whiteColor;

	[self runSwitchExamples];

}

- (void)runSwitchExamples
{

//	[self createSimpleSwitch];
//	[self createSimpleSwitchOn];
//	[self switchCheckOn];
//	[self createSwitchWithColors];
//	[self createSwitchWithImages];//doesn't work in iOS 7
	[self bestExample__createSwitchWithEvent];
}

#pragma mark - private methods

#pragma mark - example 1
- (void)createSwitchWithColors
{
	[self createSwitch];
    
	self.mySwitch.tintColor = [UIColor redColor];  // Adjust the off-mode tint color
	self.mySwitch.onTintColor = [UIColor brownColor]; // Adjust the on-mode tint color
	self.mySwitch.thumbTintColor = [UIColor greenColor];  // Also change the knob's tint color
    
}

- (void)createSwitch
{
	self.mySwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
	self.mySwitch.center = self.view.center;
	[self.view addSubview:self.mySwitch];
}

- (void)createSwitchWithImages
{
	[self createSwitch];
    
	/* Customize the switch */
	self.mySwitch.onImage = [UIImage imageNamed:@"On"];
	self.mySwitch.offImage = [UIImage imageNamed:@"Off"];
}

#pragma mark - example 2

- (void)bestExample__createSwitchWithEvent
{
	self.mySwitch = [[UISwitch alloc] initWithFrame: CGRectMake(100, 100, 0, 0)];
	self.mySwitch.on = YES;
//	self.mySwitch.center = self.view.center;

	[self.mySwitch addTarget:self action:@selector(switchIsChanged:) forControlEvents:UIControlEventValueChanged];

	[self.view addSubview:self.mySwitch];
}

- (void)createSimpleSwitch
{
	self.mySwitch = [[UISwitch alloc] initWithFrame: CGRectMake(100, 100, 0, 0)];

	[self.view addSubview:self.mySwitch];
}

- (void)createSimpleSwitchOn
{
	self.mySwitch = [[UISwitch alloc] initWithFrame: CGRectMake(100, 100, 0, 0)];
	self.mySwitch.on = YES;

	[self.view addSubview:self.mySwitch];
}

- (void)switchCheckOn
{
	self.mySwitch = [[UISwitch alloc] initWithFrame: CGRectMake(100, 100, 0, 0)];
	self.mySwitch.on = NO;//YES;  //	[self.mySwitch setOn:YES];

	[self.view addSubview:self.mySwitch];

	if(self.mySwitch.on) //	if([self.mySwitch isOn])
	{
		NSLog(@"The switch is on.");
	}
	else
	{
		NSLog(@"The switch is off.");
	}

}

#pragma mark - Switch changed

- (void) switchIsChanged:(UISwitch *)pSwitch
{
	NSLog(@"Sender is = %@", pSwitch);

	if (pSwitch.on){
		NSLog(@"The switch is turned on.");
	} else {
		NSLog(@"The switch is turned off.");
	}

}


@end
