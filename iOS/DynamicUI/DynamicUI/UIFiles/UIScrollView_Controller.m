//
//  UIScrollView_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/19/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UIScrollView_Controller.h"

@interface UIScrollView_Controller ()<UIScrollViewDelegate>

@property (nonatomic, strong) UIScrollView *myScrollView;
@property (nonatomic, strong) UIImageView *myImageView;

@end

@implementation UIScrollView_Controller


- (void)viewDidLoad{
	[super viewDidLoad];
	self.view.backgroundColor = UIColor.whiteColor;

	[self presentExamples];
}

- (void)presentExamples
{
//	[self presentSimpleScrollView];
//	[self presentSimpleScrollView_WithWhiteIndicator];
	[self presentSimpleScrollView_WithMultipleSubViews];
}

- (void)presentSimpleScrollView
{
    UIImage *imageToLoad = [UIImage imageNamed:@"MacBookAir"];
    self.myImageView = [[UIImageView alloc] initWithImage:imageToLoad];

    self.myScrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
	self.myScrollView.contentSize = self.myImageView.bounds.size;
	self.myScrollView.delegate = self;
	[self.myScrollView addSubview:self.myImageView];

	[self.view addSubview:self.myScrollView];
}


- (void)presentSimpleScrollView_WithWhiteIndicator
{
	UIImage *imageToLoad = [UIImage imageNamed:@"MacBookAir"];
	self.myImageView = [[UIImageView alloc] initWithImage:imageToLoad];

	self.myScrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
	self.myScrollView.contentSize = self.myImageView.bounds.size;
	self.myScrollView.delegate = self;
	self.myScrollView.indicatorStyle = UIScrollViewIndicatorStyleWhite;
	[self.myScrollView addSubview:self.myImageView];

	[self.view addSubview:self.myScrollView];
}

- (void)presentSimpleScrollView_WithMultipleSubViews
{
	CGRect scrollViewRect = self.view.bounds;
	self.myScrollView = [[UIScrollView alloc] initWithFrame:scrollViewRect];
	self.myScrollView.pagingEnabled = YES;
	self.myScrollView.contentSize = CGSizeMake(scrollViewRect.size.width * 3.0f,
											   scrollViewRect.size.height);

	[self.view addSubview:self.myScrollView];
	[self populateScrollView];
}



#pragma mark - helper methods

- (void)populateScrollView
{
	UIImage *macBookAir = [UIImage imageNamed:@"MacBookAir"];
	UIImage *iPhone     = [UIImage imageNamed:@"iPhone"];
	UIImage *iPad       = [UIImage imageNamed:@"iPad"];


	CGRect imageViewRect = self.view.bounds;
	UIImageView *iPhoneImageView = [self newImageViewWithImage:iPhone frame:imageViewRect];

	imageViewRect.origin.x += imageViewRect.size.width;
	UIImageView *iPadImageView = [self newImageViewWithImage:iPad frame:imageViewRect];

	imageViewRect.origin.x += imageViewRect.size.width;
	UIImageView *macBookAirImageView = [self newImageViewWithImage:macBookAir frame:imageViewRect];

	[self.myScrollView addSubview:iPhoneImageView];
	[self.myScrollView addSubview:iPadImageView];
	[self.myScrollView addSubview:macBookAirImageView];
}

- (UIImageView *) newImageViewWithImage:(UIImage *)paramImage frame:(CGRect)paramFrame
{

	UIImageView *result = [[UIImageView alloc] initWithFrame:paramFrame];
    result.contentMode = UIViewContentModeScaleAspectFit;
	result.image = paramImage;
	return result;

}

#pragma mark - events
// Gets called when user scrolls or drags
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	self.myScrollView.alpha = 0.50f;
}

// Gets called only after scrolling
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
	self.myScrollView.alpha = 1.0f;
}

// Make sure the alpha is reset even if the user is dragging
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	self.myScrollView.alpha = 1.0f;
}





@end
