//
//  Constants.m
//  DynamicUI
//
//  Created by jbott on 3/27/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "Constants.h"

NSString * const someStringConst = @"ksomeStringConst";
const int someIntConst = 30;

@implementation Constants

@end
