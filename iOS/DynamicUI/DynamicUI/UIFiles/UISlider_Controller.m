//
//  UISlider_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/10/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UISlider_Controller.h"

@interface UISlider_Controller ()

@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UISlider *mySlider;

@end

@implementation UISlider_Controller

- (void)viewDidLoad
{
    [super viewDidLoad];
	self.view.backgroundColor = UIColor.whiteColor;

	[self presentExamples];
}

- (void)presentExamples
{
//		[self createDefaultSlider];
//		[self createSliderWithCustomImages];
//		[self createSlideWithColors];

	[self addSliderWithCustomButtons];
	//	[self addSliderWithCallback];

}

- (void)createSliderWithCustomImages
{
	[super viewDidLoad];

	self.slider = [[UISlider alloc] initWithFrame: CGRectMake(0.0f, 0.0f, 218.0f, 23.0f)];
	self.slider.value = 0.5;
	self.slider.minimumValue = 0.0f;
	self.slider.maximumValue = 1.0f;
	self.slider.center = self.view.center;

	self.slider.minimumValueImage = [UIImage imageNamed:@"MinimumValue"];
	self.slider.maximumValueImage = [UIImage imageNamed:@"MaximumValue"];

	[self.slider setThumbImage:[UIImage imageNamed:@"Thumb"] forState:UIControlStateNormal];
	[self.slider setThumbImage:[UIImage imageNamed:@"Thumb"] forState:UIControlStateHighlighted];

	[self.slider setMinimumTrackImage:self.minimumTrackImage forState:UIControlStateNormal];
	[self.slider setMaximumTrackImage:self.maximumTrackImage forState:UIControlStateNormal];

	[self.view addSubview:self.slider];

}

- (void)createSlideWithColors
{
	self.slider = [[UISlider alloc] initWithFrame: CGRectMake(0.0f, 0.0f, 118.0f, 23.0f)];
	self.slider.value = 0.5;
	self.slider.minimumValue = 0.0f;
	self.slider.maximumValue = 1.0f;
	self.slider.center = self.view.center;

	self.slider.minimumTrackTintColor = UIColor.redColor;
	self.slider.maximumTrackTintColor = UIColor.greenColor;
	self.slider.thumbTintColor = UIColor.blackColor;

	[self.view addSubview:self.slider];
}

- (UIImage *) minimumTrackImage
{
	UIImage *result = [UIImage imageNamed:@"MinimumTrack"];
	return [result resizableImageWithCapInsets: UIEdgeInsetsMake(0, 4, 0, 0)];
}

- (UIImage *) maximumTrackImage
{
	UIImage *result = [UIImage imageNamed:@"MaximumTrack"];
	return [result resizableImageWithCapInsets: UIEdgeInsetsMake(0, 0, 0, 3)];
}


#pragma mark - second examples

- (void)createDefaultSlider
{
	self.mySlider = [[UISlider alloc] initWithFrame: CGRectMake(0.0f, 0.0f, 200.0f, 23.0f)];
	self.mySlider.center = self.view.center;
	self.mySlider.minimumValue = 0.0f;
	self.mySlider.maximumValue = 100.0f;
	self.mySlider.value = self.mySlider.maximumValue / 2.0;

	[self.view addSubview:self.mySlider];
}


- (void)addSliderWithCustomButtons
{
	[self createDefaultSlider];

	[self.mySlider setThumbImage:[UIImage imageNamed:@"ThumbNormal.png"] forState:UIControlStateNormal];
	[self.mySlider setThumbImage:[UIImage imageNamed:@"ThumbHighlighted.png"] forState:UIControlStateHighlighted];

	[self.view addSubview:self.mySlider];
}


#pragma mark - slider value changed

- (void)addSliderWithCallback
{
	[self createDefaultSlider];

	[self.mySlider addTarget:self action:@selector(sliderValueChanged:) forControlEvents:UIControlEventValueChanged];

	[self.view addSubview:self.mySlider];
}

- (void) sliderValueChanged:(UISlider *)paramSender
{

	if (paramSender == self.mySlider)
	{
		NSLog(@"New value = %f", paramSender.value);
	}
}

@end
