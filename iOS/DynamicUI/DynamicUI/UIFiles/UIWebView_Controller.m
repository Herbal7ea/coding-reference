//
//  UIWebView_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/19/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UIWebView_Controller.h"

@interface UIWebView_Controller ()

@property(nonatomic, strong) UIWebView *myWebView;

@end

@implementation UIWebView_Controller

- (void)viewDidLoad
{
    [super viewDidLoad];

	[self presentExamples];
}


- (void)presentExamples
{
//	[self renderHtmlString];
	[self loadWebPage_WithEvents];
}


- (void)renderHtmlString
{
	NSString *htmlString = @"<br/>iOS 7 Programming <strong>Cookbook</strong>";

	self.myWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
	[self.myWebView loadHTMLString:htmlString baseURL:nil];

	[self.view addSubview:self.myWebView];
}

- (void)loadWebPage_WithEvents
{
	NSURL *url = [NSURL URLWithString:@"http://www.apple.com"];
	NSURLRequest *request = [NSURLRequest requestWithURL:url];

	self.myWebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
	self.myWebView.scalesPageToFit = YES;
	self.myWebView.delegate = self;
	[self.myWebView loadRequest:request];

	[self.view addSubview:self.myWebView];
}


#pragma mark - events

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	UIApplication.sharedApplication.networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	UIApplication.sharedApplication.networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	UIApplication.sharedApplication.networkActivityIndicatorVisible = NO;
}

@end
