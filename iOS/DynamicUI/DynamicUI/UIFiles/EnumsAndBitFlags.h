//
//  EnumsAndBitFlags.h
//  DynamicUI
//
//  Created by jbott on 3/27/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EnumsAndBitFlags : NSObject

+ (void) showExample;

@end
