//
//  StringReverserActivity.m
//  Presenting Custom Sharing Options with UIActivityViewController
//
//  Created by Vandad NP on 23/06/2013.
//  Copyright (c) 2013 Pixolity Ltd. All rights reserved.
//

#import "StringReverserActivity.h"
#import "NSArray+LinqExtensions.h"

@interface StringReverserActivity () <UIAlertViewDelegate>

@property(nonatomic, strong) NSArray *activityItems;

@end

@implementation StringReverserActivity

- (NSString *)activityType
{
	NSString *className = [NSBundle.mainBundle.bundleIdentifier stringByAppendingFormat:@".%@", NSStringFromClass([self class])];
	return className;
}

- (NSString *)activityTitle
{
	return @"Reverse String";
}

- (UIImage *)activityImage
{
	return [UIImage imageNamed:@"Reverse"];
}

- (BOOL)canPerformWithActivityItems:(NSArray *)activityItems
{
	for(id object in activityItems)
	{
		if([object isKindOfClass:[NSString class]])
			return YES;
	}

	return NO;
}

- (void)prepareWithActivityItems:(NSArray *)activityItems
{
	NSArray *stringObjects = [activityItems linq_where:^BOOL(id item)
	{
		return [item isKindOfClass:NSString.class];
	}];

	self.activityItems = stringObjects;
}

- (void)performActivity
{
	NSMutableString *reversedStrings = [NSMutableString new];

	for(NSString *string in self.activityItems)
	{
		NSString *reverse = [self reverseOfString:string];
		[reversedStrings appendString:reverse];
		[reversedStrings appendString:@"\n"];
	}

	[[[UIAlertView alloc] initWithTitle:@"Reversed"
								message:reversedStrings
							   delegate:self
					  cancelButtonTitle:@"OK"
					  otherButtonTitles:nil] show];
}


#pragma mark - UIAlertViewDelegate Methods

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
	NSLog(@"buttonIndex = %@", buttonIndex);
	[self activityDidFinish:YES];
}

#pragma mark - helper methods

- (NSString *)reverseOfString:(NSString *)paramString
{
	NSMutableString *reversed = [[NSMutableString alloc] initWithCapacity:paramString.length];

	for(NSInteger i = paramString.length - 1; i >= 0; i--)
	{
		[reversed appendFormat:@"%c", [paramString characterAtIndex:i]];
	}

	return [reversed copy];
}

@end
