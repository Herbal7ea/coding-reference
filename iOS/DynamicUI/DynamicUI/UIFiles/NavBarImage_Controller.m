//
//  NavBarImage_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/17/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "NavBarImage_Controller.h"

@interface NavBarImage_Controller ()

@end

@implementation NavBarImage_Controller


- (void)viewDidLoad
{
    [super viewDidLoad];

	UIImage *image = [UIImage imageNamed:@"Logo"];

    UIImageView *imageView = [[UIImageView alloc] initWithFrame: CGRectMake(0, 0, 100, 40)];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    imageView.image = image;

	self.navigationItem.titleView = imageView;
}
@end
