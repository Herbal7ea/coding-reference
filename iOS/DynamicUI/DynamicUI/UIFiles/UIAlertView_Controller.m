//
//  UIAlertView_Controller.m
//  DynamicUI
//
//  Created by jbott on 3/20/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "UIAlertView_Controller.h"

@interface UIAlertView_Controller () <UIAlertViewDelegate>

@end

@implementation UIAlertView_Controller

- (void) viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

	[self presentExamples];
}

- (void)presentExamples
{
//	[self presentAlertWithEvents];
//	[self presentAlertViewWithKeyPad];
	[self presentAlertWithSecureTextField];
//	[self presentAlertViewWithLoginAndPass];
}

- (void)presentAlertWithEvents
{

	UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Open Link"
                              message:@"Are you sure you want to open this link in Safari?"
                              delegate:self
                              cancelButtonTitle:@"No"
                              otherButtonTitles:@"Yes", nil];
	[alertView show];

}


- (void) presentAlertViewWithKeyPad
{
    UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Credit Card Number"
                              message:@"Please enter your credit card number:"
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"Ok", nil];

    alertView.alertViewStyle = UIAlertViewStylePlainTextInput;

    UITextField *textField = [alertView textFieldAtIndex:0];
    textField.keyboardType = UIKeyboardTypeNumberPad;

    [alertView show];
}

- (void)presentAlertWithSecureTextField
{
	UIAlertView *alertView = [[UIAlertView alloc]
                              initWithTitle:@"Password"
                              message:@"Please enter your password:"
                              delegate:self
                              cancelButtonTitle:@"Cancel"
                              otherButtonTitles:@"Ok", nil];

	[alertView setAlertViewStyle:UIAlertViewStyleSecureTextInput];
	[alertView show];
}



- (void)presentAlertViewWithLoginAndPass
{

  UIAlertView *alertView = [[UIAlertView alloc]
                            initWithTitle:@"Password"
                            message:@"Please enter your credentials:"
                            delegate:self
                            cancelButtonTitle:@"Cancel"
                            otherButtonTitles:@"Ok", nil];

  [alertView setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
  [alertView show];

}


#pragma mark - events

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	NSString *buttonTitle = [alertView buttonTitleAtIndex:buttonIndex];

	@try
	{
		UITextField *usernameTextField = [alertView textFieldAtIndex:0];
		UITextField *passwordTextField = [alertView textFieldAtIndex:1];

		if(usernameTextField && passwordTextField)
			NSLog(@"Username: %@, Password:%@", usernameTextField.text, passwordTextField.text);

	}
	@catch (NSException *e)
	{

	}

	if ([buttonTitle isEqualToString:@"Yes"])
	{
		NSLog(@"User pressed the Yes button.");
	}
	else if ([buttonTitle isEqualToString:@"No"])
	{
		NSLog(@"User pressed the No button.");
	}
}






@end
