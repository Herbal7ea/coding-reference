//
//  ExamplesViewController.m
//  DynamicUI
//
//  Created by jbott on 3/13/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "ExamplesViewController.h"
#import "UISwitch_ViewController.h"
#import "UISlider_Controller.h"
#import "UITextField_Controller.h"
#import "UILabel_Controller.h"
#import "StyledText_Controller.h"
#import "UITextView_Controller.h"
#import "UIButton_Controller.h"
#import "UIBarButtonItem_Controller.h"
#import "UIImageView_Controller.h"
#import "UIWebView_Controller.h"
#import "UIPickerView_Controller.h"
#import "UIDatePicker_Controller.h"
#import "UIScrollView_Controller.h"
#import "UIAlertView_Controller.h"
#import "UIProgressView_Controller.h"
#import "UISegmentedControl_Controller.h"
#import "UIActivity_Controller.h"
#import "UIActivity2_Controller.h"
#import "NSArray+LinqExtensions.h"


@interface ExamplesViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *examplesData;

@end

@implementation ExamplesViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

	[self initializeData];
}

- (void)initializeData
{
	_examplesData = @[

			UISwitch_ViewController.class,
			UISlider_Controller.class,
			UITextField_Controller.class,
			UITextView_Controller.class,
			UILabel_Controller.class,
			StyledText_Controller.class,
			UIButton_Controller.class,
			UIImageView_Controller.class,
			UIWebView_Controller.class,
			UIPickerView_Controller.class,
			UIDatePicker_Controller.class,
			UIScrollView_Controller.class,
			UIAlertView_Controller.class,
			UIProgressView_Controller.class,
			UISegmentedControl_Controller.class,
			UIActivity_Controller.class,
			UIActivity2_Controller.class,

//		[UIBarButtonItem_Controller class],  // this class is created at the AppDelegate
	];

//    _examplesData = [_examplesData linq_reverse];



//	self.tableView.delegate = self;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _examplesData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *className = NSStringFromClass(_examplesData[indexPath.row]);

	static NSString *CellIdentifier = @"Cell";
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
	cell.textLabel.text = className;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	Class vcClass = _examplesData[indexPath.row];

	UIViewController *newVC = [[vcClass alloc] init];

	CGRect rectToChange = newVC.view.frame;
	rectToChange.size.height = 300;

	newVC.view.frame = rectToChange;

    [self.navigationController pushViewController:newVC animated:YES];
//    [self presentViewController:newVC animated:YES completion:nil];
//    [self presentModalViewController:newVC animated:YES]; -- deprecated
    
    
//	UINavigationController *nvc = UIApplication.sharedApplication.keyWindow.rootViewController;
//	nvc.modalPresentationStyle = UIModalPresentationCustom;
//	[nvc presentViewController:newVC animated:YES completion:nil];

}



@end
