//
//  SecondViewController.m
//  DynamicUI
//
//  Created by jbott on 3/26/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad
{
	[super viewDidLoad];

	self.title = @"Second Controller";

	[self performSelector:@selector(goBack) withObject:nil afterDelay:5.0f];
}

#pragma mark - helper methods

- (void) goBack
{
//	[self goback1];
	[self goBack2];
}

- (void)goback1
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (void) goBack2
{
	NSArray *currentControllers = self.navigationController.viewControllers;
	NSMutableArray *newControllers = [NSMutableArray arrayWithArray:currentControllers];
	[newControllers removeLastObject];

	[self.navigationController setViewControllers:newControllers animated:YES];
//	self.navigationController.viewControllers = newControllers;
}


@end
