//
//  NavController2.m
//  DynamicUI
//
//  Created by jbott on 3/26/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "NavController2.h"
#import "SecondViewController.h"

@interface NavController2 ()

@property(nonatomic, strong) UIButton *displaySecondViewController;

@end

@implementation NavController2

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self presentExamples];

}

- (void)presentExamples
{
	self.title = @"First Controller";

	self.displaySecondViewController = [UIButton buttonWithType:UIButtonTypeSystem];

	[self.displaySecondViewController setTitle:@"Display Second View Controller" forState:UIControlStateNormal];
	[self.displaySecondViewController sizeToFit];
	self.displaySecondViewController.center = self.view.center;
	[self.displaySecondViewController addTarget:self action:@selector(performDisplaySecondViewController:) forControlEvents:UIControlEventTouchUpInside];

	[self.view addSubview:self.displaySecondViewController];
}

#pragma mark - helper methods

- (void)performDisplaySecondViewController:(id)paramSender
{
	SecondViewController *secondController = [[SecondViewController alloc] initWithNibName:nil bundle:NULL];
	[self.navigationController pushViewController:secondController animated:YES];
}

@end
