//
//  DetailViewController.m
//  Presenting Master-Detail Views with UISplitViewController
//
//  Created by Vandad NP on 02/07/2013.
//  Copyright (c) 2013 Pixolity Ltd. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@property(strong, nonatomic) UIPopoverController *masterPopoverController;

@end

@implementation DetailViewController

#pragma mark - Managing the detail item

- (void)viewDidLoad
{
	[super viewDidLoad];

	[self configureView];
}

#pragma mark - Split view

- (BOOL)splitViewController:(UISplitViewController *)svc shouldHideViewController:(UIViewController *)vc inOrientation:(UIInterfaceOrientation)orientation
{
	return NO;
}

- (void)splitViewController:(UISplitViewController *)splitController willHideViewController:(UIViewController *)viewController withBarButtonItem:(UIBarButtonItem *)barButtonItem forPopoverController:(UIPopoverController *)popoverController
{
	barButtonItem.title = NSLocalizedString(@"Master", @"Master");
	[self.navigationItem setLeftBarButtonItem:barButtonItem animated:YES];
	self.masterPopoverController = popoverController;
}

- (void)splitViewController:(UISplitViewController *)splitController willShowViewController:(UIViewController *)viewController invalidatingBarButtonItem:(UIBarButtonItem *)barButtonItem
{
	[self.navigationItem setLeftBarButtonItem:nil animated:YES];
	self.masterPopoverController = nil;
}

#pragma mark - helper methods

- (void)configureView
{
	if(self.detailItem)
		self.detailDescriptionLabel.text = [self.detailItem description];
}

- (void)setDetailItem:(id)newDetailItem
{
	if(_detailItem != newDetailItem)
	{
		_detailItem = newDetailItem;

		[self configureView];
	}

	if(self.masterPopoverController != nil)
		[self.masterPopoverController dismissPopoverAnimated:YES];
}

@end
