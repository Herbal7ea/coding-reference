//
//  UIColor+HexString.m
//  ExtensionApp
//
//  Created by Brian Mullen on 9/23/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "UIColor+HexString.h"


@implementation UIColor (HexString)

+ (NSArray *)colorComponentsWithHexString:(NSString *)hexString
{
    NSMutableArray *result = [NSMutableArray arrayWithCapacity:4];
    NSString *colorString = [[hexString stringByReplacingOccurrencesOfString:@"#" withString:@""] uppercaseString];
    CGFloat alpha = 1.0f;
    CGFloat red = 0.0f;
    CGFloat blue = 0.0f;
    CGFloat green = 0.0f;
    
    switch( colorString.length )
    {
        case 3: // #RGB
                alpha = 1.0f;
                red   = [self colorComponentFrom:colorString start:0 length:1];
                green = [self colorComponentFrom:colorString start:1 length:1];
                blue  = [self colorComponentFrom:colorString start:2 length:1];
            break;
        case 4: // #ARGB
                alpha = [self colorComponentFrom:colorString start:0 length:1];
                red   = [self colorComponentFrom:colorString start:1 length:1];
                green = [self colorComponentFrom:colorString start:2 length:1];
                blue  = [self colorComponentFrom:colorString start:3 length:1];
            break;
        case 6: // #RRGGBB
                alpha = 1.0f;
                red   = [self colorComponentFrom:colorString start:0 length:2];
                green = [self colorComponentFrom:colorString start:2 length:2];
                blue  = [self colorComponentFrom:colorString start:4 length:2];
            break;
        case 8: // #AARRGGBB
                alpha = [self colorComponentFrom:colorString start:0 length:2];
                red   = [self colorComponentFrom:colorString start:2 length:2];
                green = [self colorComponentFrom:colorString start:4 length:2];
                blue  = [self colorComponentFrom:colorString start:6 length:2];
            break;
        default:
                [NSException raise:@"Invalid color value" format: @"Color value %@ is invalid.  It should be a hex value of the form #RBG, #ARGB, #RRGGBB, or #AARRGGBB", hexString];
            break;
    }
    
    result[ 0 ] = [NSNumber numberWithFloat:red];
    result[ 1 ] = [NSNumber numberWithFloat:green];
    result[ 2 ] = [NSNumber numberWithFloat:blue];
    result[ 3 ] = [NSNumber numberWithFloat:alpha];
    
    return result;
}

+ (UIColor *)colorWithHexString:(NSString *)hexString
{
    NSArray *colorComponents = [self colorComponentsWithHexString:hexString];
    CGFloat red = [(NSNumber *)colorComponents[ 0 ] floatValue];
    CGFloat green = [(NSNumber *)colorComponents[ 1 ] floatValue];
    CGFloat blue = [(NSNumber *)colorComponents[ 2 ] floatValue];
    CGFloat alpha = [(NSNumber *)colorComponents[ 3 ] floatValue];
    
    return [UIColor colorWithRed:red green:green blue:blue alpha:alpha];
}

+ (CGFloat)colorComponentFrom:(NSString *)string start:(NSUInteger)start length:(NSUInteger)length
{
    NSString *substring = [string substringWithRange: NSMakeRange(start, length)];
    NSString *fullHex = length == 2 ? substring : [NSString stringWithFormat: @"%@%@", substring, substring];
    unsigned hexComponent;
    [[NSScanner scannerWithString: fullHex] scanHexInt: &hexComponent];
    
    return hexComponent / 255.0;
}

@end
