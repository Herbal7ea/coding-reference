//
//  ActionViewController.m
//  ActionExtension
//
//  Created by Brian Mullen on 9/19/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "ActionViewController.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "UIColor+HexString.h"


typedef struct
{
    float Position[3];
    float Color[4];
} Vertex;

const Vertex Vertices[ ] =
{
    {{1, -1, 0}, {1, 0, 0, 1}},
    {{1, 1, 0}, {0, 1, 0, 1}},
    {{-1, 1, 0}, {0, 0, 1, 1}},
    {{-1, -1, 0}, {0, 0, 0, 1}}
};

const GLubyte Indices[ ] =
{
    0, 1, 2,
    2, 3, 0
};




@interface ActionViewController ( )
@property (nonatomic, assign) float currentRed;
@property (nonatomic, assign) float currentGreen;
@property (nonatomic, assign) float currentBlue;
@property (nonatomic, assign) float currentAlpha;
@property (nonatomic, assign) BOOL increasing;
@property (nonatomic, strong) EAGLContext *context;
@property (nonatomic, assign) GLuint vertexBuffer;
@property (nonatomic, assign) GLuint indexBuffer;
@property (nonatomic, strong) GLKBaseEffect *effect;
@property (nonatomic, assign) float rotation;
@property (nonatomic, assign) float rotationDirection;
@property (nonatomic, assign) float rotationSpeed;
@property (nonatomic, assign) float rotationSpeedMax;
@property (nonatomic, assign) float rotationSpeedIncrement;
@end


@implementation ActionViewController

- (void)dealloc
{
    [EAGLContext setCurrentContext:_context];
    glDeleteBuffers( 1, &_vertexBuffer );
    glDeleteBuffers( 1, &_indexBuffer );
    _effect = nil;
    
    [EAGLContext setCurrentContext:nil];
    _context = nil;
}

- (void)setupOpenGL
{
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    if( self.context == nil )
    {
        NSLog( @"Failed to create ES context" );
    }
    
    GLKView *view = (GLKView *)self.view;
    view.context = self.context;
    
    
    [EAGLContext setCurrentContext:self.context];
    
    self.effect = [[GLKBaseEffect alloc] init];
    
    glGenBuffers( 1, &_vertexBuffer );
    glBindBuffer( GL_ARRAY_BUFFER, _vertexBuffer );
    glBufferData( GL_ARRAY_BUFFER, sizeof( Vertices ), Vertices, GL_STATIC_DRAW );
    
    glGenBuffers( 1, &_indexBuffer );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, _indexBuffer );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof(Indices), Indices, GL_STATIC_DRAW );
}

- (NSString *)parseValueForKey:(NSString *)key withData:(NSString *)data
{
    NSString *result = nil;
    
    NSRange dataKeyRange = [data rangeOfString:key];
    if( dataKeyRange.location != NSNotFound )
    {
        result = [data substringFromIndex:dataKeyRange.location + key.length];
            
        NSRange separator = [result rangeOfString:@";"];
        if( separator.location != NSNotFound )
        {
            result = [result substringToIndex:separator.location];
        }
    }
    else
    {
        NSLog( @"%@ not found!!!", key );
    }
    
    return result;
}

- (void)parseColorData:(NSString *)data
{
    NSString *colorValue = [self parseValueForKey:@"color:" withData:data];
    
    if( colorValue != nil )
    {
        NSArray *colorComponents = [UIColor colorComponentsWithHexString:colorValue];
        
        self.currentRed = [(NSNumber *)colorComponents[ 0 ] floatValue];
        self.currentGreen = [(NSNumber *)colorComponents[ 1 ] floatValue];
        self.currentBlue = [(NSNumber *)colorComponents[ 2 ] floatValue];
        self.currentAlpha = [(NSNumber *)colorComponents[ 3 ] floatValue];
    }
}

- (void)parseRotationData:(NSString *)data
{
    NSString *rotationSpeedValue = [self parseValueForKey:@"rotationSpeed:" withData:data];
    if( rotationSpeedValue != nil )
    {
        self.rotationSpeed = [rotationSpeedValue floatValue];
    }
    
    NSString *rotationSpeedMaxValue = [self parseValueForKey:@"rotationSpeedMax:" withData:data];
    if( rotationSpeedMaxValue != nil )
    {
        self.rotationSpeedMax = [rotationSpeedMaxValue floatValue];
    }
    
    NSString *rotationSpeedIncrementValue = [self parseValueForKey:@"rotationSpeedIncrement:" withData:data];
    if( rotationSpeedIncrementValue != nil )
    {
        self.rotationSpeedIncrement = [rotationSpeedIncrementValue floatValue];
        
        if( self.rotationSpeed < self.rotationSpeedIncrement )
        {
            self.rotationSpeed = self.rotationSpeedIncrement;
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupOpenGL];
    self.currentRed = 1.0f;
    self.currentGreen = 0.0f;
    self.currentBlue = 0.0f;
    self.currentAlpha = 1.0f;
    self.rotation = 0.0f;
    self.rotationDirection = 1.0f;
    self.rotationSpeed = 0.0f;
    self.rotationSpeedMax = 360.0f;
    self.rotationSpeedIncrement = 90.0f;
    
    
    
//    BOOL canHandleRequest = NO;
    for( NSExtensionItem *item in self.extensionContext.inputItems )
    {
        for( NSItemProvider *itemProvider in item.attachments )
        {
            if( [itemProvider hasItemConformingToTypeIdentifier:@"com.glasses.vto"] )
            {
//                canHandleRequest = YES;
                
                [itemProvider loadItemForTypeIdentifier:@"com.glasses.vto" options:nil completionHandler:^( NSData *item, NSError *error )
                {
                    NSString *itemData = [[NSString alloc] initWithData:item encoding:NSUTF8StringEncoding];
                    NSLog( @"itemData: %@", itemData );
                    
                    [self parseColorData:itemData];
                    [self parseRotationData:itemData];
                    
//                    self.titleLabel.text = itemData;
                }];
                
                break;
            }
        }
        
//        if( canHandleRequest )
//        {
//            break;
//        }
    }
    
//TODO: figure out how to cancel a request if items are invalid
//    if( !canHandleRequest )
//    {
//        NSDictionary *userInfo = @{ NSLocalizedDescriptionKey:@"Unable to handle request." };
//        NSError *error = [NSError errorWithDomain:@"com.extensionApp.launch" code:1 userInfo:userInfo];
//        
//        [self.extensionContext cancelRequestWithError:error];
//    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)done
{
    // Return any edited content to the host app.
    // This template doesn't do anything, so we just echo the passed in items.
    [self.extensionContext completeRequestReturningItems:self.extensionContext.inputItems completionHandler:nil];
}

#pragma mark - GLKViewDelegate

- (void)glkView:(GLKView *)view drawInRect:(CGRect)rect
{
    glClearColor( self.currentRed, self.currentGreen, self.currentBlue, self.currentAlpha );
    glClear( GL_COLOR_BUFFER_BIT );
    
    [self.effect prepareToDraw];
    
    glBindBuffer( GL_ARRAY_BUFFER, _vertexBuffer );
    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, _indexBuffer );
    
    glEnableVertexAttribArray( GLKVertexAttribPosition );
    glVertexAttribPointer( GLKVertexAttribPosition, 3, GL_FLOAT, GL_FALSE, sizeof( Vertex ), (const GLvoid *) offsetof( Vertex, Position ) );
    glEnableVertexAttribArray( GLKVertexAttribColor);
    glVertexAttribPointer( GLKVertexAttribColor, 4, GL_FLOAT, GL_FALSE, sizeof( Vertex ), (const GLvoid *) offsetof( Vertex, Color ) );
    
    glDrawElements( GL_TRIANGLES, sizeof( Indices ) / sizeof( Indices[ 0 ] ), GL_UNSIGNED_BYTE, 0 );
}

#pragma mark - GLKViewControllerDelegate

- (void)update
{
    float aspect = fabsf(self.view.bounds.size.width / self.view.bounds.size.height);
    GLKMatrix4 projectionMatrix = GLKMatrix4MakePerspective(GLKMathDegreesToRadians(65.0f), aspect, 4.0f, 10.0f);
    self.effect.transform.projectionMatrix = projectionMatrix;
    
    GLKMatrix4 modelViewMatrix = GLKMatrix4MakeTranslation(0.0f, 0.0f, -6.0f);
    self.rotation += self.rotationSpeed * self.timeSinceLastUpdate;
    modelViewMatrix = GLKMatrix4Rotate(modelViewMatrix, GLKMathDegreesToRadians(self.rotation * self.rotationDirection), 0, 0, 1);
    self.effect.transform.modelviewMatrix = modelViewMatrix;
}

#pragma mark - Touch Handling

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.rotationDirection *= -1.0f;
}

@end
