//
//  UIColor+HexString.h
//  ExtensionApp
//
//  Created by Brian Mullen on 9/23/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UIColor (HexString)

+ (NSArray *)colorComponentsWithHexString:(NSString *)hexString;
+ (UIColor *)colorWithHexString:(NSString *)hexString;

@end
