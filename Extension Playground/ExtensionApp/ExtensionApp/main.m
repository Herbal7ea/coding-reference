//
//  main.m
//  ExtensionApp
//
//  Created by Brian Mullen on 9/18/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"


int main( int argc, char * argv[ ] )
{
    @autoreleasepool
    {
        return UIApplicationMain( argc, argv, nil, NSStringFromClass( AppDelegate.class ) );
    }
}
