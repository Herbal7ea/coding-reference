//
//  AppDelegate.h
//  ExtensionApp
//
//  Created by Brian Mullen on 9/18/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

