//
//  LocalHTTPServer.h
//  AncestryTest
//
//  Created by Brian Mullen on 8/25/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "HTTPServer.h"


@interface LocalHTTPServer : HTTPServer

+ (instancetype)sharedInstance;

@end
