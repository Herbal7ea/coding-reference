//
//  APIClient.m
//  AncestryTest
//
//  Created by Brian Mullen on 8/25/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "APIClient.h"


NSString * const kResultsKey = @"results";
NSString * const kBaseUrl = @"http://whoismyrepresentative.com";


@implementation APIClient

+ (instancetype)sharedInstance
{
    static APIClient *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once( &oncePredicate, ^
    {
        _sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:kBaseUrl]];
                      
        NSMutableSet *contentTypes = [NSMutableSet setWithSet:_sharedInstance.responseSerializer.acceptableContentTypes];
        [contentTypes addObject:@"text/html"];
        _sharedInstance.responseSerializer.acceptableContentTypes = contentTypes;
    } );
    
    return _sharedInstance;
}

+ (NSError *)jsonParsingError
{
    NSDictionary *userInfo = @{ NSLocalizedDescriptionKey:@"Unable to parse JSON data." };
    NSError *error = [NSError errorWithDomain:@"com.whoismyrepresentative" code:APIClientErrorJsonParsing userInfo:userInfo];
    
    return error;
}

- (void)fetchSenatorsByState:(NSString *)state success:(void(^)(NSArray *senators))success failure:(void(^)(NSError *error))failure
{
    NSDictionary *parameters = @{
                                    @"state": state,
                                    @"output": @"json"
                                };
    
    [self GET:@"getall_sens_bystate.php" parameters:parameters success:^( NSURLSessionDataTask *task, id responseObject )
    {
        if( [responseObject isKindOfClass:NSDictionary.class] )
        {
            if( success )
            {
                success( [responseObject objectForKey:kResultsKey] );
            }
        }
        else
        {
            if( failure )
            {
                failure( [APIClient jsonParsingError] );
            }
        }
    }
    failure:^( NSURLSessionDataTask *task, NSError *error )
    {
        if( failure )
        {
            failure( error );
        }
    }];
}

@end
