//
//  APIClient.h
//  AncestryTest
//
//  Created by Brian Mullen on 8/25/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "AFHTTPSessionManager.h"


extern NSString * const kResultsKey;


typedef NS_ENUM( NSInteger, APIClientError )
{
    APIClientErrorJsonParsing,
    APIClientErrorFileNotFound,
    APIClientErrorFetchFailed
};


@interface APIClient : AFHTTPSessionManager

+ (instancetype)sharedInstance;
+ (NSError *)jsonParsingError;

- (void)fetchSenatorsByState:(NSString *)state success:(void(^)(NSArray *senators))success failure:(void(^)(NSError *error))failure;

@end
