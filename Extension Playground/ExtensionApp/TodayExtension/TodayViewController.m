//
//  TodayViewController.m
//  TodayExtension
//
//  Created by Brian Mullen on 9/18/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>
#import "APIClient.h"


@interface TodayViewController ( ) <NCWidgetProviding>
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@end


@implementation TodayViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler
{
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData
    
    [APIClient.sharedInstance fetchSenatorsByState:@"UT" success:^( NSArray *senators )
    {
        BOOL updatedData = NO;
        NSDictionary *senatorData = senators.count > 0 ? senators[ 0 ] : nil;
        
        if( senatorData != nil )
        {
            NSString *senatorName = [senatorData objectForKey:@"name"];
            
            if( ![self.titleLabel.text isEqualToString:senatorName] )
            {
                self.titleLabel.text = senatorName;
                
                updatedData = YES;
            }
        }
        
        if( updatedData )
        {
            completionHandler( NCUpdateResultNewData );
        }
        else
        {
            completionHandler( NCUpdateResultNoData );
        }
    }
    failure:^( NSError *error )
    {
        completionHandler( NCUpdateResultFailed );
    }];
}

//- (UIEdgeInsets)widgetMarginInsetsForProposedMarginInsets:(UIEdgeInsets)defaultMarginInsets
//{
//    return UIEdgeInsetsZero;
//}

@end
