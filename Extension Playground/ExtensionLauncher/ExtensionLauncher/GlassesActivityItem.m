//
//  GlassesActivityItem.m
//  ExtensionLauncher
//
//  Created by Brian Mullen on 9/19/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "GlassesActivityItem.h"


@interface GlassesActivityItem ( ) <UIActivityItemSource>
@property (nonatomic, strong) NSData<NSSecureCoding, NSCoding> *data;
@end


@implementation GlassesActivityItem

+ (instancetype)activityItemWithData:(NSData<NSSecureCoding, NSCoding> *)data
{
    return [[self alloc] initWithItemData:data];
}

- (instancetype)initWithItemData:(NSData<NSSecureCoding, NSCoding> *)data
{
    self = [super init];
    
    if( self )
    {
        _data = data;
    }
    
    return self;
}

#pragma mark - NSCoding

- (id)initWithCoder:(NSCoder *)decoder
{
    return [self initWithItemData:[decoder decodeObjectOfClass:NSData.class forKey:@"data"]];
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    [encoder encodeObject:self.data forKey:@"data"];
}

#pragma mark - NSSecureCoding

+ (BOOL)supportsSecureCoding
{
    return YES;
}

#pragma mark - UIActivityItemSource

- (id)activityViewControllerPlaceholderItem:(UIActivityViewController *)activityViewController
{
    return self.data;
}

- (id)activityViewController:(UIActivityViewController *)activityViewController itemForActivityType:(NSString *)activityType
{
    return self.data;
}

- (NSString *)activityViewController:(UIActivityViewController *)activityViewController dataTypeIdentifierForActivityType:(NSString *)activityType
{
    return @"com.glasses.vto";
}

@end
