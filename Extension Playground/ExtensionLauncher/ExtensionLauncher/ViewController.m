//
//  ViewController.m
//  ExtensionLauncher
//
//  Created by Brian Mullen on 9/19/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "ViewController.h"
#import "GlassesActivityItem.h"


@interface ViewController ( )
@property (nonatomic, strong) IBOutlet UIButton *redBackgroundButton;
@property (nonatomic, strong) IBOutlet UIButton *greenBackgroundButton;
@property (nonatomic, strong) IBOutlet UIButton *blueBackgroundButton;
@end


@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)launchExtensionWithItems:(NSArray *)items
{
    UIActivityViewController *activityController = [[UIActivityViewController alloc] initWithActivityItems:items applicationActivities:nil];
    activityController.excludedActivityTypes = @[ UIActivityTypePostToFacebook,
                                                  UIActivityTypePostToTwitter,
                                                  UIActivityTypePostToWeibo,
                                                  UIActivityTypeMessage,
                                                  UIActivityTypeMail,
                                                  UIActivityTypePrint,
                                                  UIActivityTypeCopyToPasteboard,
                                                  UIActivityTypeAssignToContact,
                                                  UIActivityTypeSaveToCameraRoll,
                                                  UIActivityTypeAddToReadingList,
                                                  UIActivityTypePostToFlickr,
                                                  UIActivityTypePostToVimeo,
                                                  UIActivityTypePostToTencentWeibo,
                                                  UIActivityTypeAirDrop
                                                ];
    
    [self presentViewController:activityController animated:YES completion:nil];
}

- (IBAction)onRedSlowButtonTapped:(id)sender
{
    NSMutableArray *sharingItems = [NSMutableArray array];
    [sharingItems addObject:[GlassesActivityItem activityItemWithData:[@"upc:1234567890;color:#FF0000;colorName:red;rotationSpeed:90;rotationSpeedMax:360;rotationSpeedIncrement:135" dataUsingEncoding:NSUTF8StringEncoding]]];
    
    [self launchExtensionWithItems:sharingItems];
}

- (IBAction)onRedMediumButtonTapped:(id)sender
{
    NSMutableArray *sharingItems = [NSMutableArray array];
    [sharingItems addObject:[GlassesActivityItem activityItemWithData:[@"upc:1234567890;color:#FF0000;colorName:red;rotationSpeed:225;rotationSpeedMax:360;rotationSpeedIncrement:135" dataUsingEncoding:NSUTF8StringEncoding]]];
    
    [self launchExtensionWithItems:sharingItems];
}

- (IBAction)onRedFastButtonTapped:(id)sender
{
    NSMutableArray *sharingItems = [NSMutableArray array];
    [sharingItems addObject:[GlassesActivityItem activityItemWithData:[@"upc:1234567890;color:#FF0000;colorName:red;rotationSpeed:360;rotationSpeedMax:360;rotationSpeedIncrement:135" dataUsingEncoding:NSUTF8StringEncoding]]];
    
    [self launchExtensionWithItems:sharingItems];
}

- (IBAction)onGreenSlowButtonTapped:(id)sender
{
    NSMutableArray *sharingItems = [NSMutableArray array];
    [sharingItems addObject:[GlassesActivityItem activityItemWithData:[@"upc:1234567890;color:#00FF00;colorName:green;rotationSpeed:90;rotationSpeedMax:360;rotationSpeedIncrement:135" dataUsingEncoding:NSUTF8StringEncoding]]];
    
    [self launchExtensionWithItems:sharingItems];
}

- (IBAction)onGreenMediumButtonTapped:(id)sender
{
    NSMutableArray *sharingItems = [NSMutableArray array];
    [sharingItems addObject:[GlassesActivityItem activityItemWithData:[@"upc:1234567890;color:#00FF00;colorName:green;rotationSpeed:225;rotationSpeedMax:360;rotationSpeedIncrement:135" dataUsingEncoding:NSUTF8StringEncoding]]];
    
    [self launchExtensionWithItems:sharingItems];
}

- (IBAction)onGreenFastButtonTapped:(id)sender
{
    NSMutableArray *sharingItems = [NSMutableArray array];
    [sharingItems addObject:[GlassesActivityItem activityItemWithData:[@"upc:1234567890;color:#00FF00;colorName:green;rotationSpeed:360;rotationSpeedMax:360;rotationSpeedIncrement:135" dataUsingEncoding:NSUTF8StringEncoding]]];
    
    [self launchExtensionWithItems:sharingItems];
}

- (IBAction)onBlueSlowButtonTapped:(id)sender
{
    NSMutableArray *sharingItems = [NSMutableArray array];
    [sharingItems addObject:[GlassesActivityItem activityItemWithData:[@"upc:1234567890;color:#0000FF;colorName:blue;rotationSpeed:90;rotationSpeedMax:360;rotationSpeedIncrement:135" dataUsingEncoding:NSUTF8StringEncoding]]];
    
    [self launchExtensionWithItems:sharingItems];
}

- (IBAction)onBlueMediumButtonTapped:(id)sender
{
    NSMutableArray *sharingItems = [NSMutableArray array];
    [sharingItems addObject:[GlassesActivityItem activityItemWithData:[@"upc:1234567890;color:#0000FF;colorName:blue;rotationSpeed:225;rotationSpeedMax:360;rotationSpeedIncrement:135" dataUsingEncoding:NSUTF8StringEncoding]]];
    
    [self launchExtensionWithItems:sharingItems];
}

- (IBAction)onBlueFastButtonTapped:(id)sender
{
    NSMutableArray *sharingItems = [NSMutableArray array];
    [sharingItems addObject:[GlassesActivityItem activityItemWithData:[@"upc:1234567890;color:#0000FF;colorName:blue;rotationSpeed:360;rotationSpeedMax:360;rotationSpeedIncrement:135" dataUsingEncoding:NSUTF8StringEncoding]]];
    
    [self launchExtensionWithItems:sharingItems];
}

@end
