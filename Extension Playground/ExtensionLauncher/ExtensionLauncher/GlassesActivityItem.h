//
//  GlassesActivityItem.h
//  ExtensionLauncher
//
//  Created by Brian Mullen on 9/19/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface GlassesActivityItem : NSObject

+ (instancetype)activityItemWithData:(NSData<NSSecureCoding, NSCoding> *)data;
- (instancetype)initWithItemData:(NSData<NSSecureCoding, NSCoding> *)data;

@end
