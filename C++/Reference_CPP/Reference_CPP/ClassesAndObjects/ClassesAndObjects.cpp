//
//  ClassesAndObjects.cpp
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#include "ClassesAndObjects.h"
#include "Person1.h"
#include "Tweeter.h"
#include "status.h"
#include <iostream>

using std::cout;
using std::endl;

void ClassesAndObjects::run()
{
	Person1 p1("Kate", "Gregory", 123);
	{
		Tweeter t1("Someone", "Else", 456, "@whoever");
	}

	cout << "after innermost block" << endl;
	Status s = Pending;
	s = Approved;
}


