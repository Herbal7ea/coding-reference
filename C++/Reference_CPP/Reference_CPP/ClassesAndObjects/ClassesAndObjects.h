//
//  ClassesAndObjects.h
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#ifndef __Reference_CPP__ClassesAndObjects__
#define __Reference_CPP__ClassesAndObjects__

class ClassesAndObjects
{

public:
	static void run();

};


#endif /* defined(__Reference_CPP__ClassesAndObjects__) */
