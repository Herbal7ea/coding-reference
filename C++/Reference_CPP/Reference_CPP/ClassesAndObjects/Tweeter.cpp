#include "Tweeter.h"
#include <iostream>

using namespace std;

Tweeter::Tweeter(string first, string last, int arbitrary, string handle) :
Person1(first, last, arbitrary),
twitterhandle(handle)
{
	std::cout << "constructing tweeter" << twitterhandle << std::endl;
}

Tweeter::~Tweeter(void)
{
	std::cout << "destructing tweeter" << twitterhandle << std::endl;
}
