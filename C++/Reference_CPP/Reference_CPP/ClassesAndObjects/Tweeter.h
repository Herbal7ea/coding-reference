#pragma once
#include "Person1.h"
#include <string>

using namespace std;

class Tweeter : public Person1
{
private:
	string twitterhandle;
public:
	Tweeter(std::string first, std::string last, int arbitrary, std::string handle);
	~Tweeter(void);
};

