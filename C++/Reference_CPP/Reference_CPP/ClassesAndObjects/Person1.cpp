#include "Person1.h"
#include <iostream>

using namespace std;

Person1::Person1(string first,string last, int arbitrary) :
	firstname(first),
	lastname(last),
	arbitrarynumber(arbitrary)
{
	cout << "constructing " << firstname << " " << lastname << endl;
}

Person1::~Person1()
{
	cout << "destructing " << firstname << " " << lastname << endl;
}