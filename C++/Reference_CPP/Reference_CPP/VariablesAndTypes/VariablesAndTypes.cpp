//
//  VariablesAndTypes.cpp
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#include "VariablesAndTypes.h"

#include <iostream>

using namespace std;

void VariablesAndTypes::run()
{

//	variablesExample();
//	forLoops();
}

void VariablesAndTypes::variablesExample()
{
	int i1 = 1L;
	cout << "i1= " << i1 << endl;
	int i2;
	i2 = 2;
	cout << "i2= " << i2 << endl;
	int i3(3);
	cout << "i3= " << i3 << endl;

	double d1 = 2.2;
	double d2 = i1;
	int i4 = (int) d1;
	cout << "d1= " << d1 << endl;
	cout << "d2= " << d2 << endl;
	cout << "i4= " << i4 << endl;

	char c1 = 'a';
	//char c2 = "b";
	cout << "c1= " << c1 << endl;
	//cout << "c2= " << c2 << endl;

	bool flag = false;
	cout << "flag= " << flag << endl;
	flag =  (bool) i1;
	cout << "flag= " << flag << endl;
	flag =  (bool) d1;
	cout << "flag= " << flag << endl;

	/*	unsigned char n1 = 128;
		char n2 = 128;
		cout << "n1= " << n1 << endl;
		cout << "n2= " << n2 << endl;
		n1 = 254;
		n2 = 254;
		cout << "n1= " << n1 << endl;
		cout << "n2= " << n2 << endl;
		n1 = 300;
		n2 = 300;
		cout << "n1= " << n1 << endl;
		cout << "n2= " << n2 << endl;
	*/}

void VariablesAndTypes::forLoops()
{
	int x;
	cout << "Enter a number" << endl;
	cin >> x ;

	bool prime = true;
	for (int i=2; i <= x/i; i = i + 1)
	{
		int factor = x/i;
		if (factor*i == x)
		{
			cout << "factor found: " << factor << endl;
			prime = false;
			break;
		}
	}

	cout << x << " is " ;
	if (prime)
		cout << "prime" << endl;
	else
		cout << "not prime" << endl;
}
