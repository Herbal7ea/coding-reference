//
//  VariablesAndTypes.h
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#ifndef __Reference_CPP__VariablesAndTypes__
#define __Reference_CPP__VariablesAndTypes__

class VariablesAndTypes
{

public:
	static void run();

	static void variablesExample();

	static void forLoops();
};

#endif /* defined(__Reference_CPP__VariablesAndTypes__) */
