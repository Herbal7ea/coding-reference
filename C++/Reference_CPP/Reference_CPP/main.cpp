//
//  main.cpp
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#import "SimpleOutput.h"
#import "ClassesAndObjects.h"
#include "VariablesAndTypes.h"
#import "Functions.h"
#include "FlowControl.h"
#import "Arithmetic.h"
#include "ClassesAndObjects2.h"
#import "Comparison.h"
#include "Accumulator.h"
#import "MaxExample.h"
#include "ConstExample.h"
#include "FreeStoreExample.h"
#include "Memory.h"

void runExamples();

int main(int argc, const char * argv[])
{
	runExamples();

	return 0;
}

void runExamples()
{

	Memory::run();
//	FreeStoreExample::run();
//	ConstExample::run();
//	MaxExample::run();
//	Accumulator::run();
//	Comparison::run();
//	ClassesAndObjects2::run();
//	Arithmetic::run();
//	FlowControl::run();
//	Functions::run();
//	VariablesAndTypes::run();
//	ClassesAndObjects::run();
//	SimpleOutput::run();
}



