//
//  MaxExample.h
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#ifndef __Reference_CPP__MaxExample__
#define __Reference_CPP__MaxExample__

class MaxExample
{

public:
	static void run();

};

#endif /* defined(__Reference_CPP__MaxExample__) */
