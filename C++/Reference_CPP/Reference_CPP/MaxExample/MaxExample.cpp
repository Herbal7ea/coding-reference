// max.cpp : Defines the entry point for the console application.
//
//  MaxExample.cpp
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#include "MaxExample.h"
#include <iostream>
#include <string>
#include "Person.h"
#include "BankAccount.h"

using namespace std;

template <class T>
T max(T& t1, T& t2)
{
	return t1 < t2? t2: t1;
}

void MaxExample::run()
{
	string s1 = "hello";
	string s2 = "world";

	Person p1("Kate", "Gregory", 123);
	Person p2("Someone", "Else", 456);

	cout << "max of 33 and 44 is " << max(33, 44) << endl;
	cout << "max of " << s1 << " and " << s2 << " is " << max(s1,s2) << endl;
	cout << "max of " << p1.GetName() << " and " << p2.GetName() << " is " << max(p1,p2).GetName() << endl;


//	BankAccount b1;
//	BankAccount b2;
//	cout << "max of " << b1.GetHolderName() << " and " << b2.GetHolderName() << " is " << max(b1,b2).GetHolderName() << endl;
}