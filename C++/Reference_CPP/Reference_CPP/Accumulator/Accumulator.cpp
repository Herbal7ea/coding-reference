//
//  Accumulator.cpp
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#include "Accumulator.h"
#include "Accum.h"
#include "Person.h"
#include <iostream>

using namespace std;

void Accumulator::run()
{
	Accum<int> integers(0);
	integers += 3;
	integers += 7;
	cout << integers.GetTotal() << endl;

	Accum<string> strings("");
	strings += "hello";
	strings += " world";
	cout << strings.GetTotal() << endl;

	//integers += "testing";
	//strings += 4;

	//Person start("","",0);
	Accum<Person> people(0);
	Person p1("Kate","Gregory",123);
	Person p2("Someone","Else",456);
	people += p1;
	people += p2;
	cout << people.GetTotal() << endl;
}