#include "Memory.h"
#include "Person.h"

void Memory::run()
{
	Person Kate("Kate", "Gregory", 345);
	Kate.AddResource();
	Kate.SetFirstName("Kate2");
	Kate.AddResource();
	Person Kate2 = Kate;
	Kate = Kate2;
	string s1 = Kate.GetResourceName();
}