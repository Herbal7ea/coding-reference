#include <iostream>
#include <string>
#include "SimpleOutput.h"

using namespace std;

void SimpleOutput::run()
{
	string name;

	cout << "Type your name" << endl;
	cin >> name;
	cout << "Hello, " << name << endl;
}