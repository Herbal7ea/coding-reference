#include <string>
#include <memory>
#include "Resource.h"

#ifndef _Person_H
#define _Person_H

using namespace std;

class Person
{
private:
	string firstname;
	string lastname;
	int arbitrarynumber;
	shared_ptr<Resource> pResource;
public:
	Person(string first, string last, int arbitrary);
	~Person();
	string GetName();
	int GetNumber() const { return arbitrarynumber; }
	void SetNumber(int number) { arbitrarynumber = number; }
	void SetFirstName(string first) { firstname = first; }
	bool operator<(Person& p);
	bool operator<(int i);
	friend bool operator<(int i, Person& p);
	void AddResource();
	string GetResourceName() const { return pResource->GetName(); }
};
bool operator<(int i, Person& p);
#endif