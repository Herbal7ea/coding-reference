//
//  Arithmetic.cpp
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#include "Arithmetic.h"
#include "Utility.h"
#include <iostream>

using namespace std;

void Arithmetic::run()
{
	int i=0;
	cout << "i " << i << endl;
	i += 2;
	cout << "i " << i << endl;
	i *= 3;
	cout << "i " << i << endl;
	i -= 2;
	cout << "i " << i << endl;
	i /= 4;
	cout << "i " << i << endl;

	int j = i++;
	cout << "i " << i << ", j " << j << endl;
	j = ++i;
	cout << "i " << i << ", j " << j << endl;
	j = i--;
	cout << "i " << i << ", j " << j << endl;
	j = --i;
	cout << "i " << i << ", j " << j << endl;

	i = 2;
	j = 0;
	while (i<101)
		j += IsPrime(i++)?1:0;
	cout << "I found " << j << " primes up to 100" << endl;
}