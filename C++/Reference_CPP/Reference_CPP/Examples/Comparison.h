//
//  Comparison.h
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#ifndef __Reference_CPP__Comparison__
#define __Reference_CPP__Comparison__

class Comparison
{

public:
	static void run();

};

#endif /* defined(__Reference_CPP__Comparison__) */
