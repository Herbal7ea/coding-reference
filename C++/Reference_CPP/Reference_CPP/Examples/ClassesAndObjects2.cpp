//
//  ClassesAndObjects2.cpp
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#include "ClassesAndObjects2.h"
#import "Person.h"
#include <iostream>

using namespace std;

void ClassesAndObjects2::run()
{
	Person p1("Kate", "Gregory", 123);
	Person p2("Someone", "Else", 456);

	cout << "p1 is ";
	if (!(p1 < p2)) cout << "not ";
	cout << "less than p2" << endl;

	cout << "p1 is ";
	if (!(p1 < 300)) cout << "not ";
	cout << "less than 300" << endl;

	cout << "300 is ";
	if (!(300 < p1)) cout << "not ";
	cout << "less than p1" << endl;
}