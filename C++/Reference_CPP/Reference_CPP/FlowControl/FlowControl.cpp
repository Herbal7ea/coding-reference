//
//  If.cpp
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#include "FlowControl.h"
#include <iostream>

using namespace std;

void FlowControl::run()
{
//	switch_statements();
	while_statements2();
//	while_statements();
//	if_statements();

}

void FlowControl::ifStatements()
{
	int x,y;
	cout << "Enter two numbers" << endl;
	cin >> x >> y;

	cout << x << " " ;
	if (x > y)
	{
		cout << "is larger than ";
	}
	else
	{
		cout << "is not larger than ";
	}
	cout << y << endl;

	if (x+y > 10)
		cout << "thanks for choosing larger numbers!" << endl;
}

void FlowControl::while_statements()
{
	int x = 99;
	while (x > 0)
	{
		cout << "Enter a number, 0 to quit" << endl;
		cin >> x ;
		string sign = x > 0 ? "positive": "0 or negative";
		cout << "your number is " << sign << endl;
	}
}

void FlowControl::switch_statements()
{
	int x;
	cout << "Enter a number, 0 to quit" << endl;
	cin >> x ;
	while (x > 0)
	{

		switch(x)
		{
			case 1:
				cout << "you entered 1" << endl;
				break;
			case 2:
			case 3:
				cout << "you entered 2 or 3" << endl;
				break;
			case 4:
				cout << "you entered 4" << endl;
			case 5:
				cout << "you entered 5" << endl;
				break;
			default:
				cout << "you entered other than 1-5" << endl;
		}
		cout << "Enter a number, 0 to quit" << endl;
		cin >> x ;
	}
}

void FlowControl::while_statements2()
{
	int x;
	cout << "Enter a number" << endl;
	cin >> x ;

	bool prime = true;
	int i=2;

	while(i <= x/i)
	{
		int factor = x/i;
		if (factor*i == x)
		{
			cout << "factor found: " << factor << endl;
			prime = false;
			break;
		}
		i = i + 1;
	}

	cout << x << " is " ;
	if (prime)
		cout << "prime" << endl;
	else
		cout << "not prime" << endl;


}
