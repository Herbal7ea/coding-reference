//
//  If.h
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#ifndef __Reference_CPP__If__
#define __Reference_CPP__If__

class FlowControl
{
public:
	static void run();

	static void ifStatements();

	static void while_statements();

	static void switch_statements();

	static void while_statements2();
};

#endif /* defined(__Reference_CPP__If__) */
