//
//  Functions.cpp
//  Reference_CPP
//
//  Created by jbott on 3/31/14.
//  Copyright (c) 2014 Jon Bott. All rights reserved.
//

#include "Functions.h"
#include <iostream>
#include "Utility.h"

using namespace std;

void Functions::run()
{
	int x;
	cout << "Enter a number" << endl;
	cin >> x ;
    
	if (IsPrime(x))
		cout <<  x << " is prime" << endl;
	else
		cout <<  x << " is not prime" << endl;
    
	if (Is2MorePrime(x))
		cout << x << "+2 is prime" << endl;
	else
		cout << x << "+2 is not prime" << endl;

}