//
//  APIClient.m
//  Playground
//
//  Created by Brian Mullen on 8/21/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "APIClient.h"
#import "MovieModel.h"


@implementation APIClient

+ (instancetype)sharedInstance
{
    static APIClient *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once( &oncePredicate, ^
    {
        _sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:@"http://localhost:12866"]];
    } );
    
    return _sharedInstance;
}

- (void)fetchMovies
{
    [self GET:@"movies.json" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {

        [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
            
            [MovieModel MR_importFromArray:responseObject inContext:localContext];
            
        }];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog( @"FAILED: %@", error );
    }];
}


//    [self GET:@"movies.json" parameters:nil success:^( NSURLSessionDataTask *task, NSArray *responseObject )
//    {
//        [MagicalRecord saveWithBlockAndWait:^( NSManagedObjectContext *localContext )
//        {
////  Create a new object regardless of what is currently in the database
////            MovieModel *movie = [MovieModel MR_createInContext:localContext];
////            [movie MR_importValuesForKeysWithObject:responseObject[ 0 ]];
//
//
////  Import an object but make sure it doesn't exist first using the 'relatedByAttribute' value defined in the xcdatamodeld
////            [MovieModel MR_importFromObject:responseObject[ 0 ] inContext:localContext];
//
////  Import a list of movies
//            [MovieModel MR_importFromArray:responseObject inContext:localContext];
//        }];
//    }
//    failure:^( NSURLSessionDataTask *task, NSError *error )
//    {
//        NSLog( @"FAILED: %@", error );
//    }];


@end
