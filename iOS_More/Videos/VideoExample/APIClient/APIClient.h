//
//  APIClient.h
//  Playground
//
//  Created by Brian Mullen on 8/21/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "AFHTTPSessionManager.h"


@interface APIClient : AFHTTPSessionManager

+ (instancetype)sharedInstance;

- (void)fetchMovies;

@end
