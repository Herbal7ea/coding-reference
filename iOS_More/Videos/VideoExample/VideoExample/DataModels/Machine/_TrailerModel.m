// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to TrailerModel.m instead.

#import "_TrailerModel.h"

const struct TrailerModelAttributes TrailerModelAttributes = {
	.title = @"title",
	.url = @"url",
};

const struct TrailerModelRelationships TrailerModelRelationships = {
	.movies = @"movies",
};

const struct TrailerModelFetchedProperties TrailerModelFetchedProperties = {
};

@implementation TrailerModelID
@end

@implementation _TrailerModel

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"TrailerModel" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"TrailerModel";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"TrailerModel" inManagedObjectContext:moc_];
}

- (TrailerModelID*)objectID {
	return (TrailerModelID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	

	return keyPaths;
}




@dynamic title;






@dynamic url;






@dynamic movies;

	






@end
