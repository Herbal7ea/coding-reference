//
//  SKFirstViewController.m
//  VideoExample
//
//  Created by jbott on 8/25/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import "SKFirstViewController.h"
#import "APIClient.h"
#import "MovieModel.h"
#import "GenreModel.h"
#import "TrailerModel.h"

@interface SKFirstViewController ()

@end

@implementation SKFirstViewController

- (IBAction)loadData:(id)sender
{
    [[APIClient sharedInstance] fetchMovies];
}

- (IBAction)logData:(id)sender
{
	[self logMovies];
	[self logGenres];
	[self logTrailers];
}

- (void)logMovies
{
	NSArray *movies = [MovieModel MR_findAll];


	NSLog( @"============== MOVIE BREAKOUT ===============" );
	NSLog( @"Movies: %lu", (unsigned long)movies.count );

	[movies enumerateObjectsUsingBlock:^( MovieModel *movie, NSUInteger movieIndex, BOOL *movieStop )
	{
		NSLog( @"Movie: %@", movie.title );
		NSLog( @"\tGenres: %lu", (unsigned long)movie.genres.count );

		[movie.genres enumerateObjectsUsingBlock:^( GenreModel *genre, NSUInteger genreIndex, BOOL *genreStop){
			NSLog( @"\t\tGenre: %@", genre.name );
		}];

		NSLog( @"\tTrailers: %lu", (unsigned long)movie.trailers.count );

		[movie.trailers enumerateObjectsUsingBlock:^( TrailerModel *trailer, NSUInteger trailerIndex, BOOL *trailerStop ){
			NSLog( @"\t\tTrailer: %@", trailer.title );
		}];
	}];
	NSLog( @"==============================================" );
}

- (void)logGenres
{
	NSArray *genres = [GenreModel MR_findAll];

	NSLog( @"============= GENRE BREAKOUT ================" );
	NSLog( @"Genres: %lu", (unsigned long)genres.count );

	[genres enumerateObjectsUsingBlock:^( GenreModel *genre, NSUInteger genreIndex, BOOL *genreStop ){
		NSLog( @"Genre: %@", genre.name );
	}];
	NSLog( @"=============================\n\n\n" );
}

- (void)logTrailers
{
	NSArray *trailers = [TrailerModel MR_findAll];

	NSLog( @"============== TRAILER BREAKOUT ===============" );
	NSLog( @"Trailers: %lu", (unsigned long)trailers.count );

	[trailers enumerateObjectsUsingBlock:^( TrailerModel *trailer, NSUInteger trailerIndex, BOOL *trailerStop ){
		NSLog( @"Trailer: %@", trailer.title );
	}];
	NSLog( @"=============================\n\n\n" );
}

- (IBAction)clearData:(id)sender
{
	[MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
		[MovieModel MR_truncateAllInContext:localContext];
		[GenreModel MR_truncateAllInContext:localContext];
		[TrailerModel MR_truncateAllInContext:localContext];
	}];
}

@end
