//
//  main.m
//  VideoExample
//
//  Created by jbott on 8/25/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SKAppDelegate class]));
    }
}
