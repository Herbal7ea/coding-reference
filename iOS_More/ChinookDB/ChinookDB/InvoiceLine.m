#import "InvoiceLine.h"


@interface InvoiceLine ()

// Private interface goes here.

@end


@implementation InvoiceLine

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nInvoiceLine\ninvoiceLineId: %@\n invoiceId: %@\n trackId: %@\n unitPrice: %@\n quantity: %@", self.invoiceLineId, self.invoiceId, self.trackId, self.unitPrice, self.quantity];
}

@end
