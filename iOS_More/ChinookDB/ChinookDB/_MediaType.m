// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MediaType.m instead.

#import "_MediaType.h"

const struct MediaTypeAttributes MediaTypeAttributes = {
	.mediaTypeId = @"mediaTypeId",
	.name = @"name",
};

const struct MediaTypeRelationships MediaTypeRelationships = {
};

const struct MediaTypeFetchedProperties MediaTypeFetchedProperties = {
};

@implementation MediaTypeID
@end

@implementation _MediaType

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"MediaType" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"MediaType";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"MediaType" inManagedObjectContext:moc_];
}

- (MediaTypeID*)objectID {
	return (MediaTypeID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"mediaTypeIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"mediaTypeId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic mediaTypeId;



- (int32_t)mediaTypeIdValue {
	NSNumber *result = [self mediaTypeId];
	return [result intValue];
}

- (void)setMediaTypeIdValue:(int32_t)value_ {
	[self setMediaTypeId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveMediaTypeIdValue {
	NSNumber *result = [self primitiveMediaTypeId];
	return [result intValue];
}

- (void)setPrimitiveMediaTypeIdValue:(int32_t)value_ {
	[self setPrimitiveMediaTypeId:[NSNumber numberWithInt:value_]];
}





@dynamic name;











@end
