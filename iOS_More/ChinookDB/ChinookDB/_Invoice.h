// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Invoice.h instead.

#import <CoreData/CoreData.h>


extern const struct InvoiceAttributes {
	__unsafe_unretained NSString *billingAddress;
	__unsafe_unretained NSString *billingCity;
	__unsafe_unretained NSString *billingCountry;
	__unsafe_unretained NSString *billingPostalCode;
	__unsafe_unretained NSString *billingState;
	__unsafe_unretained NSString *customerId;
	__unsafe_unretained NSString *invoiceDate;
	__unsafe_unretained NSString *invoiceId;
	__unsafe_unretained NSString *total;
} InvoiceAttributes;

extern const struct InvoiceRelationships {
} InvoiceRelationships;

extern const struct InvoiceFetchedProperties {
} InvoiceFetchedProperties;












@interface InvoiceID : NSManagedObjectID {}
@end

@interface _Invoice : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (InvoiceID*)objectID;





@property (nonatomic, strong) NSString* billingAddress;



//- (BOOL)validateBillingAddress:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* billingCity;



//- (BOOL)validateBillingCity:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* billingCountry;



//- (BOOL)validateBillingCountry:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* billingPostalCode;



//- (BOOL)validateBillingPostalCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* billingState;



//- (BOOL)validateBillingState:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* customerId;



@property int32_t customerIdValue;
- (int32_t)customerIdValue;
- (void)setCustomerIdValue:(int32_t)value_;

//- (BOOL)validateCustomerId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* invoiceDate;



//- (BOOL)validateInvoiceDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* invoiceId;



@property int32_t invoiceIdValue;
- (int32_t)invoiceIdValue;
- (void)setInvoiceIdValue:(int32_t)value_;

//- (BOOL)validateInvoiceId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDecimalNumber* total;



//- (BOOL)validateTotal:(id*)value_ error:(NSError**)error_;






@end

@interface _Invoice (CoreDataGeneratedAccessors)

@end

@interface _Invoice (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveBillingAddress;
- (void)setPrimitiveBillingAddress:(NSString*)value;




- (NSString*)primitiveBillingCity;
- (void)setPrimitiveBillingCity:(NSString*)value;




- (NSString*)primitiveBillingCountry;
- (void)setPrimitiveBillingCountry:(NSString*)value;




- (NSString*)primitiveBillingPostalCode;
- (void)setPrimitiveBillingPostalCode:(NSString*)value;




- (NSString*)primitiveBillingState;
- (void)setPrimitiveBillingState:(NSString*)value;




- (NSNumber*)primitiveCustomerId;
- (void)setPrimitiveCustomerId:(NSNumber*)value;

- (int32_t)primitiveCustomerIdValue;
- (void)setPrimitiveCustomerIdValue:(int32_t)value_;




- (NSDate*)primitiveInvoiceDate;
- (void)setPrimitiveInvoiceDate:(NSDate*)value;




- (NSNumber*)primitiveInvoiceId;
- (void)setPrimitiveInvoiceId:(NSNumber*)value;

- (int32_t)primitiveInvoiceIdValue;
- (void)setPrimitiveInvoiceIdValue:(int32_t)value_;




- (NSDecimalNumber*)primitiveTotal;
- (void)setPrimitiveTotal:(NSDecimalNumber*)value;




@end
