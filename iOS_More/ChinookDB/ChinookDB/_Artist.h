// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Artist.h instead.

#import <CoreData/CoreData.h>


extern const struct ArtistAttributes {
	__unsafe_unretained NSString *artistId;
	__unsafe_unretained NSString *name;
} ArtistAttributes;

extern const struct ArtistRelationships {
} ArtistRelationships;

extern const struct ArtistFetchedProperties {
} ArtistFetchedProperties;





@interface ArtistID : NSManagedObjectID {}
@end

@interface _Artist : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (ArtistID*)objectID;





@property (nonatomic, strong) NSNumber* artistId;



@property int32_t artistIdValue;
- (int32_t)artistIdValue;
- (void)setArtistIdValue:(int32_t)value_;

//- (BOOL)validateArtistId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;






@end

@interface _Artist (CoreDataGeneratedAccessors)

@end

@interface _Artist (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveArtistId;
- (void)setPrimitiveArtistId:(NSNumber*)value;

- (int32_t)primitiveArtistIdValue;
- (void)setPrimitiveArtistIdValue:(int32_t)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




@end
