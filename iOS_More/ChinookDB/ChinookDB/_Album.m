// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Album.m instead.

#import "_Album.h"

const struct AlbumAttributes AlbumAttributes = {
	.albumId = @"albumId",
	.artistId = @"artistId",
	.title = @"title",
};

const struct AlbumRelationships AlbumRelationships = {
};

const struct AlbumFetchedProperties AlbumFetchedProperties = {
};

@implementation AlbumID
@end

@implementation _Album

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Album" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Album";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Album" inManagedObjectContext:moc_];
}

- (AlbumID*)objectID {
	return (AlbumID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"albumIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"albumId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"artistIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"artistId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic albumId;



- (int32_t)albumIdValue {
	NSNumber *result = [self albumId];
	return [result intValue];
}

- (void)setAlbumIdValue:(int32_t)value_ {
	[self setAlbumId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAlbumIdValue {
	NSNumber *result = [self primitiveAlbumId];
	return [result intValue];
}

- (void)setPrimitiveAlbumIdValue:(int32_t)value_ {
	[self setPrimitiveAlbumId:[NSNumber numberWithInt:value_]];
}





@dynamic artistId;



- (int32_t)artistIdValue {
	NSNumber *result = [self artistId];
	return [result intValue];
}

- (void)setArtistIdValue:(int32_t)value_ {
	[self setArtistId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveArtistIdValue {
	NSNumber *result = [self primitiveArtistId];
	return [result intValue];
}

- (void)setPrimitiveArtistIdValue:(int32_t)value_ {
	[self setPrimitiveArtistId:[NSNumber numberWithInt:value_]];
}





@dynamic title;











@end
