#import "Artist.h"


@interface Artist ()

// Private interface goes here.

@end


@implementation Artist

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nArtist\nartistId: %@ \n name: %@", self.artistId, self.name];
}


@end
