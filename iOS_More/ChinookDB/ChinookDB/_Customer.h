// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Customer.h instead.

#import <CoreData/CoreData.h>


extern const struct CustomerAttributes {
	__unsafe_unretained NSString *address;
	__unsafe_unretained NSString *city;
	__unsafe_unretained NSString *company;
	__unsafe_unretained NSString *country;
	__unsafe_unretained NSString *customerId;
	__unsafe_unretained NSString *email;
	__unsafe_unretained NSString *fax;
	__unsafe_unretained NSString *firstName;
	__unsafe_unretained NSString *lastName;
	__unsafe_unretained NSString *phone;
	__unsafe_unretained NSString *postalCode;
	__unsafe_unretained NSString *state;
	__unsafe_unretained NSString *supportRepId;
} CustomerAttributes;

extern const struct CustomerRelationships {
} CustomerRelationships;

extern const struct CustomerFetchedProperties {
} CustomerFetchedProperties;
















@interface CustomerID : NSManagedObjectID {}
@end

@interface _Customer : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (CustomerID*)objectID;





@property (nonatomic, strong) NSString* address;



//- (BOOL)validateAddress:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* city;



//- (BOOL)validateCity:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* company;



//- (BOOL)validateCompany:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* country;



//- (BOOL)validateCountry:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* customerId;



@property int32_t customerIdValue;
- (int32_t)customerIdValue;
- (void)setCustomerIdValue:(int32_t)value_;

//- (BOOL)validateCustomerId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* email;



//- (BOOL)validateEmail:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* fax;



//- (BOOL)validateFax:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* firstName;



//- (BOOL)validateFirstName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* lastName;



//- (BOOL)validateLastName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* phone;



//- (BOOL)validatePhone:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* postalCode;



//- (BOOL)validatePostalCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* state;



//- (BOOL)validateState:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* supportRepId;



@property int32_t supportRepIdValue;
- (int32_t)supportRepIdValue;
- (void)setSupportRepIdValue:(int32_t)value_;

//- (BOOL)validateSupportRepId:(id*)value_ error:(NSError**)error_;






@end

@interface _Customer (CoreDataGeneratedAccessors)

@end

@interface _Customer (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(NSString*)value;




- (NSString*)primitiveCity;
- (void)setPrimitiveCity:(NSString*)value;




- (NSString*)primitiveCompany;
- (void)setPrimitiveCompany:(NSString*)value;




- (NSString*)primitiveCountry;
- (void)setPrimitiveCountry:(NSString*)value;




- (NSNumber*)primitiveCustomerId;
- (void)setPrimitiveCustomerId:(NSNumber*)value;

- (int32_t)primitiveCustomerIdValue;
- (void)setPrimitiveCustomerIdValue:(int32_t)value_;




- (NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(NSString*)value;




- (NSString*)primitiveFax;
- (void)setPrimitiveFax:(NSString*)value;




- (NSString*)primitiveFirstName;
- (void)setPrimitiveFirstName:(NSString*)value;




- (NSString*)primitiveLastName;
- (void)setPrimitiveLastName:(NSString*)value;




- (NSString*)primitivePhone;
- (void)setPrimitivePhone:(NSString*)value;




- (NSString*)primitivePostalCode;
- (void)setPrimitivePostalCode:(NSString*)value;




- (NSString*)primitiveState;
- (void)setPrimitiveState:(NSString*)value;




- (NSNumber*)primitiveSupportRepId;
- (void)setPrimitiveSupportRepId:(NSNumber*)value;

- (int32_t)primitiveSupportRepIdValue;
- (void)setPrimitiveSupportRepIdValue:(int32_t)value_;




@end
