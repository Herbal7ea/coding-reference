#import "PlaylistTrack.h"


@interface PlaylistTrack ()

// Private interface goes here.

@end


@implementation PlaylistTrack

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nPlaylistTrack\ntrackId: %@\n playlistId: %@", self.trackId, self.playlistId];
}

@end
