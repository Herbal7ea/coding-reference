// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Genre.m instead.

#import "_Genre.h"

const struct GenreAttributes GenreAttributes = {
	.genreId = @"genreId",
	.name = @"name",
};

const struct GenreRelationships GenreRelationships = {
};

const struct GenreFetchedProperties GenreFetchedProperties = {
};

@implementation GenreID
@end

@implementation _Genre

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Genre" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Genre";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Genre" inManagedObjectContext:moc_];
}

- (GenreID*)objectID {
	return (GenreID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"genreIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"genreId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic genreId;



- (int32_t)genreIdValue {
	NSNumber *result = [self genreId];
	return [result intValue];
}

- (void)setGenreIdValue:(int32_t)value_ {
	[self setGenreId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveGenreIdValue {
	NSNumber *result = [self primitiveGenreId];
	return [result intValue];
}

- (void)setPrimitiveGenreIdValue:(int32_t)value_ {
	[self setPrimitiveGenreId:[NSNumber numberWithInt:value_]];
}





@dynamic name;











@end
