#import "Employee.h"


@interface Employee ()

// Private interface goes here.

@end


@implementation Employee

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nEmployee\nfirstName: %@\n lastName: %@\n title: %@\n employeeId: %@\n birthDate: %@\n hireDate: %@\n reportsTo: %@\n email: %@\n fax: %@\n phone: %@\n address: %@\n postalCode: %@\n city: %@\n state: %@\n country: %@", self.firstName, self.lastName, self.title, self.employeeId, self.birthDate, self.hireDate, self.reportsToId, self.email, self.fax, self.phone, self.address, self.postalCode, self.city, self.state, self.country];
}

@end



