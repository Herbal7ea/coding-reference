// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Track.m instead.

#import "_Track.h"

const struct TrackAttributes TrackAttributes = {
	.albumId = @"albumId",
	.bytes = @"bytes",
	.composer = @"composer",
	.genreId = @"genreId",
	.mediaTypeId = @"mediaTypeId",
	.milliseconds = @"milliseconds",
	.name = @"name",
	.trackId = @"trackId",
	.unitPrice = @"unitPrice",
};

const struct TrackRelationships TrackRelationships = {
};

const struct TrackFetchedProperties TrackFetchedProperties = {
};

@implementation TrackID
@end

@implementation _Track

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Track" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Track";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Track" inManagedObjectContext:moc_];
}

- (TrackID*)objectID {
	return (TrackID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"albumIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"albumId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"bytesValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"bytes"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"genreIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"genreId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"mediaTypeIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"mediaTypeId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"millisecondsValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"milliseconds"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"trackIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"trackId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic albumId;



- (int32_t)albumIdValue {
	NSNumber *result = [self albumId];
	return [result intValue];
}

- (void)setAlbumIdValue:(int32_t)value_ {
	[self setAlbumId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveAlbumIdValue {
	NSNumber *result = [self primitiveAlbumId];
	return [result intValue];
}

- (void)setPrimitiveAlbumIdValue:(int32_t)value_ {
	[self setPrimitiveAlbumId:[NSNumber numberWithInt:value_]];
}





@dynamic bytes;



- (int32_t)bytesValue {
	NSNumber *result = [self bytes];
	return [result intValue];
}

- (void)setBytesValue:(int32_t)value_ {
	[self setBytes:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveBytesValue {
	NSNumber *result = [self primitiveBytes];
	return [result intValue];
}

- (void)setPrimitiveBytesValue:(int32_t)value_ {
	[self setPrimitiveBytes:[NSNumber numberWithInt:value_]];
}





@dynamic composer;






@dynamic genreId;



- (int32_t)genreIdValue {
	NSNumber *result = [self genreId];
	return [result intValue];
}

- (void)setGenreIdValue:(int32_t)value_ {
	[self setGenreId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveGenreIdValue {
	NSNumber *result = [self primitiveGenreId];
	return [result intValue];
}

- (void)setPrimitiveGenreIdValue:(int32_t)value_ {
	[self setPrimitiveGenreId:[NSNumber numberWithInt:value_]];
}





@dynamic mediaTypeId;



- (int32_t)mediaTypeIdValue {
	NSNumber *result = [self mediaTypeId];
	return [result intValue];
}

- (void)setMediaTypeIdValue:(int32_t)value_ {
	[self setMediaTypeId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveMediaTypeIdValue {
	NSNumber *result = [self primitiveMediaTypeId];
	return [result intValue];
}

- (void)setPrimitiveMediaTypeIdValue:(int32_t)value_ {
	[self setPrimitiveMediaTypeId:[NSNumber numberWithInt:value_]];
}





@dynamic milliseconds;



- (int32_t)millisecondsValue {
	NSNumber *result = [self milliseconds];
	return [result intValue];
}

- (void)setMillisecondsValue:(int32_t)value_ {
	[self setMilliseconds:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveMillisecondsValue {
	NSNumber *result = [self primitiveMilliseconds];
	return [result intValue];
}

- (void)setPrimitiveMillisecondsValue:(int32_t)value_ {
	[self setPrimitiveMilliseconds:[NSNumber numberWithInt:value_]];
}





@dynamic name;






@dynamic trackId;



- (int32_t)trackIdValue {
	NSNumber *result = [self trackId];
	return [result intValue];
}

- (void)setTrackIdValue:(int32_t)value_ {
	[self setTrackId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveTrackIdValue {
	NSNumber *result = [self primitiveTrackId];
	return [result intValue];
}

- (void)setPrimitiveTrackIdValue:(int32_t)value_ {
	[self setPrimitiveTrackId:[NSNumber numberWithInt:value_]];
}





@dynamic unitPrice;











@end
