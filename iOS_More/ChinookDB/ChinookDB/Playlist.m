#import "Playlist.h"


@interface Playlist ()

// Private interface goes here.

@end


@implementation Playlist

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nPlaylist\nplaylistId: %@\n name: %@", self.playlistId, self.name];
}

@end
