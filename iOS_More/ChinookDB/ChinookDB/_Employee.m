// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Employee.m instead.

#import "_Employee.h"

const struct EmployeeAttributes EmployeeAttributes = {
	.address = @"address",
	.birthDate = @"birthDate",
	.city = @"city",
	.country = @"country",
	.email = @"email",
	.employeeId = @"employeeId",
	.fax = @"fax",
	.firstName = @"firstName",
	.hireDate = @"hireDate",
	.lastName = @"lastName",
	.phone = @"phone",
	.postalCode = @"postalCode",
	.reportsToId = @"reportsToId",
	.state = @"state",
	.title = @"title",
};

const struct EmployeeRelationships EmployeeRelationships = {
};

const struct EmployeeFetchedProperties EmployeeFetchedProperties = {
};

@implementation EmployeeID
@end

@implementation _Employee

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Employee" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Employee";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Employee" inManagedObjectContext:moc_];
}

- (EmployeeID*)objectID {
	return (EmployeeID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"employeeIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"employeeId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"reportsToIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"reportsToId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic address;






@dynamic birthDate;






@dynamic city;






@dynamic country;






@dynamic email;






@dynamic employeeId;



- (int32_t)employeeIdValue {
	NSNumber *result = [self employeeId];
	return [result intValue];
}

- (void)setEmployeeIdValue:(int32_t)value_ {
	[self setEmployeeId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveEmployeeIdValue {
	NSNumber *result = [self primitiveEmployeeId];
	return [result intValue];
}

- (void)setPrimitiveEmployeeIdValue:(int32_t)value_ {
	[self setPrimitiveEmployeeId:[NSNumber numberWithInt:value_]];
}





@dynamic fax;






@dynamic firstName;






@dynamic hireDate;






@dynamic lastName;






@dynamic phone;






@dynamic postalCode;






@dynamic reportsToId;



- (int32_t)reportsToIdValue {
	NSNumber *result = [self reportsToId];
	return [result intValue];
}

- (void)setReportsToIdValue:(int32_t)value_ {
	[self setReportsToId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveReportsToIdValue {
	NSNumber *result = [self primitiveReportsToId];
	return [result intValue];
}

- (void)setPrimitiveReportsToIdValue:(int32_t)value_ {
	[self setPrimitiveReportsToId:[NSNumber numberWithInt:value_]];
}





@dynamic state;






@dynamic title;











@end
