// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Invoice.m instead.

#import "_Invoice.h"

const struct InvoiceAttributes InvoiceAttributes = {
	.billingAddress = @"billingAddress",
	.billingCity = @"billingCity",
	.billingCountry = @"billingCountry",
	.billingPostalCode = @"billingPostalCode",
	.billingState = @"billingState",
	.customerId = @"customerId",
	.invoiceDate = @"invoiceDate",
	.invoiceId = @"invoiceId",
	.total = @"total",
};

const struct InvoiceRelationships InvoiceRelationships = {
};

const struct InvoiceFetchedProperties InvoiceFetchedProperties = {
};

@implementation InvoiceID
@end

@implementation _Invoice

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Invoice" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Invoice";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Invoice" inManagedObjectContext:moc_];
}

- (InvoiceID*)objectID {
	return (InvoiceID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"customerIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"customerId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"invoiceIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"invoiceId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic billingAddress;






@dynamic billingCity;






@dynamic billingCountry;






@dynamic billingPostalCode;






@dynamic billingState;






@dynamic customerId;



- (int32_t)customerIdValue {
	NSNumber *result = [self customerId];
	return [result intValue];
}

- (void)setCustomerIdValue:(int32_t)value_ {
	[self setCustomerId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveCustomerIdValue {
	NSNumber *result = [self primitiveCustomerId];
	return [result intValue];
}

- (void)setPrimitiveCustomerIdValue:(int32_t)value_ {
	[self setPrimitiveCustomerId:[NSNumber numberWithInt:value_]];
}





@dynamic invoiceDate;






@dynamic invoiceId;



- (int32_t)invoiceIdValue {
	NSNumber *result = [self invoiceId];
	return [result intValue];
}

- (void)setInvoiceIdValue:(int32_t)value_ {
	[self setInvoiceId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveInvoiceIdValue {
	NSNumber *result = [self primitiveInvoiceId];
	return [result intValue];
}

- (void)setPrimitiveInvoiceIdValue:(int32_t)value_ {
	[self setPrimitiveInvoiceId:[NSNumber numberWithInt:value_]];
}





@dynamic total;











@end
