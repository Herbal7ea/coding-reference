//
//  APIClient.m
//  Playground
//
//  Created by Brian Mullen on 8/21/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//


#import "APIClient.h"
#import "Album.h"
#import "NSManagedObject+MagicalDataImport.h"
#import "MagicalRecord.h"
#import "MagicalRecord+Actions.h"


@implementation APIClient

+ (instancetype)sharedInstance
{
    static APIClient *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once( &oncePredicate, ^
    {
        _sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:@"http://localhost:12866"]];

		_sharedInstance.classNames =  @[@"Album",
										@"Artist",
										@"Customer",
										@"Employee",
										@"Genre",
										@"Invoice",
										@"InvoiceLine",
										@"MediaType",
										@"Playlist",
										@"PlaylistTrack",
										@"Track"];

	} );
    
    return _sharedInstance;
}

- (void)loadJsonAll
{
	[_classNames enumerateObjectsUsingBlock:^(NSString *className, NSUInteger idx, BOOL *stop) {
		[self loadJsonIntoDB:className];
	}];
}

- (void)loadJsonIntoDB:(NSString *)className
{
	Class pClass = NSClassFromString( className );
	if(pClass != NULL )
	{
		NSString *urlString = [NSString stringWithFormat:@"%@.json", className];

		[self GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {

			[MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {

				[pClass MR_importFromArray:responseObject inContext:localContext];

			}];

		} failure:^(NSURLSessionDataTask *task, NSError *error) {
			NSLog( @"FAILED: %@", error );
		}];
	}
}

- (void)loadJsonForClass:(NSString *)className
{
	Class pClass = NSClassFromString( className );
	if(pClass != NULL )
	{
		NSString *urlString = [NSString stringWithFormat:@"%@.json", className];

		[self GET:urlString parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {

			[MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {

				[pClass MR_importFromArray:responseObject inContext:localContext];

			}];

		} failure:^(NSURLSessionDataTask *task, NSError *error) {
			NSLog( @"FAILED: %@", error );
		}];
	}
}


//    }];
@end
