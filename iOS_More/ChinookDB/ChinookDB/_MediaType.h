// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to MediaType.h instead.

#import <CoreData/CoreData.h>


extern const struct MediaTypeAttributes {
	__unsafe_unretained NSString *mediaTypeId;
	__unsafe_unretained NSString *name;
} MediaTypeAttributes;

extern const struct MediaTypeRelationships {
} MediaTypeRelationships;

extern const struct MediaTypeFetchedProperties {
} MediaTypeFetchedProperties;





@interface MediaTypeID : NSManagedObjectID {}
@end

@interface _MediaType : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (MediaTypeID*)objectID;





@property (nonatomic, strong) NSNumber* mediaTypeId;



@property int32_t mediaTypeIdValue;
- (int32_t)mediaTypeIdValue;
- (void)setMediaTypeIdValue:(int32_t)value_;

//- (BOOL)validateMediaTypeId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;






@end

@interface _MediaType (CoreDataGeneratedAccessors)

@end

@interface _MediaType (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitiveMediaTypeId;
- (void)setPrimitiveMediaTypeId:(NSNumber*)value;

- (int32_t)primitiveMediaTypeIdValue;
- (void)setPrimitiveMediaTypeIdValue:(int32_t)value_;




- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




@end
