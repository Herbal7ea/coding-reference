// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to InvoiceLine.m instead.

#import "_InvoiceLine.h"

const struct InvoiceLineAttributes InvoiceLineAttributes = {
	.invoiceId = @"invoiceId",
	.invoiceLineId = @"invoiceLineId",
	.quantity = @"quantity",
	.trackId = @"trackId",
	.unitPrice = @"unitPrice",
};

const struct InvoiceLineRelationships InvoiceLineRelationships = {
};

const struct InvoiceLineFetchedProperties InvoiceLineFetchedProperties = {
};

@implementation InvoiceLineID
@end

@implementation _InvoiceLine

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"InvoiceLine" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"InvoiceLine";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"InvoiceLine" inManagedObjectContext:moc_];
}

- (InvoiceLineID*)objectID {
	return (InvoiceLineID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];
	
	if ([key isEqualToString:@"invoiceIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"invoiceId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"invoiceLineIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"invoiceLineId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"quantityValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"quantity"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"trackIdValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"trackId"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}




@dynamic invoiceId;



- (int32_t)invoiceIdValue {
	NSNumber *result = [self invoiceId];
	return [result intValue];
}

- (void)setInvoiceIdValue:(int32_t)value_ {
	[self setInvoiceId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveInvoiceIdValue {
	NSNumber *result = [self primitiveInvoiceId];
	return [result intValue];
}

- (void)setPrimitiveInvoiceIdValue:(int32_t)value_ {
	[self setPrimitiveInvoiceId:[NSNumber numberWithInt:value_]];
}





@dynamic invoiceLineId;



- (int32_t)invoiceLineIdValue {
	NSNumber *result = [self invoiceLineId];
	return [result intValue];
}

- (void)setInvoiceLineIdValue:(int32_t)value_ {
	[self setInvoiceLineId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveInvoiceLineIdValue {
	NSNumber *result = [self primitiveInvoiceLineId];
	return [result intValue];
}

- (void)setPrimitiveInvoiceLineIdValue:(int32_t)value_ {
	[self setPrimitiveInvoiceLineId:[NSNumber numberWithInt:value_]];
}





@dynamic quantity;



- (int32_t)quantityValue {
	NSNumber *result = [self quantity];
	return [result intValue];
}

- (void)setQuantityValue:(int32_t)value_ {
	[self setQuantity:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveQuantityValue {
	NSNumber *result = [self primitiveQuantity];
	return [result intValue];
}

- (void)setPrimitiveQuantityValue:(int32_t)value_ {
	[self setPrimitiveQuantity:[NSNumber numberWithInt:value_]];
}





@dynamic trackId;



- (int32_t)trackIdValue {
	NSNumber *result = [self trackId];
	return [result intValue];
}

- (void)setTrackIdValue:(int32_t)value_ {
	[self setTrackId:[NSNumber numberWithInt:value_]];
}

- (int32_t)primitiveTrackIdValue {
	NSNumber *result = [self primitiveTrackId];
	return [result intValue];
}

- (void)setPrimitiveTrackIdValue:(int32_t)value_ {
	[self setPrimitiveTrackId:[NSNumber numberWithInt:value_]];
}





@dynamic unitPrice;











@end
