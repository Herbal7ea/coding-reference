// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Employee.h instead.

#import <CoreData/CoreData.h>


extern const struct EmployeeAttributes {
	__unsafe_unretained NSString *address;
	__unsafe_unretained NSString *birthDate;
	__unsafe_unretained NSString *city;
	__unsafe_unretained NSString *country;
	__unsafe_unretained NSString *email;
	__unsafe_unretained NSString *employeeId;
	__unsafe_unretained NSString *fax;
	__unsafe_unretained NSString *firstName;
	__unsafe_unretained NSString *hireDate;
	__unsafe_unretained NSString *lastName;
	__unsafe_unretained NSString *phone;
	__unsafe_unretained NSString *postalCode;
	__unsafe_unretained NSString *reportsToId;
	__unsafe_unretained NSString *state;
	__unsafe_unretained NSString *title;
} EmployeeAttributes;

extern const struct EmployeeRelationships {
} EmployeeRelationships;

extern const struct EmployeeFetchedProperties {
} EmployeeFetchedProperties;


















@interface EmployeeID : NSManagedObjectID {}
@end

@interface _Employee : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (EmployeeID*)objectID;





@property (nonatomic, strong) NSString* address;



//- (BOOL)validateAddress:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* birthDate;



//- (BOOL)validateBirthDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* city;



//- (BOOL)validateCity:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* country;



//- (BOOL)validateCountry:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* email;



//- (BOOL)validateEmail:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* employeeId;



@property int32_t employeeIdValue;
- (int32_t)employeeIdValue;
- (void)setEmployeeIdValue:(int32_t)value_;

//- (BOOL)validateEmployeeId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* fax;



//- (BOOL)validateFax:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* firstName;



//- (BOOL)validateFirstName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSDate* hireDate;



//- (BOOL)validateHireDate:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* lastName;



//- (BOOL)validateLastName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* phone;



//- (BOOL)validatePhone:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* postalCode;



//- (BOOL)validatePostalCode:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* reportsToId;



@property int32_t reportsToIdValue;
- (int32_t)reportsToIdValue;
- (void)setReportsToIdValue:(int32_t)value_;

//- (BOOL)validateReportsToId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* state;



//- (BOOL)validateState:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSString* title;



//- (BOOL)validateTitle:(id*)value_ error:(NSError**)error_;






@end

@interface _Employee (CoreDataGeneratedAccessors)

@end

@interface _Employee (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveAddress;
- (void)setPrimitiveAddress:(NSString*)value;




- (NSDate*)primitiveBirthDate;
- (void)setPrimitiveBirthDate:(NSDate*)value;




- (NSString*)primitiveCity;
- (void)setPrimitiveCity:(NSString*)value;




- (NSString*)primitiveCountry;
- (void)setPrimitiveCountry:(NSString*)value;




- (NSString*)primitiveEmail;
- (void)setPrimitiveEmail:(NSString*)value;




- (NSNumber*)primitiveEmployeeId;
- (void)setPrimitiveEmployeeId:(NSNumber*)value;

- (int32_t)primitiveEmployeeIdValue;
- (void)setPrimitiveEmployeeIdValue:(int32_t)value_;




- (NSString*)primitiveFax;
- (void)setPrimitiveFax:(NSString*)value;




- (NSString*)primitiveFirstName;
- (void)setPrimitiveFirstName:(NSString*)value;




- (NSDate*)primitiveHireDate;
- (void)setPrimitiveHireDate:(NSDate*)value;




- (NSString*)primitiveLastName;
- (void)setPrimitiveLastName:(NSString*)value;




- (NSString*)primitivePhone;
- (void)setPrimitivePhone:(NSString*)value;




- (NSString*)primitivePostalCode;
- (void)setPrimitivePostalCode:(NSString*)value;




- (NSNumber*)primitiveReportsToId;
- (void)setPrimitiveReportsToId:(NSNumber*)value;

- (int32_t)primitiveReportsToIdValue;
- (void)setPrimitiveReportsToIdValue:(int32_t)value_;




- (NSString*)primitiveState;
- (void)setPrimitiveState:(NSString*)value;




- (NSString*)primitiveTitle;
- (void)setPrimitiveTitle:(NSString*)value;




@end
