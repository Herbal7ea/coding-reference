#import "Customer.h"


@interface Customer ()

// Private interface goes here.

@end


@implementation Customer

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nCustomer\nfirstName: %@\n lastName: %@\n company: %@\n customerId: %@\n supportRepId: %@\n email: %@\n phone: %@\n fax: %@\n address: %@\n city: %@\n state: %@\n postalCode: %@\n country: %@", self.firstName, self.lastName, self.company, self.customerId, self.supportRepId, self.email, self.phone, self.fax, self.address, self.city, self.state, self.postalCode, self.country];
}

@end
