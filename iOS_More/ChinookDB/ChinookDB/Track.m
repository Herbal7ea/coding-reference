#import "Track.h"


@interface Track ()

// Private interface goes here.

@end


@implementation Track

- (NSString *)description
{
	return [NSString stringWithFormat:@"\nTrack\ntrackId: %@\n albumId: %@\n mediaTypeId: %@\n genreId: %@\n name: %@\n composer: %@\n bytes: %@\n milliseconds: %@\n unitPrice: %@", self.trackId, self.albumId, self.mediaTypeId, self.genreId, self.name, self.composer, self.bytes, self.milliseconds, self.unitPrice];
}


@end
