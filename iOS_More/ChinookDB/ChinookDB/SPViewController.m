//
//  SPViewController.m
//  ChinookDB
//
//  Created by jbott on 8/27/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import "SPViewController.h"
#import "Album.h"
#import "NSManagedObject+MagicalRecord.h"
#import "MagicalRecord+Actions.h"
#import "APIClient.h"
#import "NSManagedObject+MagicalFinders.h"

@interface SPViewController ()
{
	NSString *_className;
}

@end

@implementation SPViewController

- (void)viewDidLoad
{
	_className = @"Track";

	//	_sharedInstance.classNames =  @[@"Album",
	//									@"Artist",
	//									@"Customer",
	//									@"Employee",
	//									@"Genre",
	//									@"Invoice",
	//									@"InvoiceLine",
	//									@"MediaType",
	//									@"Playlist",
	//									@"PlaylistTrack",
	//									@"Track"];
}

- (IBAction)loadClicked:(id)sender
{
//	[self loadSingle];
	[self loadAll];
}

- (IBAction)logClicked:(id)sender
{

//	[self logSingle];
	[self logAll];
}

- (IBAction)clearClicked:(id)sender
{
	//	[self clearSingle];
	[self clearAll];
}



- (void)loadSingle
{
	[[APIClient sharedInstance] loadJsonForClass:_className];
}

- (void)loadAll
{
	[[APIClient sharedInstance] loadJsonAll];
}

- (void)logSingle
{
	Class pClass = NSClassFromString( _className );

	NSArray *dataItems = [pClass MR_findAll];

	NSLog( @"============= %@ BREAKOUT ================", _className );
	NSLog( @"%@: %lu", _className, (unsigned long) dataItems.count );

	[dataItems enumerateObjectsUsingBlock:^(id data, NSUInteger genreIndex, BOOL *genreStop) {
		NSLog(@"-----------------------");
        NSLog( [data description] );
	}];

	NSLog( @"=============================\n\n\n" );
}

- (void)logAll
{
	[APIClient.sharedInstance.classNames enumerateObjectsUsingBlock:^(NSString * className, NSUInteger idx, BOOL *stop) {
		Class pClass = NSClassFromString( className );

		NSArray *dataItems = [pClass MR_findAll];

		NSLog( @"============= %@ BREAKOUT ================", className );
		NSLog( @"%@: %lu", className, (unsigned long) dataItems.count );

		//		[dataItems enumerateObjectsUsingBlock:^(id data, NSUInteger genreIndex, BOOL *genreStop) {
		//			NSLog(@"-----------------------");
		//			NSLog( [data description] );
		//		}];

		NSLog( @"=============================\n\n\n" );
	}];
}

- (void)clearSingle
{
	[MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
		Class pClass = NSClassFromString( _className );
		[pClass MR_truncateAllInContext:localContext];
	}];
}

- (void)clearAll
{
	[MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {

		[APIClient.sharedInstance.classNames enumerateObjectsUsingBlock:^(NSString * className, NSUInteger idx, BOOL *stop) {

			Class pClass = NSClassFromString( className );
			[pClass MR_truncateAllInContext:localContext];

		}];

	}];
}

@end
