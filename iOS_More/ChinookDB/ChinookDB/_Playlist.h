// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Playlist.h instead.

#import <CoreData/CoreData.h>


extern const struct PlaylistAttributes {
	__unsafe_unretained NSString *name;
	__unsafe_unretained NSString *playlistId;
} PlaylistAttributes;

extern const struct PlaylistRelationships {
} PlaylistRelationships;

extern const struct PlaylistFetchedProperties {
} PlaylistFetchedProperties;





@interface PlaylistID : NSManagedObjectID {}
@end

@interface _Playlist : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PlaylistID*)objectID;





@property (nonatomic, strong) NSString* name;



//- (BOOL)validateName:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* playlistId;



@property int32_t playlistIdValue;
- (int32_t)playlistIdValue;
- (void)setPlaylistIdValue:(int32_t)value_;

//- (BOOL)validatePlaylistId:(id*)value_ error:(NSError**)error_;






@end

@interface _Playlist (CoreDataGeneratedAccessors)

@end

@interface _Playlist (CoreDataGeneratedPrimitiveAccessors)


- (NSString*)primitiveName;
- (void)setPrimitiveName:(NSString*)value;




- (NSNumber*)primitivePlaylistId;
- (void)setPrimitivePlaylistId:(NSNumber*)value;

- (int32_t)primitivePlaylistIdValue;
- (void)setPrimitivePlaylistIdValue:(int32_t)value_;




@end
