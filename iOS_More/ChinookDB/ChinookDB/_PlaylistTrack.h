// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to PlaylistTrack.h instead.

#import <CoreData/CoreData.h>


extern const struct PlaylistTrackAttributes {
	__unsafe_unretained NSString *playlistId;
	__unsafe_unretained NSString *trackId;
} PlaylistTrackAttributes;

extern const struct PlaylistTrackRelationships {
} PlaylistTrackRelationships;

extern const struct PlaylistTrackFetchedProperties {
} PlaylistTrackFetchedProperties;





@interface PlaylistTrackID : NSManagedObjectID {}
@end

@interface _PlaylistTrack : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
- (PlaylistTrackID*)objectID;





@property (nonatomic, strong) NSNumber* playlistId;



@property int32_t playlistIdValue;
- (int32_t)playlistIdValue;
- (void)setPlaylistIdValue:(int32_t)value_;

//- (BOOL)validatePlaylistId:(id*)value_ error:(NSError**)error_;





@property (nonatomic, strong) NSNumber* trackId;



@property int32_t trackIdValue;
- (int32_t)trackIdValue;
- (void)setTrackIdValue:(int32_t)value_;

//- (BOOL)validateTrackId:(id*)value_ error:(NSError**)error_;






@end

@interface _PlaylistTrack (CoreDataGeneratedAccessors)

@end

@interface _PlaylistTrack (CoreDataGeneratedPrimitiveAccessors)


- (NSNumber*)primitivePlaylistId;
- (void)setPrimitivePlaylistId:(NSNumber*)value;

- (int32_t)primitivePlaylistIdValue;
- (void)setPrimitivePlaylistIdValue:(int32_t)value_;




- (NSNumber*)primitiveTrackId;
- (void)setPrimitiveTrackId:(NSNumber*)value;

- (int32_t)primitiveTrackIdValue;
- (void)setPrimitiveTrackIdValue:(int32_t)value_;




@end
