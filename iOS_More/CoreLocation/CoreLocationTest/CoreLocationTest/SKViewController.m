//
//  SKViewController.m
//  CoreLocationTest
//
//  Created by jbott on 8/26/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import "SKViewController.h"

@interface SKViewController ()

@property(weak, nonatomic) IBOutlet UILabel *longitude;
@property(weak, nonatomic) IBOutlet UILabel *latitude;
@property(weak, nonatomic) IBOutlet UILabel *altitude;
@property(weak, nonatomic) IBOutlet UILabel *speed;
@property(nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation SKViewController

- (void)viewDidLoad
{
	[super viewDidLoad];
	_locationManager = [CLLocationManager new];
	_locationManager.delegate = self;
	_locationManager.desiredAccuracy = kCLLocationAccuracyBest;

	[_locationManager startUpdatingLocation];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
	CLLocation *currentLocation = [locations lastObject];

	_latitude.text   = [NSString stringWithFormat:@"%.8f",     currentLocation.coordinate.latitude];
	_longitude.text  = [NSString stringWithFormat:@"%.8f",     currentLocation.coordinate.longitude];
	_altitude.text   = [NSString stringWithFormat:@"%.0f m",   currentLocation.altitude];
	_speed.text      = [NSString stringWithFormat:@"%.1f m/s", currentLocation.speed];
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	[[[UIAlertView alloc] initWithTitle:@"Error"
								message:@"There was an error retrieving your location"
							   delegate:nil
					  cancelButtonTitle:@"OK"
					  otherButtonTitles:nil] show];

	NSLog( @"Error: %@", error.description );
}

@end
