//
//  SKViewController.h
//  CoreLocationTest
//
//  Created by jbott on 8/26/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

@interface SKViewController : UIViewController <CLLocationManagerDelegate>


@end
