//
//  APIClient.m
//  Playground
//
//  Created by Brian Mullen on 8/21/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//


#import "APIClient.h"
#import "NSManagedObject+MagicalDataImport.h"
#import "MagicalRecord.h"
#import "MagicalRecord+Actions.h"


@implementation APIClient

+ (instancetype)sharedInstance
{
    static APIClient *_sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    
    dispatch_once( &oncePredicate, ^
    {
        _sharedInstance = [[self alloc] initWithBaseURL:[NSURL URLWithString:@"http://localhost:12866"]];
	} );
    
    return _sharedInstance;
}

- (void)loadJsonAll
{

    
    [self GET:@"Album.json" parameters:nil success:^(NSURLSessionDataTask *task, id responseObject) {
        
//        [MagicalRecord saveWithBlockAndWait:^(NSManagedObjectContext *localContext) {
//            
//            [pClass MR_importFromArray:responseObject inContext:localContext];
//            
//        }];
        
    } failure:^(NSURLSessionDataTask *task, NSError *error) {
        NSLog( @"FAILED: %@", error );
    }];
}

@end
