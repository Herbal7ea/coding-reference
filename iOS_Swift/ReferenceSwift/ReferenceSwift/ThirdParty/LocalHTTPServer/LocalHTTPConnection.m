//
//  LocalHTTPConnection.m
//  Playground
//
//  Created by Brian Mullen on 8/21/14.
//  Copyright (c) 2014 Rutile Interactive. All rights reserved.
//

#import "LocalHTTPConnection.h"
#import "HTTPMessage.h"


@implementation LocalHTTPConnection

- (NSData *)preprocessResponse:(HTTPMessage *)response
{
    NSURL *requestUrl = [NSURL URLWithString:self.requestURI];
    
    if( [requestUrl.pathExtension.lowercaseString isEqualToString:@"json"] )
    {
        [response setHeaderField:@"Content-Type" value:@"application/json"];
    }
    
	return [super preprocessResponse:response];
}

@end
