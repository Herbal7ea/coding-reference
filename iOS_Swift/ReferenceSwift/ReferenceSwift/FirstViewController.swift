//
//  FirstViewController.swift
//  ReferenceSwift
//
//  Created by jbott on 8/29/14.
//  Copyright (c) 2014 SkunkPuncher. All rights reserved.
//

import UIKit

class FirstViewController: UIViewController {
                            
    @IBAction func loadClicked(sender: UIButton) {
        APIClient.sharedInstance().loadJsonAll()
    }
    
    @IBAction func logClicked(sender: AnyObject) {
        
    }
    
    @IBAction func clearClicked(sender: AnyObject) {
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

