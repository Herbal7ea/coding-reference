//
//  PSKViewController.m
//  SKPocketCyclops
//
//  Created by Jake Gundersen on 10/26/13.
//  Copyright (c) 2013 Razeware, LLC. All rights reserved.
//

#import "PSKGameViewController.h"
#import "PSKLevelScene.h"

@interface PSKGameViewController () <SceneDelegate>
@property(nonatomic, strong) NSMutableArray *observers;
@end

@implementation PSKGameViewController

- (void)viewWillLayoutSubviews
{
    [super viewWillLayoutSubviews];

    [self setupObservers];

    // Configure the view.
    SKView *skView = (SKView *) self.view;
    skView.showsFPS = YES;
    skView.showsNodeCount = YES;

    // Create and configure the scene.
    PSKLevelScene *scene = [[PSKLevelScene alloc] initWithSize:skView.bounds.size level:self.currentLevel];
    scene.scaleMode = SKSceneScaleModeAspectFill;
    scene.sceneDelegate = self;

    // Present the scene.
    [skView presentScene:scene];
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (void)setupObservers
{
    self.observers = [NSMutableArray array];

    [self addObserverForEvent:UIApplicationDidEnterBackgroundNotification paused:YES];
    [self addObserverForEvent:UIApplicationWillResignActiveNotification paused:YES];
    [self addObserverForEvent:UIApplicationWillEnterForegroundNotification paused:NO];

}

- (void)dealloc
{
    [self removeObservers];
}

- (void)dismissScene
{
    [self removeObservers];

    self.observers = nil;

    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)removeObservers
{
    for (id observer in self.observers)
    {
        [NSNotificationCenter.defaultCenter removeObserver:observer];
    }
}

- (void)addObserverForEvent:(NSString *)eventName paused:(BOOL)paused
{
    id observer = [NSNotificationCenter.defaultCenter addObserverForName:eventName object:nil queue:NSOperationQueue.mainQueue usingBlock:^(NSNotification *note)
    {
        SKView *skView = (SKView *) self.view;

        skView.paused = paused;
    }];
    [self.observers addObject:observer];
}

@end
