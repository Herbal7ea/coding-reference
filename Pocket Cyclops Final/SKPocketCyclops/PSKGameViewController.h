//
//  PSKViewController.h
//  SKPocketCyclops
//
//  Created by Jake Gundersen on 10/26/13.
//  Copyright (c) 2013 Razeware, LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SpriteKit/SpriteKit.h>

@interface PSKGameViewController : UIViewController

@property (nonatomic, assign) NSUInteger currentLevel;

@end
